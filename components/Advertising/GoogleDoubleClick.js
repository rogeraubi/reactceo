import {Component} from 'react';
import ReactDOM from 'react-dom';
import helper from '../../helpers/helper.js';
import PropTypes from 'prop-types';
import {inject, observer} from "mobx-react";
@inject('store') @observer

export default class GoogleDoubleClick extends Component {

    constructor(props) {
        super(props);
        this.state = {
            addScrollClass: false,
            reachedBottomArticle: false,
            mrecCurrentPosition: 0
        }
        this.scrollEventListener = this.scrollEventListener.bind(this);
        this.displayAd = this.displayAd.bind(this);
    }

    componentDidMount() {
        // Get current location's distance from the top of the page
        var position = window.pageYOffset;
        // Get an element's distance from the top of the page
        let {mrecCurrentPosition} = this.state;
        if (document.querySelector('#adv-mrec-fixed')) {
            mrecCurrentPosition = helper.getElemDistance(document.querySelector('#adv-mrec-fixed')) - 80;
            if (document.getElementById('entry-sidebar-before-inner')) {
                this.state.mrecCurrentPosition = mrecCurrentPosition + document.getElementById('entry-sidebar-before-inner').clientHeight;
            }
        }
        this.scrollEventListener();
        document.addEventListener('scroll', () => {
            this.scrollEventListener();
        });

        const self = this;

        /**** GOOGLE DOUBLE CLICK ADS *****/
        var googletag = window.googletag;
        if (googletag && googletag.cmd && window.googletag.cmd) {
            googletag.cmd = window.googletag ? window.googletag.cmd : {};
            if (googletag && googletag.cmd) {
                googletag.cmd.push(function () {
                    if (self.props.type == 'leaderboard') {
                        var leaderboard_mapping = googletag.sizeMapping().addSize([520, 0], [728, 90]).addSize([0, 0], [320, 50]).build();
                        var mrec_mapping = googletag.sizeMapping().addSize([0, 0], [300, 250]).build();
                        googletag.defineSlot(self.props.adslot, [728, 90], self.props.id).defineSizeMapping(leaderboard_mapping).addService(googletag.pubads());
                        //googletag.defineSlot('/77678524/CEO_Home_RSB_ATF_300X250', [300, 250], 'div-gpt-ad-1488342309549-1').addService(googletag.pubads());
                        googletag.pubads().enableSingleRequest();
                        googletag.pubads().collapseEmptyDivs();
                        googletag.enableServices();
                    } else {
                        var leaderboard_mapping = googletag.sizeMapping().addSize([520, 0], [728, 90]).addSize([0, 0], [320, 50]).build();
                        var mrec_mapping = googletag.sizeMapping().addSize([0, 0], [300, 250]).build();
                        //googletag.defineSlot(self.props.adslot, [728, 90], self.props.id).defineSizeMapping(leaderboard_mapping).addService(googletag.pubads());
                        googletag.defineSlot(self.props.adslot, [300, 250], self.props.id).addService(googletag.pubads());
                        googletag.pubads().enableSingleRequest();
                        googletag.pubads().collapseEmptyDivs();
                        googletag.enableServices();
                    }
                });
                self.displayAd();
            }
        }
    }

    displayAd() {
        const {id} = this.props;
        console.log(id);
        if (id && id != "") {
            window.googletag.cmd.push(function () {
                googletag.display(id);
            });
        }
    }

    scrollEventListener() {
        const {mrecCurrentPosition} = this.state;
        let articleBodyBottomPosition = 0;
        if (document.getElementById('content-entry')) {
            articleBodyBottomPosition = document.getElementById('content-entry').getBoundingClientRect().bottom;
        }

        let windowPosition = window.pageYOffset;

        if (windowPosition > mrecCurrentPosition && articleBodyBottomPosition > 350) { // 80 for header 250 for mrec plsu spacing
            this.setState({
                addScrollClass: true,
                reachedBottomArticle: false
            })
        } else if (articleBodyBottomPosition > 0 && windowPosition > mrecCurrentPosition) {
            this.setState({
                addScrollClass: false,
                reachedBottomArticle: true
            })
        } else if (windowPosition < mrecCurrentPosition) {
            this.setState({
                addScrollClass: false,
                reachedBottomArticle: false
            })
        }
    }

    render() {
        const {id,type} = this.props;
        const {addScrollClass, reachedBottomArticle} = this.state;
        let csstype=''
            ,executivetype='';
        executivetype = this.props.store.posttype;
        let mrec = "";
        if ((executivetype == "executive-interviews") || (executivetype == "globalShop")) {
            mrec = <div id="adv-mrec">
                <div id={id}></div>
            </div>;
        }
        else if (type == "mrec") {
            mrec = <div id="adv-mrec">
                <div id={id}></div>
            </div>;
        } else if (type == "mrec-fixed") {
            mrec = <div id="adv-mrec-fixed"
                        className={(addScrollClass ? "widget-fixed" : '') + (reachedBottomArticle ? "widget-fixed-bottom" : "")}>
                <div id={id}></div>
            </div>;
        } else {
            mrec = <div id={id}></div>
        }
        if (((executivetype == "executive-interviews") || (executivetype == "globalShop"))&& (type== "leaderboard")) {
            csstype = executivetype;
        }
        else {
            csstype = type;
        }

        return <div className={"widget widget-adv clear text-center widget-adv-" + csstype}>
             {mrec}
             <style global jsx>{`
                    .widget-adv-leaderboard {
                        padding: 20px 0px;
                        overflow: hidden;
                        margin: 0px !important;
                    }

                    .widget-adv-executive-interviews {
                        background:#efefef;
                        padding: 20px 0px;
                        overflow: hidden;
                        margin: 0px !important;
                    }

                    .widget-adv-globalShop {
                        background:#efefef;
                        padding: 20px 0px;
                        overflow: hidden;
                        margin: 0px !important;
                    }

                    @media ( min-width: 961px ) {
                        .widget-fixed-bottom{
                            position: absolute;
                            bottom: 20px;
                        }
                   }
                    @media ( min-width: 961px ) {
                        .widget-fixed{
                            position: fixed;
                            top: 80px;
                            z-index: 99;
                        }
                   }
                   @media ( max-width: 960px ) {
                        .post-view .entry-sidebar .widget-adv-MREC {
                            padding-bottom: 20px;
                        }
                   }
                `}</style>
        </div>
    }
}

GoogleDoubleClick.propTypes = {
    adslot: PropTypes.string,
    type: PropTypes.string,
    id: PropTypes.string
}
