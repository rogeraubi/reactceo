import { PureComponent } from 'react';
import gtmParts from 'react-google-tag-manager';

class GoogleTagManager extends PureComponent {

    componentDidMount() {
        const dataLayerName = this.props.dataLayerName || 'dataLayer';
        const scriptId = this.props.scriptId || 'react-google-tag-manager-gtm';
        
        const gtmScriptNode = document.getElementById(scriptId);
        eval(gtmScriptNode.textContent);
    }

    render() {

         const gtm = gtmParts({
            id: this.props.gtmId,
            dataLayerName: this.props.dataLayerName || 'dataLayer',
            additionalEvents: this.props.additionalEvents || {}
         });

        let script = "";

        if(this.props.type == "noscript"){
            script = gtm.noScriptAsReact();
        }else{
            script = gtm.scriptAsReact();
        }
        return script;
    }
}

export default GoogleTagManager;