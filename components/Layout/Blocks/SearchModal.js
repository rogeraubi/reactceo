import { Component } from 'react';
import { inject, observer } from 'mobx-react'
//import ReactModal from 'react-modal';
import { Modal, Grid, Row, Col, Image} from 'react-bootstrap';
import service from '../../../helpers/service';
import ArchiveSingleBlock from './ArchiveSingleBlock'
import { ClipLoader } from 'react-spinners';

@inject('store') @observer
export default class SearchModal extends Component{
    constructor(props){
        super(props);

        this.state = {
          searchInput: "",
          searchList: [],
          isSearching: false,
          hasSearched: false
        }

        this.updateInput = this.updateInput.bind(this);
        this.fetchSearch = this.fetchSearch.bind(this);
        this.handleKeyPress = this.handleKeyPress.bind(this);
    }
    
    updateInput(event, index){
      const name = event.target.name;
      const value = event.target.value;
      this.setState({[name] : value})
    }

    async fetchSearch(){
      if(this.state.searchInput == ""){
        return false;
      }
      this.setState({
        isSearching: true
      })
      window.location.href = "/search?q=" + this.state.searchInput;
      /*
      this.setState({
        isSearching: true,
        hasSearched: true,
        searchList: []
      })
      const searchDataPromise = await service.getArchive('search', { q: '"' + this.state.searchInput + '"' });
      const searchData = await searchDataPromise.json();
      this.setState({
        searchList: searchData,
        isSearching: false
      })*/
    }

    handleKeyPress = (event) => {
      if(event.key == 'Enter'){
        this.fetchSearch();
      }
    }

    render(){
      
       if(!this.props.store.isSearchModalOpen || this.props.store.isSearchModalOpen == false){
        return <div/>
       }
       
		const { classNamesLi  } = this.props;
        /*
        let searchListText = "";
        if(this.state.searchList){
            searchListText = this.state.searchList.map( search =>{
              return <ArchiveSingleBlock key={search.id} classNamesLi="col-sm-4 col-xs-12" {...this.props} imgSize="medium" archive={search} />
            })
        }
        let searchViewMore = "";
        if ( this.state.hasSearched) {
            searchViewMore = <div className="text-center"><a className="btn btn-main btn-lg" href={"/search?q=" + this.state.searchInput}><strong>View More Results <i className="fa fa-chevron-right"></i></strong></a></div>
        }
        <div className="search-modal-results">
            <div className="text-center">
                <ClipLoader color={'#123abc'} loading={this.state.isSearching}  size={100} />
            </div>
            <ul className="row widget-pab list-unstyled">
                { searchListText }
            </ul>
        </div>
        { !this.state.isSearching ?  searchViewMore : ''} 
        */

        return <div className="seach-modal-parent">
                	<Modal.Dialog dialogClassName="search-modal" bsSize="large">
                		<Modal.Title className="widget-title">
                			<span className="modal-close cursor" onClick={this.props.store.closeSearchModal } ><i className="fa fa-close"></i></span>
                		</Modal.Title>
                  		<Modal.Body className="widget-body">
	                        <div className="search-input-wrapper">
	                          <input type="text" className="search-input" onKeyPress={this.handleKeyPress} onChange={this.updateInput} name="searchInput" value={this.state.searchInput} placeholder="Enter search text... "/>
	                          <button className="btn btn-main btn-lg" onClick={ this.fetchSearch } > <em className="fa fa-search"></em> </button>
	                        </div>  
                            <div className="text-center">
                                <ClipLoader color={'#123abc'} loading={this.state.isSearching}  size={100} />
                            </div>
                  		</Modal.Body>
                	</Modal.Dialog>

					<style global jsx>{`

						.seach-modal-parent .modal {
						    background: #475e6b96;
						}
						.seach-modal-parent .modal-content {
							position: relative;
						}
						.seach-modal-parent .modal-dialog {
							height: 100%;
						}
						.search-modal .widget-title {
							text-align: right;
							position: absolute;
							top: 0px;
							left: 0px;
							right: 32px;
							z-index: 999;
						}
						.search-modal .modal-close {
							position: fixed;
						}
						.search-modal .modal-close .fa {
							font-size: 30px;
							width: 30px;
							height: 30px;
							color: #000;
						}
						.search-modal .modal-body {
							padding-left: 30px;
							padding-right: 30px;
							padding-bottom: 80px; 
						}
						  .search-input{
						    width: 100%;
						  }
						  .search-modal .modal-content{
						    border-radius: 2px;
						    min-height: 300px;
						    max-height: 100%;
						    overflow:hidden;
						    overflow-y: scroll;
						    padding:5px;
						    background: #F8F8F8;
						  }
						.search-modal .search-input-wrapper {
							padding: 0px 0px 20px;
							max-width: 480px;
							margin: 0 auto;	
							position: relative;
						
						}	
						.search-modal .search-input {
							border: 0px;
							line-height: 50px;
							text-align: center;
							font-size: 25px;
							background: #FFF;
							padding-right: 50px;
						}
						.search-modal .search-input-wrapper .btn {
							position: absolute;
							right: 0px;
							height: 50px;
							line-height: 30px;
							font-size: 25px;
						}
						.search-modal .widget-pab-body {
						  	padding-top: 0px;
						}
						.search-modal .search-modal-results {
							padding: 20px 10px;
						}
						.search-modal .search-modal-results .widget-item {
							padding-left: 10px;
							padding-right: 10px;
						}
						.search-modal .search-modal-results .widget-item:nth-child(3n+1){
							clear: both;
						}
						@media ( max-width: 767px ) {
							.search-modal .search-input {
								font-size: 16px;
							}
						}
                    `}</style>
                </div>
            
    }
}
