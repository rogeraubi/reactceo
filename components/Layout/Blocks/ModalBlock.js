import { PureComponent } from 'react';
import helper from '../../../helpers/helper'
import { Modal, Grid, Row, Col, Image} from 'react-bootstrap';

export default class ModalBlock extends PureComponent{

render(){
    return <Modal.Dialog dialogClassName="general-modal modal-video modal-dark" bsSize="large">
                    <Modal.Title className="widget-title">
                        <span className="modal-close cursor" onClick={this.props.modalHandler } ><i className="fa fa-close"></i></span>
                    </Modal.Title>
                    <Modal.Body className="modal-body">
                        <div dangerouslySetInnerHTML={{__html: this.props.modalBody }} />
                    </Modal.Body>
				<style global jsx>{`
					.modal {
						background: #060b0e99;	
					}
					.general-modal.modal-dialog {
					    margin-top: 80px;
					 }
					
					.general-modal .modal-content {
					    box-shadow: 0 5px 15px rgba(0,0,0,.5);
					    height: auto;
					    min-height: 100%;
					    border-radius: 0;
					}
					.modal-dark .modal-content{
						background: #000;
						position: relative;
					}
					.modal-video .modal-close {
						color: #FFF;
						font-weight: bold;
						z-index: 999;
						position: absolute;
						right: 0px;
						top: 0px;
						cursor: pointer;
						font-size: 40px;
						height: 40px;
						width: 40px;
						text-align: center;
					}
					.modal-video .modal-body {
						position: static;
						padding: 0px;
					}
					.modal-video .video-container {
						margin: 0px;
					}
                `}</style>
                
            </Modal.Dialog>
	}


}