import { Component } from 'react';
import helper from '../../../../helpers/helper'
import { Grid, Row, Col, Image} from 'react-bootstrap';
//import GoogleMapReact from 'google-map-react'

export default class MapBlock extends Component{

  constructor(props){
    super(props);

  }

  render(){
    const layoutStyle = {
        
    }

    const { singlePostCustomMeta } = this.props;

    /*
    	return <GoogleMapReact
		          defaultCenter={{lat: 40.7446790, lng: -73.9485420} }
		          defaultZoom={ 11 }>
		        </GoogleMapReact>
	*/

	return <div className="post-widget widget widget-company-map">
			 <div className="map-block" dangerouslySetInnerHTML={{__html: singlePostCustomMeta.custom_meta_company_map }} />    

       <style global jsx>{`
           .map-block iframe{
            width:100%;
           }
        `}</style>

		   </div>
   
  }
}
