import { PureComponent } from 'react';
import helper from '../../../../helpers/helper'
import { Grid, Row, Col, Image} from 'react-bootstrap';

export default class SocialBlock extends PureComponent{

  constructor(props){
    super(props);

    this.openSocialDialog = this.openSocialDialog.bind(this);
  }

  openSocialDialog(evt){
    const type = evt.target.getAttribute('data-type');
    const postTitle = this.props.singlePost ? (this.props.singlePost.title ? this.props.singlePost.title.rendered : '') : '';

    if(type == 'fb'){
      window.open('https://www.facebook.com/sharer/sharer.php?u=' + location.href, 'sharer', 'width=626,height=436');
    }else if(type == 'twitter'){
      window.open('https://twitter.com/share?url=' + location.href + '&text=' + postTitle, 'sharer', 'width=626,height=436');

    }else if(type == 'linkedin'){
      window.open('https://www.linkedin.com/shareArticle?mini=true&url=' + location.href, 'sharer', 'width=626,height=436');
    
    }else if(type == 'googleplus'){
      window.open('https://plus.google.com/share?url=' + location.href, 'sharer', 'width=626,height=436');

    }

  }

  render(){
    const { classNameUl, classNameLi, } = this.props;
   
    return <div id="post-share" className="post-social-share post-widget widget">
    			<div className="widget-body">
	                <ul className="list-unstyled text-upper mb20">
    			      		<li className="item">
                      <a className="fb-bg" 
                        onClick={this.openSocialDialog } href="javascript: void(0)" target="_blank" 
                      title="The CEO Magazine Australia Facebook"><i data-type="fb" className="fa fa-social-facebook"></i></a>
                    </li>
    			      		<li className="item">
                      <a className="twitter-bg" 
                          onClick={this.openSocialDialog } href="javascript: void(0)" target="_blank" 
                     title="The CEO Magazine Australia Twitter"><i data-type="twitter" className="fa fa-social-twitter"></i></a></li>
    			      		<li className="item">
                      <a className="linkedin-bg" 
                          onClick={this.openSocialDialog } href="javascript: void(0)" target="_blank"  
                          title="The CEO Magazine Australia LinkedIn"><i data-type="linkedin" className="fa fa-social-linkedin"></i></a></li>
    			      		<li className="item">
                      <a className="google-plus-bg" 
                      onClick={this.openSocialDialog } href="javascript: void(0)" target="_blank"  
                      title="The CEO Magazine Australia Google+"><i data-type="googleplus" className="fa fa-social-google-plus"></i></a></li>
 
    			      	</ul>
			    </div>
                <style global jsx>{`
                   .post-social-share {
                   	padding: 0px 0px 0px;
                   } 
                   .post-social-share .widget-title {
                   	color: #6D7784;
                   		line-height: 22px;
                   		padding-bottom: 5px;
                   }
                   .post-social-share a {
                   	display: block;
                   	text-align: center;
                   }
                   .post-social-share ul {
                   	overflow: hidden;
                   }
                   .post-social-share .item {
                   	padding: 0px;
                   	padding-right: 10px; 
                   	float: left;
                   }
                   .post-social-share .fa {
                   	font-size: 25px;
                   	width: 40px;
                   	height: 36px;
                   	line-height: 36px;
                   	color: #FFF;
                   }
                   @media ( max-width: 960px ) {
	                   	.post-social-share {
							padding: 20px 10px 10px;
							margin-bottom: 10px;
	                   	}
	                   	.post-social-share ul {
	                   		margin: 0px auto;
	                   		display: table;
	                   	}
	                   	.post-social-share .widget-title {
	                   		display: none;
	                   	}
                   } 

                   
                `}</style>
            </div>

  }
}