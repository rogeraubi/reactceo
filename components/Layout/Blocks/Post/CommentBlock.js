import { Component, PureComponent } from 'react';
import helper from '../../../../helpers/helper'
import service from '../../../../helpers/service'
import { Grid, Row, Col, Image} from 'react-bootstrap';
import { slide as Menu } from 'react-burger-menu'
import ReCAPTCHA from "react-google-recaptcha";
import { ClipLoader } from 'react-spinners';

export default class CommentBlock extends Component{

  constructor(props){
    super(props);
    this.state = {
      commentsChildren: [],
      authorName: [], 
      authorEmail: [], 
      authorContent: [],
      submitCommentResponseText: [],
      commentsFormActive: {},
      captchaResponse: "",
      submittingComment: []
    }

    this.submitComment = this.submitComment.bind(this);
    this.updateInput = this.updateInput.bind(this);
    this.showCommentForm = this.showCommentForm.bind(this);
    this.captchaOnChange = this.captchaOnChange.bind(this);

    this.state.authorName[0] = "";
    this.state.authorEmail[0] = "";
    this.state.authorContent[0] = "";
    this.state.submitCommentResponseText[0] = "";
    this.state.submittingComment[0] = false;
  }

  componentDidMount(){

  }

  updateInput(event, index){
    const name = event.target.name;
    const value = event.target.value;
    const commentId = event.target.getAttribute('comment-id');
    const tempHolder = this.state[name];

    tempHolder[commentId] = value;
    this.setState({[name] : tempHolder})  
  }

  showCommentForm(event){
    const commentId = event.target.getAttribute('comment-id')
    const targetComment = this.state.commentsFormActive[commentId];

    if(targetComment){
      let authorName = this.state.authorName;
      let authorEmail = this.state.authorEmail;
      let authorContent = this.state.authorContent;
      let submitCommentResponseText = this.state.submitCommentResponseText;

      authorName[commentId] = "";
      authorEmail[commentId] = "";
      authorContent[commentId] = "";
      submitCommentResponseText[commentId] = "";

      this.setState({
        authorName: authorName,
        authorEmail: authorEmail,
        authorContent: authorContent,
        submitCommentResponseText: submitCommentResponseText
      })
    }
      
      let tempCommentsForm = this.state.commentsFormActive;
      for(let i in tempCommentsForm){
        tempCommentsForm[i] = false;  
      }
      tempCommentsForm[commentId] = !targetComment;

      this.setState({
        commentsFormActive: tempCommentsForm
      })
      if(window.grecaptcha){
        window.grecaptcha.reset();  
      }
  }

  captchaOnChange(value){
    this.setState({
      captchaResponse: value
    })
  }

  submitComment(evt){
    evt.preventDefault();

    const self = this;
    const parentId = evt.target.getAttribute('comment-id');

    let authorName = this.state.authorName[parentId];
    let authorEmail = this.state.authorEmail[parentId];
    let authorContent = this.state.authorContent[parentId];
    let submittingComment = this.state.submittingComment;

    submittingComment[parentId] = true;
    this.setState({ submittingComment: submittingComment });



    let tempResponse = this.state.submitCommentResponseText;
    let submittingCommentError = this.state.submittingComment;

    if(authorName == "" || authorEmail == "" || authorContent == ""){
      tempResponse[parentId] = <p style={{color:'red'}}>All fields are required</p>;

      submittingCommentError[parentId] = false;
      this.setState({ submitCommentResponseText: tempResponse, submittingComment: submittingCommentError });
      return false;
    }else{
      tempResponse[parentId] = "";

      this.setState({ submitCommentResponseText: tempResponse })
    }
    if(this.state.captchaResponse == ""){
      tempResponse[parentId] = <p style={{color:'red'}}>Please validate captcha!</p>;
      submittingCommentError[parentId] = false;
      this.setState({ submitCommentResponseText: tempResponse, submittingComment: submittingCommentError });
      return false;
    }

    service.postComment({
      authorName: authorName,
      authorEmail: authorEmail,
      content: authorContent,
      postId: this.props.postId,
      parentId: parentId,
      gRecaptchaResponse: this.state.captchaResponse
    }).then(function (response) {
        //handle success
        let respAuthorName = self.state.authorName;
        let respAuthorEmail = self.state.authorEmail;
        let respAuthorContent = self.state.authorContent;
        let submitCommentResponseText = self.state.submitCommentResponseText;
        let respCommentsFormActive = self.state.commentsFormActive;
        
        let submittingCommentResponse = self.state.submittingComment;

        if(response && response > 0){
          respAuthorName[parentId] = "";
          respAuthorEmail[parentId] = "";
          respAuthorContent[parentId] = "";
          respCommentsFormActive[parentId] = false;

          submittingCommentResponse[parentId] = false;

          if(parentId && parentId > 0){
            submitCommentResponseText[parentId] = <p style={{color:'green'}} className="post-comment-submitted">Thanks for submitting your reply! Our moderators will review it soon.</p>;
          }else{
            submitCommentResponseText[parentId] = <p style={{color:'green'}} className="post-comment-submitted">Thanks for submitting your comment! Our moderators will review it soon.</p>;
          }
          

          self.setState({
              submitCommentResponseText: submitCommentResponseText,
              authorName: respAuthorName,
              authorContent: respAuthorEmail,
              authorEmail: respAuthorContent,
              commentsFormActive: respCommentsFormActive,
              captchaResponse: "",
              submittingComment: submittingCommentResponse
            })
        }else{
          submitCommentResponseText[parentId] = <p style={{color:'red'}} dangerouslySetInnerHTML={{__html: response }}/>;
          submittingCommentResponse[parentId] = false;
          self.setState({
            submitCommentResponseText: submitCommentResponseText,
            submittingComment: submittingCommentResponse
          })
        }
    })
    .catch(function (err) {
        console.log('Error submitting comment');
        console.log(err);
    });
  }

  render(){
    const layoutStyle = {
        
    }
        
    const { classNameUl, classNameLi, comments } = this.props;
    let commentsStructured = [];
   
    for(let i in comments){
      const comment = comments[i];


    }

    let commentsText = "";

    if(comments && comments.length > 0){
      commentsText = comments.map(comment => {
          if(!this.state.commentsFormActive[comment.id] || this.state.commentsFormActive[0]){
            this.state.commentsFormActive[comment.id] = false;  
          }
          if(!this.state.submitCommentResponseText[comment.id]){
            this.state.submitCommentResponseText[comment.id] = "";
          }
          if(!this.state.authorName[comment.id]){
            this.state.authorName[comment.id] = "";
          }
          if(!this.state.authorEmail[comment.id]){
            this.state.authorEmail[comment.id] = "";
          }
          if(!this.state.authorContent[comment.id]){
            this.state.authorContent[comment.id] = "";
          }
          if(!this.state.submittingComment[comment.id]){
            this.state.submittingComment[comment.id] = false;
          }
          
          const commentFormDiv = <CommentForm commentsFormActive={ this.state.commentsFormActive } 
                                                comment-id={comment.id} 
                                                updateInput={this.updateInput} 
                                                showCommentForm={this.showCommentForm} 
                                                captchaOnChange={this.captchaOnChange}
                                                submitCommentResponseText={this.state.submitCommentResponseText}
                                                authorName={this.state.authorName} 
                                                submitComment={this.submitComment} 
                                                authorEmail={this.state.authorEmail} 
                                                authorContent={this.state.authorContent}
                                                submittingComment={this.state.submittingComment}
                                                formDivClass={"div-reply-form"}
                                                  />;
          
          let commentDiv =  <li key={comment.id} className={"item " + (comment.parent > 0 ? " item-child" : "")}>
                                        <span className="comment-meta">Added <span itemProp="commentTime">{ helper.formatDate({date: comment.date}) }</span>, By <span itemProp="name" dangerouslySetInnerHTML={{__html: comment.author_name }} /></span>
                                        <div itemProp="commentText" dangerouslySetInnerHTML={{__html: comment.content.rendered }}/>
                                        <span className="btn-comment-reply" comment-id={comment.id} onClick={this.showCommentForm}>Reply<i className="fa fa-reply"></i></span>
                                        {comment.parent <= 0 ? commentFormDiv : ''}
                                        <ul className="list-unstyled">
                                            {comment.parent > 0 ? commentFormDiv : ''}
                                            {this.state.commentsChildren[comment.id]}
                                        </ul>
                                      </li>
            if(comment.parent && comment.parent > 0){
              let commentChildTemp = this.state.commentsChildren;
              commentChildTemp[comment.parent] = commentDiv;
            }else{
              return commentDiv
            }
          });
    }

	let commentCountText = '';
	let commentCountTitle = '';
  if ( comments.length > 0 ) {
		commentCountTitle = '(' + comments.length + ') Comments';
	}else{
    commentCountTitle = 'Article Comment';
    commentCountText = <p className={"nocomment" + (this.state.commentsFormActive[0] ? " hidden" : "") }>No comment(s) has been added, be the first to have your say.</p>;
  }

   const listOpacity = this.state.commentsFormActive[0] ? 0.5 : 1;

   // <ul style={{opacity: listOpacity}} className="list-comments list-unstyled" >  itemProp="discusses" itemScope="itemscope" itemType="http://schema.org/UserComments"
    return <Menu id={ "comment-sidebar" } 
    			width={ 320 }
                className={ "post-comment-wrap" } 
                menuClassName="post-comment-inner widget" 
                isOpen={ this.props.isOpen } 
                customBurgerIcon={ false } right
				        onStateChange={ this.props.onStateChange }
                      //customBurgerIcon={ false }
                      >
              <div className="widget-title">{commentCountTitle}</div>

			   <div className="widget-body">
			   	{commentCountText}
			   	
			   	<div className="post-comment-add-wrap">
	              
                   <CommentForm commentsFormActive={ this.state.commentsFormActive } 
                                comment-id={0} 
                                captchaOnChange={this.captchaOnChange}
                                updateInput={this.updateInput} 
                                showCommentForm={this.showCommentForm} 
                                submitCommentResponseText={this.state.submitCommentResponseText}
                                authorName={this.state.authorName} 
                                submitComment={this.submitComment} 
                                authorEmail={this.state.authorEmail} 
                                submittingComment={this.state.submittingComment}
                                authorContent={this.state.authorContent}
                                  />                        
                  { !this.state.commentsFormActive[0] ? <button comment-id={0} className="btn btn-main btn-block btn-lg post-comment-add-btn" onClick={ this.showCommentForm }>Leave A Comment</button>
                                            : '' }
	            	</div>
	              <ul style={{opacity: listOpacity}} className="list-comments list-unstyled" > 
	              	{ commentsText }
	              </ul>
	          </div>
              <style global jsx>{`
              	
				.post-view #post-comment {
					border-bottom: 1px solid #EEE;
					padding-bottom: 10px;
					padding-top: 10px;
					border-top: 1px solid #EEE;
				}

           		.post-comment-btn {
					background: none;
					border: 0px;
					line-height: 40px;
					display: block;
					width: 100%;
					text-align: left;
					position: relative;
					color: #002A45;
					padding: 0px 0px 0px 35px;
					font-size: 20px;
					font-style: italic;
					line-height: 40px;
           		}
           		.post-comment-btn::before {
           			content: "\f018";
           			font-family: "Global";
           			position: absolute;
           			left: 5px;
           			line-height: 46px;
           			font-size: 24px;
           			font-style: normal;
           		}
           		.post-comment-btn .fa-comment, .post-comment-btn .fa-comment-btm {
           			font-size: 40px;
           			float: left;
           		}

           		.post-comment-btn .post-comments-count {

           		}
           		.post-comment-btn .post-comment-txt {
           			padding-left: 8px;
           		}
           		.post-comment-btn:hover .post-comment-txt {
           			text-decoration: underline;
           		}
				 @media ( max-width: 960px) {
				 	.post-view #post-comment {
				 		display: table; 
				 		margin: 0 auto;
				 		border: 0px;
				 		padding: 20px 0px 20px !important;
				 	}
				 	#post-comment .post-comment-btn {
				 		padding-left: 60px;
				 		padding-right: 20px;
				 		background: #002A45;
				 		color: #FFF;
				 	}
				 	#post-comment .post-comment-btn::before {
				 		left: 25px;
				 	}
				 }
				 
                 .post-comment-wrap {
                    top: 60px;
                    right: 0px;
                 }
                 .post-comment-inner {
                 		background: #FFF !important;
                 }
                 .post-comment-wrap .widget-title {
                 		background: #6D7784;
                 		padding-left: 15px;
                 		color: #FFF;
                 		position: absolute;
                 		right: 0px;
                 		left: 0px;
                 		top: 0px;
                 }
                 .post-comment-wrap .bm-cross-button {
                 	top: 0px !important;
                 }
                 .post-comment-wrap .bm-cross {
                 		background: #FFF;
                 		width: 2px !important;
                 		height: 24px !important;
                 }
                 .post-comment-wrap .widget-body {
                 	padding-top: 40px;
                 }
                 .post-comment-wrap .widget-body .nocomment, .post-comment-wrap  .post-comment-submitted {
                 	padding: 15px 10px 0px;
                 	font-size: 16px;
                 	line-height: 22px;
                 }
                 .post-comment-wrap .post-comment-add-wrap {
                 	padding: 15px 15px 10px;
                 }
                 .post-comment-wrap .post-comment-add-wrap h3 {
                 	margin-top: 0px;
                 	font-size: 16px;
                 }
                 .post-comment-wrap .comment-meta {
                 	display: block;
                 	color: #002A45;
                 	font-size: 12px;
                 	font-weight: bold;
                 	line-height: 20px;
                 	margin-bottom: 12px;
                 }
                 .post-comment-wrap .list-comments {
                 	font-size: 14px;
                 	line-height: 22px;
                 	padding-bottom: 60px;
                 }
        				 .post-comment-wrap .list-comments .item {
        				 	border-top: 1px dashed #CCC;
        				 	padding: 15px;
        				 }
        				 .post-comment-wrap .list-comments .item:first-child {
        				 	border: 0px;
        				 }
                 .div-reply-form{
                  background: #eee;
                  padding: 10px;
                 }
                 .item-child{
                  margin-left: 10px;
                 }
                 .comment-btn-loader-cntr{
                  margin-left: 5px;
                  float: right; 
                 }
                 .btn-comment-reply{
                  color: #6D7784;
                  font-size: 12px;
                  text-transform: uppercase;
                  cursor: pointer;
                 }
                 .btn-comment-reply .fa-reply{
                  font-size: 15px;
                  margin-left: 2px;
                 }
              `}</style>
            </Menu>

  }
}


class CommentForm extends Component{
  render(){
    
    let submitButtonDisabled = "";
    if(this.props.submittingComment[this.props['comment-id']] == true){
      submitButtonDisabled = "disabled";
    }
    return <div>
            { this.props.submitCommentResponseText[this.props['comment-id']] != "" ? this.props.submitCommentResponseText[this.props['comment-id']] : ''}
              { this.props.commentsFormActive[this.props['comment-id']] == true ?
                <div className={this.props.formDivClass}>
                    <form key={0} comment-id={this.props['comment-id']} onSubmit={this.props.submitComment}>
                      <input  type="text" comment-id={this.props['comment-id']} onChange={this.props.updateInput} name="authorName" value={this.props.authorName[this.props['comment-id']]} placeholder="Your name" className="input-block" />
                      <input  type="email" comment-id={this.props['comment-id']} onChange={this.props.updateInput} name="authorEmail" value={this.props.authorEmail[this.props['comment-id']]} placeholder="Your email" className="input-block" />
                      <textarea rows="6"  comment-id={this.props['comment-id']} onChange={this.props.updateInput} name="authorContent" value={this.props.authorContent[this.props['comment-id']]} placeholder="Your comment" className="textarea-block" />
                      <ReCAPTCHA
                        ref={(el) => {this.captcha = el; }}
                        sitekey="6LcSJwwUAAAAAATwpX-VpAyojkiomAtdJUkc1d2T"
                        onChange={this.props.captchaOnChange}
                      />
                      <div className="btn-wrapper">
                        <input type="button" comment-id={this.props['comment-id']} onClick={ this.props.showCommentForm } value="Cancel" className="btn btn-default left" />
                        <button type="submit" comment-id={this.props['comment-id']} value="" className={ submitButtonDisabled + " btn btn-main right"} >Submit Comment<div className="comment-btn-loader-cntr"><ClipLoader color={'#ebedf1'} loading={ this.props.submittingComment[this.props['comment-id']] }  size={20} /></div></button>
                      </div>
                    </form>
                </div>
              : ''
             }
          </div>
  }
}