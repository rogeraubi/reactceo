import { PureComponent } from 'react';
import helper from '../../../../helpers/helper'
import { Grid, Row, Col, Image} from 'react-bootstrap';

export default class TagBlock extends PureComponent{

  render(){
    const { tags, classNameUl, classNameLi, } = this.props;

    if(!tags){
      return <div/>
    }
    
    let tagLiText = tags.map( tag => {
        return <li key={tag.id} className={ "item" + classNameLi}>
                    <a className="label label-main label-sm post-tag-item" href={tag.link} title={tag.name} ><span dangerouslySetInnerHTML={{__html:tag.name}}/></a>
                </li>
    });
    return <div id="post-tags-wrap">
                <ul className={"list-unstyled list-inline text-upper mb0 " + classNameUl}>{ tagLiText }</ul>
                <style global jsx>{`
                	#post-tags-wrap {
                		margin-bottom: 5px;
                	}
                	#post-tags-wrap .item {
                		padding-left: 5px;
                		padding-right: 0px;
                	}
                `}</style>
            </div>

  }
}