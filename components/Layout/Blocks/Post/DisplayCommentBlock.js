import { PureComponent } from 'react';
import helper from '../../../../helpers/helper'
import CommentBlock from './CommentBlock'

export default class DisplayCommentBlock extends PureComponent{

  constructor(props){
    super(props);

    this.state = {
            isBmCommentsOpen: false
    }
    this.showBmComments = this.showBmComments.bind(this);
    this.bmCommentChangeState = this.bmCommentChangeState.bind(this);
  }

   showBmComments(){
        this.setState({
            isBmCommentsOpen: !this.state.isBmCommentsOpen
        })
    }

    bmCommentChangeState(state){ 
        this.setState({
            isBmCommentsOpen: state.isOpen
        })
    }

  render(){
    const { comments, singlePost } = this.props;
    const commentsLength = comments.length;

    let commentsDisplay = <button className="post-comment-btn" onClick={ this.showBmComments }><span className="post-comment-txt">Leave Comment <i className="fa fa-chevron-right"></i></span></button>;
        
    if ( commentsLength > 0 ) {
        commentsDisplay = <button className="post-comment-btn" onClick={ this.showBmComments }><span className="post-comments-count">({commentsLength})</span><span className="post-comment-txt">Comment{commentsLength > 1 ? "s" : ""} <i className="fa fa-chevron-right"></i></span></button>;
    }

    return <div >
                { commentsDisplay }
                <CommentBlock postId={singlePost.id} onStateChange={ this.bmCommentChangeState } isOpen={this.state.isBmCommentsOpen} comments={comments}/>  
            </div>

  }
}