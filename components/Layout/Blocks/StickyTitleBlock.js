import { Component } from 'react';
import helper from '../../../helpers/helper'

export default class StickyTitleBlock extends Component{

  constructor(props){
    super(props);
    this.state = {
         headerFixed: false,
         reachedBottomArticle: false
    }
  }

  componentDidMount(){

    let headerPosition = helper.getElemDistance( document.querySelector('#post-header') ) -60;
    headerPosition = headerPosition + ( document.getElementById('post-header') ? document.getElementById('post-header').offsetHeight : 0);
    
    document.addEventListener('scroll', () => { 
                    
        let articleBodyBottomPosition = document.getElementById('content-entry') ? document.getElementById('content-entry').getBoundingClientRect().bottom : 0;
        let windowPosition = window.pageYOffset;
        
        if ( windowPosition > headerPosition && articleBodyBottomPosition > 301 ) { 
              this.setState({
                headerFixed: true,
                reachedBottomArticle: false
             })
        } else if ( windowPosition > headerPosition && articleBodyBottomPosition < 300 ){
            this.setState({
                headerFixed: false,
                reachedBottomArticle: true
            })
        } else {
            this.setState({
                headerFixed: false,
                reachedBottomArticle: false
            })
        }
    });
  }
  
  render(){
    const { singlePost } = this.props;
    return <div > 
            <div id="header-fixed-wrap" className={(this.state.headerFixed ? "header-fixed fadeIn " : '') + (this.state.reachedBottomArticle ? " header-fixed-bottom fadeOut " : "") + "hidden header-fixed-wrap"}>
                <div className="header-fixed-title" dangerouslySetInnerHTML={{__html: singlePost.title.rendered }} />
                <div className="header-fixed-lead" dangerouslySetInnerHTML={{__html: singlePost.excerpt.rendered }}/>
                {/*<div className="header-fixed-author post-meta">By {singlePost.custom_meta.author.post_title}</div>*/}
            </div>
            <style global jsx>{`
               @media ( min-width: 961px ) {
                        #entry-sidebar-after {
                            top: 0px;
                        } 
                    }
                    
                   .fadeIn {
                      -webkit-animation-name: fadeIn;
                      animation-name: fadeIn;
                      -webkit-animation-duration: 1s;
                      animation-duration: 1s;
                      -webkit-animation-fill-mode: both;
                      animation-fill-mode: both;
                      }
                      @-webkit-keyframes fadeIn {
                      0% {opacity: 0;}
                      100% {opacity: 1;}
                      }
                      @keyframes fadeIn {
                      0% {opacity: 0;}
                      100% {opacity: 1;}
                    } 
            `}</style>
          </div>
  }
}