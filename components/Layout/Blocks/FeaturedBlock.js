import { PureComponent } from 'react';
import helper from '../../../helpers/helper'
import { Grid, Row, Col, Image} from 'react-bootstrap';
import ArchiveListBlock from './ArchiveListBlock';
import ArchiveSingleBlock from './ArchiveSingleBlock';
import GoogleDoubleClick from '../../Advertising/GoogleDoubleClick'

export default class FeaturedBlock extends PureComponent{

  render(){
        const { posttype }  = this.props;
    if  (posttype == "events") {

        const { archiveList, adslot, adId, posttype,marchiveList,mexperienceList,mprivateList}  = this.props;


        return <div className="section-featured-post" >
            <ul className="row widget-pab list-unstyled">
                <ArchiveSingleBlock showAuthor="true" showType="true" showExcerpt="true" classNamesLi="col-sm-8 col-xs-12 featured-post-pri" archive={marchiveList[0]} imgSize="full" posttype={posttype}/>
                <li className="col-sm-4 col-xs-12 featured-post-sec">
                    <ul className="list-unstyled">
                        <ArchiveSingleBlock showType="true" showExcerpt="true" archive={marchiveList[1]} imgSize="mediumwide" posttype = {posttype} />

                        <GoogleDoubleClick adslot={helper.displayAds({posttype:posttype}).mrec}
                                           type="mrec" id={helper.displayAds({posttype:posttype}).mrecId} width={300} height={250} />
                    </ul>
                </li>
            </ul>
            <ul className="row widget-pab-table widget-pab list-unstyled">
                <ArchiveSingleBlock classNamesLi="col-sm-4 col-xs-12 featured-post-block" showType="true" showExcerpt="true" archive={marchiveList[2]} imgSize="mediumwide"  posttype = {posttype}/>
                <ArchiveSingleBlock classNamesLi="col-sm-4 col-xs-12 featured-post-block" showType="true" showExcerpt="true" archive={mexperienceList[0]} imgSize="mediumwide" posttype = {posttype} />
                <ArchiveSingleBlock classNamesLi="col-sm-4 col-xs-12 featured-post-block" showType="true" showExcerpt="true" archive={mprivateList[0]} imgSize="mediumwide" posttype = {posttype}/>
            </ul>
            <style global jsx>{`
    				.widget-pab-table .widget-inner {
    					position: relative;
    					height: 100%;
    				}
    				.linkButton {
    					margin-left:9%;
    					margin-bottom:10px;
    					}
    				.section-featured-post .featured-post-block .post-title {
    					margin-bottom: 0px;
    				}
    				.section-featured-post .post-label {
    					margin-bottom: 10px;
    				}

    				@media ( min-width: 961px ) {
    					.widget-pab-table {
    					    display: flex;
    					}
    					.widget-pab-table .widget-item {
    						position: relative;
    					}
    					.widget-pab-table .widget-inner {
    						padding-bottom: 30px;
    					}
    					.widget-pab-table .widget-body {
    						padding-bottom: 5px;
    					}

    					.widget-pab-table .post-meta {
    						position: absolute;
    						bottom: 0px;
    						left: 0px;
    					}

    					.featured-post-pri .post-title {
    						font-size: 28px;
    						font-weight: bold;
    						line-height: 30px;
    					}
    					.featured-post-pri .widget-body {
    						padding: 15px 0px;
    					}

    				}
    				@media ( max-width: 767px ) {
    					.section-featured-post .widget-adv-mrec {
    						padding-bottom: 15px;
    					}
    				}
            	`}</style>
        </div>



    }
    else {
        const { archiveList, adslot, adId, posttype }  = this.props;
        return <div className="section-featured-post" >
            <ul className="row widget-pab list-unstyled">
                <ArchiveSingleBlock showType="true" showExcerpt="true" classNamesLi="col-sm-8 col-xs-12 featured-post-pri" archive={archiveList[0]} imgSize="full" />
                <li className="col-sm-4 col-xs-12 featured-post-sec">
                    <ul className="list-unstyled">
                        <ArchiveSingleBlock showType="true" showExcerpt="true" archive={archiveList[1]} imgSize="mediumwide" />
                        <GoogleDoubleClick adslot={helper.displayAds({posttype:posttype}).mrec}
                                           type="mrec" id={helper.displayAds({posttype:posttype}).mrecId} width={300} height={250} />
                    </ul>
                </li>
            </ul>
            <ul className="row widget-pab-table widget-pab list-unstyled">
                <ArchiveSingleBlock classNamesLi="col-sm-4 col-xs-12 featured-post-block" showType="true" showExcerpt="true" archive={archiveList[2]} imgSize="mediumwide" />
                <ArchiveSingleBlock classNamesLi="col-sm-4 col-xs-12 featured-post-block" showType="true" showExcerpt="true" archive={archiveList[3]} imgSize="mediumwide" />
                <ArchiveSingleBlock classNamesLi="col-sm-4 col-xs-12 featured-post-block" showType="true" showExcerpt="true" archive={archiveList[4]} imgSize="mediumwide" />
            </ul>
            <style global jsx>{`

    				.widget-pab-table .widget-inner {
    					position: relative;
    					height: 100%;
    				}
    				.section-featured-post .featured-post-block .post-title {
    					margin-bottom: 0px;
    				}
    				.section-featured-post .post-label {
    					margin-bottom: 10px;
    				}

    				@media ( min-width: 961px ) {
    					.widget-pab-table {
    					    display: flex;
    					}

    					.widget-pab-table .widget-item {
    						position: relative;
    					}
    					.widget-pab-table .widget-inner {
    						padding-bottom: 30px;
    					}
    					.widget-pab-table .widget-body {
    						padding-bottom: 5px;
    					}

    					.widget-pab-table .post-meta {
    						position: absolute;
    						bottom: 0px;
    						left: 0px;
    					}

    					.featured-post-pri .post-title {
    						font-size: 28px;
    						font-weight: bold;
    						line-height: 30px;
    					}
    					.featured-post-pri .widget-body {
    						padding: 15px 0px;
    					}

    				}
    				@media ( max-width: 767px ) {
    					.section-featured-post .widget-adv-mrec {
    						padding-bottom: 15px;
    					}
    				}
            	`}</style>
        </div>
    }
  }
}
