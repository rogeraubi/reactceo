import { PureComponent } from 'react';
import { Image } from 'react-bootstrap';
import helper from '../../../helpers/helper'
import ArchiveSingleBlock from './ArchiveSingleBlock'

export default class PostListBlock extends PureComponent{
  
  render(){
    const { postList, classNamesUl, classNamesLi  } = this.props;

    if(!postList.length || postList.length < 1){
      return <div>Empty list</div>
    }

    let postListText = postList.map(post => {
        return <ArchiveSingleBlock {...this.props} archive={ post } />
                  
    });

    return <ul className={ classNamesUl + " list-unstyled"} >
              { postListText }
           </ul>
  }
}
