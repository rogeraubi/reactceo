import { PureComponent } from 'react';
import ArchiveSingleBlock from './ArchiveSingleBlock'

export default class ArchiveListBlock extends PureComponent{

  render(){
    const { archiveList, classNamesUl, classNamesLi, archiveLimit,posttype} = this.props;

    if(!archiveList || !archiveList.length || archiveList.length < 1){
      return <div></div>
    }
    
    let theCount = 0;
  	let archiveText = archiveList.map(archive => {
  			theCount = theCount + 1;
  			if ( !archiveLimit || archiveLimit >= theCount ) {
				return <ArchiveSingleBlock {...this.props} key={archive.id} count={theCount} archive={archive} posttype = {posttype}/>
			}
		})
		
  	   return <ul className={ classNamesUl + " list-unstyled"} >
			  {archiveText}
		   </ul>
  }
}
