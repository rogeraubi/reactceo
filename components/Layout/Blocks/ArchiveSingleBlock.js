import { PureComponent } from 'react';
import helper from '../../../helpers/helper'
import { Grid, Row, Col, Image} from 'react-bootstrap';
import TagBlock from '../Blocks/Post/TagBlock'
import PropTypes from 'prop-types';

export default class ArchiveSingleBlock extends PureComponent{

  render(){
    let { archive, classNamesLi, count, imgSize, showReadMore, readMoreText,
        headingType, showExcerpt, showDate, showAuthor, showType, showAddress, showExecRole,showTags,posttype} = this.props;

    if(!archive){
        return '';
    }
    classNamesLi = classNamesLi ? ' ' + classNamesLi  : '';

    const archivePostUrl = helper.getReplacedUrlLink(archive.link)
    const media = helper.structureFeaturedMedia(archive);

    const img = helper.getImageUrl(media, imgSize); //default post-thumbnail, pass second param for type - eg: thumbnail, small
    const imgSrcUrl = img ? img.source_url : '';
    const imgSrcSet = helper.getImageSrcSet(media);

	//readmore
	let readMore = '';
	if ( showReadMore == "true" ) {
		let readMoreText = readMoreText || 'Read More';
		readMore = <span className="post-action btn btn-main text-upper"><strong>{readMoreText}</strong></span>
	}
	//define title type
	let itemTitle = <h2 className="post-title" dangerouslySetInnerHTML={{__html: helper.replaceRegex(archive.title.rendered) }}/>
	if ( headingType && headingType == 'h3') {
		itemTitle = <h3 className="post-title" dangerouslySetInnerHTML={{__html: helper.replaceRegex(archive.title.rendered) }} />
	}
	//show excerpt
	let itemExcerpt = '';
	if ( showExcerpt != "false" ) {
		itemExcerpt = <div className="post-excerpt" dangerouslySetInnerHTML={{__html: archive.excerpt.rendered }} />
	}
	//show date
	let itemDate = '';
	if ( showDate == "true" ) {
		itemDate = <span className="post-meta">{helper.formatDate({date: archive.date})}</span>
	}
	//show author
	let itemAuthor = '';
	if ( showAuthor !== "false") {
		if( posttype !== "press" &&  posttype !== 'authors' ){
			  if (archive.custom_meta.author) {
				  itemAuthor = <span className="post-meta"><a href={archive.custom_meta.author.link}>By <span
					  className="post-author">{archive.custom_meta.author.post_title}</span></a></span>
			  }
        }
	}


	let itemTypeLink = ''
	if ( showType == "true" ) {
		itemTypeLink = <div className="post-label"><a className="label label-main" title={archive.type}  href={"/" + archive.type}>{archive.type}</a></div>
	}

	//COMPANY DIRECTORY
	let companyAddress = '';
	if ( archive.type == 'company-directory' ) {
		// address
		if ( showAddress != "false" ) {
			if ( archive.custom_meta.custom_meta_company_address ) {
				companyAddress = <div className="post-company-address"><span className="post-meta">{archive.custom_meta.custom_meta_company_address}</span></div>
			}
			if ( archive.custom_meta.custom_meta_company_address && archive.custom_meta.custom_meta_company_address2 ) {
				companyAddress = <div className="post-company-address"><p className="post-meta"><strong className="post-company-address-txt">{archive.custom_meta.custom_meta_company_address}</strong>
					<span className="post-meta post-company-address-txt post-company-address2-txt">{archive.custom_meta.custom_meta_company_address2}</span></p></div>
			}
		}
	}
	// EXEC INTERVIEWS
	let ExecRole;
	if ( archive.type == 'executive-interviews' ) {
		if ( showExecRole == "true" ) {
			ExecRole = <div className="post-exec-company"><span className="post-meta">{archive.custom_meta.custom_meta_execinterviews_role}</span></div>
			// itemAuthor = "";
		}
	}

	// show tags
	const tags = helper.structurePostTags(archive);

	let itemTags = '';
	if ( showTags == "true" ) {
		itemTags = <TagBlock classNameUl="post-meta" classNameLi="" tags={tags}/>
	}

    if(!archive){
      return <p>Empty archive</p>
    }

	if ( !count ) {
		count = '1';
	}
    const mediaTitle = media.title ? media.title.rendered : '';
	let authorsFrame = '';
	if (posttype=="authors"){
		authorsFrame =  <style global jsx>{`
				    	.widget-inner {
				    		height: 300px;
				    	}
				    `}</style>
	}
  	return <li className={ "widget-item" + " widget-item-" + archive.id + classNamesLi + " widget-item-no" + count } >
      			<div className="widget-inner">
        	  				<div className={ "post-img post-imgsize-" + imgSize }>
        	            		<a key={archive.id} href={archivePostUrl} title={helper.replaceRegex(archive.title.rendered)} >
                                    <Image  src={ imgSrcUrl } alt={media.alt_text} title={mediaTitle} responsive />
                                </a>
        	            	</div>
        	            	<div className="widget-body">
        	            		{itemTags}
        	            		{itemTypeLink}
        	              		<a key={archive.id} href={archivePostUrl} title={helper.replaceRegex(archive.title.rendered)} >
                                    {itemTitle}
                                </a>
        	              		{itemDate}
        	              		{ExecRole}
        	              		{itemExcerpt}
        	              		{itemAuthor}
    							{companyAddress}
    							{readMore}
        	        		</div>

    	        </div>
		     {authorsFrame}
           </li>
  }
}
ArchiveSingleBlock.propTypes = {
    imgSize: PropTypes.string.isRequired,
    showReadMore: PropTypes.string,
    id: PropTypes.string,
    type: PropTypes.string,
    showTags: PropTypes.string
}
