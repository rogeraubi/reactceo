import { Component } from 'react';
import ArchiveListBlock from './ArchiveListBlock'
import service from '../../../helpers/service'
import { inject, observer } from 'mobx-react'
import { ClipLoader } from 'react-spinners';

@inject('store') @observer
export default class ArchiveLoadMore extends Component{

  constructor(props){
    super(props);
    this.state = {
    	offset: 9,
    	isLoading: false,
    	nextLoadMore: [],
    	stillHasData: true
    }
    this.loadMoreArchiveList = this.loadMoreArchiveList.bind(this);
    this.fetchArchive = this.fetchArchive.bind(this);
  }

  async componentDidMount(){
  	const tempNext = await this.fetchArchive();
  	if(tempNext[0]){
  		this.setState({
  			nextLoadMore: tempNext
  		})
  	}else{
  		this.setState({
  			stillHasData: false
  		})
  	}
  }

  async fetchArchive(){
  	let loadMoreData = [];
  	if(this.props.store.posttype == 'search'){
  		const loadMoreDataPromise = await service.getArchive('search', {q: this.props.searchText, offset: this.state.offset, limit: 9});
      loadMoreData = await loadMoreDataPromise.json();
  	}else{
  		const loadMoreDataPromise = await service.getArchive( this.props.store.posttype, {offset: this.state.offset, per_page:9});
      loadMoreData = await loadMoreDataPromise.json();
  	}
	   this.setState({
		    offset: (this.state.offset + 9)
	   })
	   return loadMoreData;
  }

  async loadMoreArchiveList(){

  	this.setState({
		  isLoading: true
	  })

  	const currentArchiveList = this.props.store.archiveList;
    const totalArchiveList = currentArchiveList.concat(this.state.nextLoadMore);
    this.props.store.setArchive(totalArchiveList);
    
    const newTemp = await this.fetchArchive();
    
    if(newTemp[0]){
  		this.setState({
  			nextLoadMore: newTemp,
			  isLoading: false
  		})
  	}else{
  		this.setState({
  			stillHasData: false,
			  isLoading: false
  		})
  	}
  }

  render(){

  	return <div className="btn-more-wrap clear">
			     { ( this.props.store.archiveList.length > 0 && this.state.stillHasData) ? 
              <button className="text-upper btn btn-lg btn-main" onClick={this.loadMoreArchiveList}>{this.state.isLoading ? <span>Loading  <ClipLoader color={'#123abc'}  loading={this.state.isLoading}  size={20} /></span> : <span>Load More <i className="fa fa-chevron-right"></i></span>}</button> 
              : '' }
			     <style global jsx>{`
						.btn-more-wrap {
							text-align: center;
							padding-bottom: 20px;
						}
						.btn-more-wrap .btn-main {
							font-weight: bold;
							width: 300px;
						}
				    `}</style>
			</div>
  }
}