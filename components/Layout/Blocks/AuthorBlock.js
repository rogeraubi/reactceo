import { PureComponent } from 'react';
import helper from '../../../helpers/helper'
import { Grid, Row, Col, Image} from 'react-bootstrap';

export default class AuthorBlock extends PureComponent{

  constructor(props){
    super(props);
    this.state ={
        isBioOpen: false
    }
    this.openAuthorBio = this.openAuthorBio.bind(this);
  }

  openAuthorBio(){
    let tempStatus = this.state.isBioOpen;

    this.setState({
        isBioOpen: !tempStatus
    });

  }

  render(){
    const {author} = this.props;

    if(!author){
      return <p>Empty author</p>
    }
    
	let authorLink = "";
	let authorTwitter = "";
	if(author.meta.custom_meta_logo_link){
		authorLink = <li className="item"><a href={author.meta.custom_meta_logo_link} target="_blank" title={author.post_title + " Website"}><i className="fa fa-link-variant"></i></a></li>;
	}
	if(author.meta.custom_meta_twitter){
		authorTwitter = <li className="item"><a href={"https://www.twitter.com/" + author.meta.custom_meta_twitter} target="_blank" title={author.post_title + " Twitter"}><i className="fa fa-social-twitter twitter"></i></a></li>
	}			      		
    
    
    return <div id="post-author-wrap" className={ (this.state.isBioOpen ? 'open' : '') + " post-author-wrapper post-widget widget" }>
    		<div className="widget-body">
	        	<a href={author.link}>
	        		<Image className="post-author-img img-circle" src={author.featured_thumbnail} responsive />
	        	</a>
	        	<div className="post-author-info">
	        		<strong className="post-author-name speakable-author">By <span itemProp="author">{author.post_title}</span></strong>
	        		<small onClick={ this.openAuthorBio } className="post-author-more cursor"><em className="text-upper">Full Bio</em><i data-event="open" className={(this.state.isBioOpen ? "fa-close" : "fa-chevron-down") + " fa" }></i></small>
	        	</div>
	        	<div className={ (this.state.isBioOpen ? '' : 'hidden') + " post-author-bio"} itemProp="publisher" itemScope="itemscope" itemType="http://schema.org/Organization">
					<a className="hidden" itemProp="url" href="http://www.theceomagazine.com">
		                <span itemProp="logo" itemScope="itemscope" itemType="https://schema.org/ImageObject">
		                   <img itemProp="url" src="//static.theceomagazine.net/content/web/ceomagazinelogo.png" alt="LOGO" />
		                </span>   
		                <span itemProp="name">The CEO Magazine</span>
		            </a>
	                <p dangerouslySetInnerHTML={{__html: author.post_excerpt }} />
	                <ul className="list-unstyled list-inline">
			      			{authorTwitter}
			      			{authorLink}
			      	</ul>
	            </div>
			 </div>
                
			    <style global jsx>{`
				    .post-author-wrapper.open {
				        background: #EEE;
				        border: 0px;
				        padding-bottom: 10px;
				        z-index: 1;
				    }
					.post-author-wrapper {
						padding: 15px 10px 20px;
						font-size: 14px;
						line-height: 21px;
						position: relative;
						margin-bottom: 10px !important;
					}
					.post-author-wrapper .post-author-img {
						width: 50px;
						float: left;
						padding: 0px;
						margin-bottom: 0px !important;
					}
					.post-author-wrapper p {
						margin: 0px 0px 10px !important;
					}
					.post-author-wrapper .post-author-info {
						padding-left: 65px;
						padding-top: 5px;
					}
					.post-author-wrapper .post-author-more {
						display: block;
					}
					.post-author-wrapper.open .post-author-more em {
						display: none;
					}
					.post-author-wrapper .post-author-bio {
						padding-top: 10px;
						clear: both;
					}
					.post-author-wrapper .post-author-bio .fa {
						font-size: 20px;
						line-height: 30px;
						width: 30px;
						display: block;
						height: 30px;
						text-align: center;
					}
					.post-author-wrapper .post-author-bio .item {
						padding: 0px 5px;
					}
					.post-author-wrapper .fa-close {
						position: absolute;
						right: 10px;
						top: 10px;
						font-size: 20px;
					}
					@media ( max-width: 960px ) {
						.post-view .post-author-wrapper {
							margin: 20px 15px 0px;
							background: #EEE;
						}
					}
			    `}</style>
            </div>

  }
}