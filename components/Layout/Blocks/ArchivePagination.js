import { Component } from 'react';

import service from '../../../helpers/service'
import config from '../../../helpers/config'
import { inject, observer } from 'mobx-react'
import Pagination from "react-js-pagination";

@inject('store') @observer
export default class ArchivePagination extends Component{

  constructor(props){
    super(props);
    this.state = {
        offset: 9,
        isLoading: false,
        nextLoadMore: [],
        stillHasData: true
    }
    this.paginationChange = this.paginationChange.bind(this);
    this.getPageUrl = this.getPageUrl.bind(this);
    this.getPaginatedUrl = this.getPaginatedUrl.bind(this);
  }

  getPaginatedUrl(pageNumber){
      const postKind = this.props.store.posttype;
      let postPage = Number(this.props.store.slug);
      let pageNumberUrl ="";
      let categoryUrl = "";
    if(this.props.store.category && this.props.store.category != "page"){
      categoryUrl = "/" + this.props.store.category;
      }

    if ((postKind ==="executive-interviews") && (this.props.store.slug) && (this.props.store.slug !="page") && (!(postPage > 0) || (postPage == NaN))){
        pageNumberUrl = '/'+this.props.store.slug+'/page/'+ pageNumber + '/';
      }
    else {
        pageNumberUrl = '/page/' + pageNumber + '/';
    }
    if(pageNumber == 1){
      pageNumberUrl = "";
    }

    if(this.props.store.archivePageActive == pageNumber){
      return "";
    }
      console.log(Number.isInteger(postPage));
      console.log(postPage);
    console.log(postKind);
     console.log(pageNumberUrl);

    return config.sitename + '/' + this.props.store.posttype + categoryUrl + pageNumberUrl;
  }
  paginationChange(pageNumber){
    window.location.href = this.getPaginatedUrl(pageNumber);
  }
   getPageUrl(pageNumber){
    return this.getPaginatedUrl(pageNumber);
  }

  render(){
      let itemsCountPageTemp = 0 ;

      if (this.props.store.posttype =="events"){
          itemsCountPageTemp = parseInt( this.props.store.archiveTotalPosts);
      }
      else {
          itemsCountPageTemp = 9;
      }
    return <div className="btn-more-wrap clear">
                <Pagination
                    activePage={parseInt(this.props.store.archivePageActive)}
                    itemsCountPerPage={9}
                    totalItemsCount={ parseInt(this.props.store.archiveTotalPosts) }
                    pageRangeDisplayed={10}
                    onChange={this.paginationChange}
                    getPageUrl={this.getPageUrl}
                  />
                 <style global jsx>{`
                        .btn-more-wrap {
                            text-align: center;
                            padding-bottom: 20px;
                        }
                        .btn-more-wrap .btn-main {
                            font-weight: bold;
                            width: 300px;
                        }
                    `}</style>
            </div>
  }
}
