import { PureComponent } from 'react';
import helper from '../../../helpers/helper'

export default class ArchiveFilterBlock extends PureComponent{

  constructor(props){
    super(props);
    this.state = {
      filterRegion: "",
      filterCategory: ""
    };
    this.filterChangeRe = this.filterChangeRe.bind(this);
    this.filterChangeCa = this.filterChangeCa.bind(this);
  }

  filterChangeRe(evt){
      const val = evt.target.value;
      let cate =helper.getPathVariable('executive-interviews',val);
      if(val != ""){
      this.setState({filterRegion:cate});
      //window.location.href = evt.target.value;
      }
  }
  filterChangeCa(evt){
        const val = evt.target.value;
        if  ((val != "") && this.state.filterRegion){
             window.location.href = evt.target.value+this.state.filterRegion;
        }
        else {
             window.location.href = evt.target.value;
        }
    }

  render(){
    const { subCategory, slug, archiveFilterRegion, archiveFilterCategory, classNamesUl, classNamesLi } = this.props;

    if(!archiveFilterRegion || !archiveFilterRegion.length || archiveFilterRegion.length < 1){
      return <div>Empty archive Filter Region</div>
    }

    if(!archiveFilterCategory || !archiveFilterCategory.length || archiveFilterCategory.length < 1){
      return <div>Empty archive Filter Category</div>
    }

    let archiveFilterRegionText = archiveFilterRegion.map(region => {
        let activeOption = "";
        if(slug == region.slug){
          activeOption = "selected";
        }
        return <option key={region.id} selected={activeOption} value={ helper.getReplacedUrlLink(region.link) } dangerouslySetInnerHTML={{__html: region.name }} />;
    })

    let archiveFilterCategoryText = archiveFilterCategory.map(category => {
        let activeOption = "";

        if(subCategory == category.slug){
          activeOption = "selected";
        }
        return <option key={category.id} selected={activeOption} value={ helper.getReplacedUrlLink(category.link) } dangerouslySetInnerHTML={{__html: category.name }} />;
    });
    return <div>
            <select onChange={this.filterChangeRe} > <option value="">Choose Region</option>    { archiveFilterRegionText } </select>
            <select onChange={this.filterChangeCa} > <option value="">Choose Category</option>  { archiveFilterCategoryText } </select>
          </div>
  }
}
