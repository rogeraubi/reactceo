import {PureComponent} from 'react';
import ArchiveListBlock from './Blocks/ArchiveListBlock'
import FeaturedBlock from './Blocks/FeaturedBlock'
import ModalBlock from './Blocks/ModalBlock'

import {Grid, Row, Col, Image} from 'react-bootstrap';
import ArchiveSingleBlock from './Blocks/ArchiveSingleBlock';

//@inject('store') @observer
export default class HomeInterviews extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            showModalVideo: false
        }
        this.modalShowVideo = this.modalShowVideo.bind(this);
    }

    modalShowVideo() {
        this.setState({
            showModalVideo: !this.state.showModalVideo
        })
    }

    render() {

        const {archiveList} = this.props;

        return <div className="page-home-wrapper">

            <Grid className="page-above">
                <Row>
                    <Col xs={12}>
                        <FeaturedBlock posttype="executive-interviews" classNamesLi="col-sm-4 col-xs-12"
                                       classNamesUl="widget-pab widget-pab-business row" imgSize="mediumwide"
                                       archiveList={archiveList}/>
                </Col>
                </Row>
            </Grid>

            <Grid className="section-front-exec-interviews page-below">
                <Row>
                    <Col xs={12}>
                        <ul className="widget-pab widget-pab-exec list-unstyled row">
                            <ArchiveSingleBlock showAuthor="true" posttype="executive-interviews" headingType="h3" classNamesLi="col-sm-4 col-xs-12" showExcerpt="true" showExecRole="true" archive={archiveList[5]} imgSize="mediumwide" />
                            <ArchiveSingleBlock showAuthor="true" posttype="executive-interviews" headingType="h3" classNamesLi="col-sm-4 col-xs-12" showExcerpt="true" showExecRole="true" archive={archiveList[6]} imgSize="mediumwide" />
                            <ArchiveSingleBlock showAuthor="true" posttype="executive-interviews" headingType="h3" classNamesLi="col-sm-4 col-xs-12" showExcerpt="true" showExecRole="true" archive={archiveList[7]} imgSize="mediumwide" />

                        </ul>

                    </Col>
                </Row>
            </Grid>

            <Grid className="section-front-exec-interviews page-below">
                <Row>
                    <Col xs={12}>

                        <ul className="widget-pab widget-pab-exec list-unstyled row">
                            <ArchiveSingleBlock posttype = "EXECUTIVE-INTERVIEWS" showAuthor="true" headingType="h3" classNamesLi="col-sm-4 col-xs-12"
                                                showExcerpt="true" showExecRole="true" archive={archiveList[8]}
                                                imgSize="mediumwide"/>
                            <ArchiveSingleBlock posttype = "EXECUTIVE-INTERVIEWS" showAuthor="true" headingType="h3" classNamesLi="col-sm-4 col-xs-12"
                                                showExcerpt="true" showExecRole="true" archive={archiveList[9]}
                                                imgSize="mediumwide"/>
                            <ArchiveSingleBlock posttype = "EXECUTIVE-INTERVIEWS" showAuthor="true" headingType="h3" classNamesLi="col-sm-4 col-xs-12"
                                                showExcerpt="true" showExecRole="true" archive={archiveList[10]}
                                                imgSize="mediumwide"/>
                        </ul>

                    </Col>
                </Row>
            </Grid>

            {this.state.showModalVideo ? <ModalBlock modalHandler={this.modalShowVideo}
                                                     modalBody='<div class="video-container"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/v3qJIaYQVkE?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>'/> : ''}

            <style global jsx>{`

					.section-front-about-wrapper {
						background: #EEE;
						padding: 40px 0px;
					}

					.section-front-about-item {
						margin: auto 0px;
					}
					.section-front-about-inner {
						display: flex;
					}
					.section-front-about-wrapper p {
						margin-bottom: 10px;
					}
					.section-front-about-wrapper .section-title {
						margin-top: 7px;
					}
					.section-front-about-wrapper p.last {
						margin-bottom: 20px;
					}
					.section-front-about-wrapper img {
						cursor: pointer;
					}
					.section-front-opinion .section-title-sm {
						border: 0px;
					}
            		.widget-pab-exec .post-title {
            			margin-bottom: 5px;
            		}
            		.section-front-opinion {
            			padding-top: 30px;
            		}
            		.section-front-CTA-wrapper {
            			text-align: center;
            		}
            		.section-front-CTA {
            			margin-top: 20px;
            			border-top: 1px solid #CCC;
            			padding: 20px 0px;
            		}
            		.section-front-CTA-wrapper img {
            			margin: 0 auto;
            		}
            		@media ( max-width: 769px ) {
            			.widget-pal-exec .widget-item-first {
            				border-top: 1px dashed #CCC;
            			}
            			.section-front-opinion {
            				padding-top: 0px;
            			}
            			.section-front-about-wrapper {
            				padding: 20px 0px;
            				text-align: center;
            			}
						.section-front-about-inner {
							display: block;
						}
            			.section-front-about-wrapper .section-title {
            				margin-top: 0px;
            			}
            			.section-front-about-wrapper .section-front-about-item {
            				max-width: 400px;
            				margin: 0 auto 20px;
            				clear: both;
            				float: none;
            			}
            			.section-front-about-wrapper .section-front-about-content {
            				margin-bottom: 30px;
            			}
            		}

                `}</style>
        </div>
    }
}
