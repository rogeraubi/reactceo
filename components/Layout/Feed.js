import { PureComponent } from 'react';

export default class Feed extends PureComponent{
    render(){
        return  <div className="feed-container">
                      <h1 className="">The CEO Magazine RSS Feeds </h1>
                      <h2>View articles from these The CEO Magazine categories</h2>
                      <ul className="">
                        <li>
                            <h4>Business</h4>
                            <a href="https://www.theceomagazine.com/business/feed.xml">https://www.theceomagazine.com/business/feed.xml</a>
                        </li>
                        <li>
                            <h4>Lifestyle</h4>
                            <a href="https://www.theceomagazine.com/lifestyle/feed.xml">https://www.theceomagazine.com/lifestyle/feed.xml</a>
                        </li>
                        <li>
                            <h4>Executive Interviews</h4>
                            <a href="https://www.theceomagazine.com/executive-interviews/feed.xml">https://www.theceomagazine.com/executive-interviews/feed.xml</a>
                        </li>
                        <li>
                            <h4>Opinion</h4>
                            <a href="https://www.theceomagazine.com/opinion/feed.xml">https://www.theceomagazine.com/opinion/feed.xml</a>
                        </li>
                        <li>
                            <h4>Companies</h4>
                            <a href="https://www.theceomagazine.com/company-directory/feed.xml">https://www.theceomagazine.com/company-directory/feed.xml</a>
                        </li>
                      </ul>
                

                <style jsx>{`
                  
                    .feed-container{
                        margin: auto;
                        max-width: 535px;
                    }

                    .feed-container ul{
                        margin-top: 20px;
                        padding: 0;
                    }

                    .feed-container ul li{
                        list-style: none;
                    }

                  

                    .feed-container h4{
                        font-weight: 600;
                    }
                `}</style>

                </div>
    }
}