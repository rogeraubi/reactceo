import { PureComponent } from 'react';

export default class EmptyPage extends PureComponent{
    render(){

        return <div style={{textAlign:'center'}}>
                  <h1>Page not found</h1>
                  <h1>Go back to <a href="https://www.theceomagazine.com">The CEO Magazine</a> </h1>
                </div>
    }
}