import {PureComponent} from 'react';
import ArchiveListBlock from './Blocks/ArchiveListBlock'
import FeaturedBlock from './Blocks/FeaturedBlock'
import ModalBlock from './Blocks/ModalBlock'

import {Grid, Row, Col, Image} from 'react-bootstrap';
import ArchiveSingleBlock from './Blocks/ArchiveSingleBlock';

import PartRegisterForm from '../DisplayForm/PartRegisterForm'
export default class Home extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            showModalVideo: false
        }
        this.modalShowVideo = this.modalShowVideo.bind(this);
    }

    modalShowVideo() {
        this.setState({
            showModalVideo: !this.state.showModalVideo
        })
    }

    render() {

        const {opinionList, businessList, lifestyleList, execInterviewList, featuredList, eventsList} = this.props;

        return <div className="page-home-wrapper">

            <Grid className="page-above">
                <Row>
                    <Col xs={12}>
                        <FeaturedBlock posttype="home" classNamesLi="col-sm-4 col-xs-12"
                                       classNamesUl="widget-pab widget-pab-business row" imgSize="mediumwide"
                                       archiveList={featuredList}/>
                    </Col>
                </Row>
            </Grid>
            <Grid className="section-front-exec-interviews page-below">
                <Row>
                    <Col xs={12}>
                        <div className="section-title-wrapper">
                            <h2 className="section-title-sm">Executive Interviews</h2>
                            <div className="section-title-action">
                                <a href="/executive-interviews" title="View All Executive Interviews"
                                   className="label label-alt">View All <i className="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                        <ul className="widget-pab widget-pab-exec list-unstyled row">
                            <ArchiveSingleBlock showAuthor="true" headingType="h3" classNamesLi="col-sm-4 col-xs-12"
                                                showExcerpt="true" showExecRole="true" archive={execInterviewList[0]}
                                                imgSize="mediumwide"/>
                            <ArchiveSingleBlock showAuthor="true" headingType="h3" classNamesLi="col-sm-4 col-xs-12"
                                                showExcerpt="true" showExecRole="true" archive={execInterviewList[1]}
                                                imgSize="mediumwide"/>
                            <li className="col-sm-4 col-xs-12">
                                <ul className="widget-pal widget-pal-exec list-unstyled">
                                    <ArchiveSingleBlock showAuthor="true" headingType="h3"
                                                        classNamesLi="widget-item-first" showExcerpt="false"
                                                        showExecRole="true" archive={execInterviewList[2]}
                                                        imgSize="thumbnail"/>
                                    <ArchiveSingleBlock showAuthor="true" headingType="h3" classNamesLi=""
                                                        showExcerpt="false" showExecRole="true"
                                                        archive={execInterviewList[3]} imgSize="thumbnail"/>
                                    <ArchiveSingleBlock showAuthor="true" headingType="h3"
                                                        classNamesLi="widget-item-last" showExcerpt="false"
                                                        showExecRole="true" archive={execInterviewList[4]}
                                                        imgSize="thumbnail"/>
                                </ul>
                            </li>
                        </ul>

                    </Col>
                </Row>
            </Grid>
            <Grid>
            <PartRegisterForm/>
            </Grid>
            <div className="section-front-about-wrapper page-below" style={{marginBottom:10+'px'}}>
                <Grid className="section-front-about">

                    <Row className="section-front-about-inner">
                        <Col sm={6} xs={12} className="section-front-about-item section-front-about-content">
                            <h1 className="section-title"><em>The CEO Magazine</em></h1>
                            <p className="nomargin">"<em>The CEO Magazine</em> - it really is a great magazine, top
                                quality."</p>
                            <p>Sir Richard Branson, August 2017</p>
                            <p><em>The CEO Magazine</em> is more than a business title; it’s a source of information,
                                inspiration and motivation for the world’s most successful leaders, executives,
                                investors and entrepreneurs.</p>
                            <p>A global media brand, we provide critical business insight and in-depth features on
                                companies, people, strategies, ideas and economic trends, while delivering unrivalled
                                access to the world’s most powerful people.</p>
                            <p className="last">Our content creates conversations, our voice is the one that
                                matters.</p>
                            <a href="/about" className="btn btn-main text-upper" title="About The CEO Magazine"><strong>About
                                Us</strong> <i className="fa fa-chevron-right"></i></a>
                        </Col>
                        <Col sm={6} xs={12} className="section-front-about-item section-front-about-video">
                            <img onClick={this.modalShowVideo} className="img-responsive"
                                 src="//static.theceomagazine.net/content/web/brand-video.jpg"/>
                        </Col>
                    </Row>
                </Grid>
            </div>

            <Grid className="section-front-lifestyle page-below">
                <Row>
                    <Col xs={12}>
                        <div className="section-title-wrapper">
                            <h2 className="section-title-sm">Lifestyle</h2>
                            <div className="section-title-action">
                                <a href="/lifestyle" title="View All Lifestyle Articles" className="label label-alt">View
                                    All <i className="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                        <ArchiveListBlock headingType="h3" showExcerpt="true" showDate="true"
                                          classNamesLi="col-sm-4 col-xs-12"
                                          classNamesUl="widget-pab widget-pab-lifestyle row" imgSize="mediumwide"
                                          archiveList={lifestyleList} archiveLimit="3"/>
                    </Col>
                </Row>
            </Grid>
            <Grid className="section-front-exec-interviews page-below">
                <Row>
                    <Col xs={12}>
                        <div className="section-title-wrapper">
                            <h2 className="section-title-sm">Business</h2>
                            <div className="section-title-action">
                                <a href="/business" title="View All Business Articles" className="label label-alt">View
                                    All <i className="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                        <ul className="widget-pab widget-pab-exec list-unstyled row">
                            <ArchiveSingleBlock showAuthor="true" showDate="true" headingType="h3"
                                                classNamesLi="col-sm-4 col-xs-12" showExcerpt="true"
                                                archive={businessList[0]} imgSize="mediumwide"/>
                            <ArchiveSingleBlock showAuthor="true" showDate="true" headingType="h3"
                                                classNamesLi="col-sm-4 col-xs-12" showExcerpt="true"
                                                archive={businessList[1]} imgSize="mediumwide"/>
                            <li className="col-sm-4 col-xs-12">
                                <ul className="widget-pal widget-pal-exec list-unstyled">
                                    <ArchiveSingleBlock showAuthor="true" showDate="true" headingType="h3"
                                                        classNamesLi="widget-item-first" showExcerpt="false"
                                                        archive={businessList[2]} imgSize="thumbnail"/>
                                    <ArchiveSingleBlock showAuthor="true" showDate="true" headingType="h3"
                                                        classNamesLi="widget-item-last" showExcerpt="false"
                                                        archive={businessList[3]} imgSize="thumbnail"/>
                                    <ArchiveSingleBlock showAuthor="true" showDate="true" headingType="h3"
                                                        classNamesLi="widget-item-last" showExcerpt="false"
                                                        archive={businessList[4]} imgSize="thumbnail"/>
                                </ul>
                            </li>
                        </ul>

                    </Col>
                </Row>
            </Grid>
            <Grid className="section-front-exec-interviews page-below">
                <Row>
                    <Col xs={12}>
                        <div className="section-title-wrapper">
                            <h2 className="section-title-sm">Events </h2>
                            <div className="section-title-action">
                                <a href="/events" title="View All Events" className="label label-alt">View
                                    All <i className="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                        <ul className="widget-pab widget-pab-exec list-unstyled row">
                            <ArchiveSingleBlock showAuthor="true" headingType="h3" classNamesLi="col-sm-4 col-xs-12"
                                                showExcerpt="true" showExecRole="true" archive={eventsList[0]}
                                                imgSize="mediumwide"/>
                            <ArchiveSingleBlock showAuthor="true" headingType="h3" classNamesLi="col-sm-4 col-xs-12"
                                                showExcerpt="true" showExecRole="true" archive={eventsList[1]}
                                                imgSize="mediumwide"/>
                            <li className="col-sm-4 col-xs-12">
                                <ul className="widget-pal widget-pal-exec list-unstyled">
                                    <ArchiveSingleBlock showAuthor="true" headingType="h3"
                                                        classNamesLi="widget-item-first" showExcerpt="false"
                                                        showExecRole="true" archive={eventsList[2]}
                                                        imgSize="thumbnail"/>
                                    <ArchiveSingleBlock showAuthor="true" headingType="h3" classNamesLi=""
                                                        showExcerpt="false" showExecRole="true" archive={eventsList[3]}
                                                        imgSize="thumbnail"/>
                                    <ArchiveSingleBlock showAuthor="true" headingType="h3"
                                                        classNamesLi="widget-item-last" showExcerpt="false"
                                                        showExecRole="true" archive={eventsList[4]}
                                                        imgSize="thumbnail"/>
                                </ul>
                            </li>
                        </ul>

                    </Col>
                </Row>
            </Grid>
            <Grid className="section-front-lifestyle page-below">


                <Row>
                    <Col xs={12}>
                        <div className="section-title-wrapper">
                            <h2 className="section-title-sm">Opinions</h2>
                            <div className="section-title-action">
                                <a href="/opinion" title="View All Opinions" className="label label-alt">View All <i
                                    className="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                        <ArchiveListBlock headingType="h3" showExcerpt="true" classNamesLi="col-sm-4 col-xs-12"
                                          classNamesUl="widget-pab widget-pab-opinion row" imgSize="mediumwide"
                                          archiveList={opinionList} archiveLimit="3"/>
                    </Col>
                </Row>
            </Grid>


            {/*{ this.state.showModalVideo ? <ModalBlock modalHandler={this.modalShowVideo} modalBody='<div class="video-container"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/uB5_HpuBLCA?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>'/> : '' }*/}
            {this.state.showModalVideo ? <ModalBlock modalHandler={this.modalShowVideo} modalBody='<div class="video-container"><iframe width="100%" height="100%" src="https://www.youtube.com/embed/2ItXe_l8I7c" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></div>'/> : ''}

            <style global jsx>{`

				    .subscribe-below {
				       margin-top:25px;
				    }
					.section-front-about-wrapper {
						background: #EEE;
						padding: 40px 0px;
					}
					
					.section-front-about-item {
						margin: auto 0px;
					}
					.section-front-about-inner {
						display: flex;
					}
					.section-front-about-wrapper p {
						margin-bottom: 10px;
					}
					.section-front-about-wrapper .section-title {
						margin-top: 7px;
					}
					.section-front-about-wrapper p.last {
						margin-bottom: 20px;
					}
					.section-front-about-wrapper img {
						cursor: pointer;
					}
					.section-front-opinion .section-title-sm {
						border: 0px;
					}
            		.widget-pab-exec .post-title {
            			margin-bottom: 5px;
            		}
            		.section-front-opinion {
            			padding-top: 30px;
            		}
            		.section-front-CTA-wrapper {
            			text-align: center;
            		}
            		.section-front-CTA {
            			margin-top: 20px;
            			border-top: 1px solid #CCC;
            			padding: 20px 0px;
            		}
            		.section-front-CTA-wrapper img {
            			margin: 0 auto;
            		}
            		@media ( max-width: 769px ) {
            			.widget-pal-exec .widget-item-first {
            				border-top: 1px dashed #CCC;
            			}
            			.section-front-opinion {
            				padding-top: 0px;
            			}
            			.section-front-about-wrapper {
            				padding: 20px 0px;
            				text-align: center;
            			}
						.section-front-about-inner {
							display: block;
						}
            			.section-front-about-wrapper .section-title {
            				margin-top: 0px;
            			}
            			.section-front-about-wrapper .section-front-about-item {
            				max-width: 400px;
            				margin: 0 auto 20px;
            				clear: both;
            				float: none;
            			}
            			.section-front-about-wrapper .section-front-about-content {
            				margin-bottom: 30px;
            			}
            		}
						
                `}</style>
        </div>
    }
}
