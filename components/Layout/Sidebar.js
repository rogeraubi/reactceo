import { PureComponent } from 'react';
import { inject, observer } from 'mobx-react'
import ArchiveListBlock from './Blocks/ArchiveListBlock'
import { Image} from 'react-bootstrap'
import GoogleDoubleClick from '../Advertising/GoogleDoubleClick'
import helper from '../../helpers/helper'

@inject('store') @observer
export default class Sidebar extends PureComponent{
	render(){
		const { sidebarList, posttype } = this.props.store;

		if(!sidebarList){
			return <div>Empty sidebar</div>
		}

		return <div className="sidebar">
					<div id="sidebar-inner" className="sidebar-inner">
						<div className="widget">
							<ArchiveListBlock headingType="h3" showExcerpt="false" classNamesUl="widget-pal widget-pab-business" archiveList={sidebarList} imgSize="thumbnail" />
						</div>
						<div className="widget widget-sidebar-mrec">
							<GoogleDoubleClick adslot={helper.displayAds({posttype:posttype}).mrec}
					    					   type="mrec" id={helper.displayAds({posttype:posttype}).mrecId} width={300} height={250} />
						</div>
					</div>
			  </div>
	}
}