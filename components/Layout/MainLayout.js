import Header from '../Header/Header'
import SearchModal from './Blocks/SearchModal'
import Footer from '../Footer/Footer'
import HeadCustom from '../Header/HeadCustom'
import { Component } from 'react';
import { inject, observer } from 'mobx-react'
import GoogleDoubleClick from '../Advertising/GoogleDoubleClick'
import helper from '../../helpers/helper';
import '../../design/index.scss';
@inject('store') @observer
export default class MainLayout extends Component {

    componentDidMount() {
        if (document.getElementById('cookieconsent-wrapper')) {
            console.log("component");
            window.addEventListener("load", function () {
                window.cookieconsent.initialise({
                    "palette": {
                        "popup": {
                            "background": "#103750"
                        },
                        "button": {
                            "background": "#fff",
                            "text": "#103750"
                        }
                    },
                    "type": "opt-out",
                    "content": {
                        "message": "This site uses cookies and other tracking technologies to \nassist with navigation and your ability to provide feedback, analyse your use of our products and services, assist with our promotional and marketing efforts, and provide content from third parties.",
                        "dismiss": "Accept",
                        "link": "Privacy Policy",
                        "href": "https://www.theceomagazine.com/privacy"
                    }
                })
            });
        }
    }

    render() {
        const {posttype, viewerRegion} = this.props.store;
        if (posttype == 'home' ) {
            return <div id="wrapper">
                {viewerRegion == 'Europe' ? <cookieConsent/> : ''}
                <SearchModal/>
                <HeadCustom/>
                <Header/>
                <div id="content-wrapper" className="clear">

                    <GoogleDoubleClick adslot={helper.displayAds({posttype: posttype}).leaderboard}
                                       type="leaderboard" id={helper.displayAds({posttype: posttype}).leaderboardId}/>

                    {this.props.children}
                </div>
                <Footer/>
            </div>
        }
        else {
            return <div id="wrapper">
                {viewerRegion == 'Europe' ? <cookieConsent/> : ''}
                <SearchModal/>
                <HeadCustom/>
                <Header/>
                <div id="content-wrapper" className="clear">

                    <GoogleDoubleClick adslot={helper.displayAds({posttype: posttype}).leaderboard}
                                       type="leaderboard" id={helper.displayAds({posttype: posttype}).leaderboardId}/>
                    {this.props.children}
                </div>
                <Footer/>
            </div>
		}

    }
}

const cookieConsent = ({}) => {
	return <div id="cookieconsent-wrapper">
				<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
				<script src="/static/js/cookieconsent.js"></script>
			</div>
}
