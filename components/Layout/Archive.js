import {Component} from 'react';
import {inject, observer} from 'mobx-react'
import {observable} from 'mobx'
import {Link, Router} from '../../routes'

import EventsArchiveJs from './CustomPages/Events/NewEventsArchive'
import ShopPostJs from './CustomPages/Shop/ShopPost'
import CareersArchive from './CustomPages/Careers/CareersArchive'
import CompanyDirectoryArchive from './CustomPages/CompanyDirectory/CompanyDirectoryArchive'
import Sidebar from './Sidebar'
import helper from '../../helpers/helper';

import ArchiveLoadMore from './Blocks/ArchiveLoadMore'
import ArchiveListBlock from './Blocks/ArchiveListBlock'
import ArchivePagination from './Blocks/ArchivePagination'
import ArchiveFilterBlock from './Blocks/ArchiveFilterBlock'
import EventApp from '../../helpers/EventApp'
import HomeInterviews from './HomeInterviews'

@inject('store') @observer
export default class Archive extends Component {
    eventTag = {
        EXPERIENCEEVENT: 'experiencevent',
        PRIVATEEVENT: 'privateevent',
        REWARD2017: 'reward2017',
        REWARD2018: 'reward2018',
        REWARD2019: 'reward2019',
        REWARD2020: 'reward2020',
        REWARD2021: 'reward2021',
        REWARD2022: 'reward2022',
        REWARD2023: 'reward2023',
        REWARD2024: 'reward2024',
        REWARD2025: 'reward2025',
    };

    eventFilter = {
        EVENTEXEUCTIVE: 'events-executive',
        EVENTPRIVATE: 'events-private',
        EVENTEXPERIENCE: 'events-experience'
    };

    constructor(props) {
        super(props);
        this.state = {
            eventcategory: this.eventTag,
            posttype: this.props.store.posttype,
            results: null,
            error: null,
            isLoading: false,
            archiveList: this.props.store.archiveList,
            eexperiencelist: null,
            eawordlist: null,
            eprivatelist: null,
        };
    }

    componentDidMount() {
        this.setState({isLoading: true});
    }

    isEmpty(obj) {
        for (let key in obj) {
            if (obj.hasOwnProperty(key))
                return false;

        }
        return true
    }

    componentWillMount() {
        if (this.state.posttype == "events") {
            const {archiveList} = this.state;
            let eprivatelist = [];
            let eexperiencelist = [];
            let eawordlist = [];
            if (archiveList.length > 0) {

                archiveList.map(singleArchive => {
                        let eprivate = {};
                        let maward = {};
                        let mprivate = {};
                        let mexperience = {};
                        let eexperience = {};
                        let eaword = {};
                        let slugeevent = {};
                        let slugtype = "";

                        slugeevent = helper.structurePostTags(singleArchive);
                        if (!this.isEmpty(slugeevent)) {
                            slugtype = slugeevent[0]['slug'];
                            switch (slugtype) {
                                case this.eventTag.PRIVATEEVENT:
                                    Object.assign(eprivate, singleArchive);
                                    eprivatelist.push(eprivate);
                                    break;
                                case this.eventTag.REWARD2017:
                                case this.eventTag.REWARD2018:
                                case this.eventTag.REWARD2019:
                                case this.eventTag.REWARD2020:
                                case this.eventTag.REWARD2021:
                                case this.eventTag.REWARD2022:
                                case this.eventTag.REWARD2023:
                                case this.eventTag.REWARD2024:
                                case this.eventTag.REWARD2025:
                                    Object.assign(eaword, singleArchive);
                                    eawordlist.push(eaword);
                                    break;
                                case this.eventTag.EXPERIENCEEVENT:
                                    Object.assign(eexperience, singleArchive);
                                    eexperiencelist.push(eexperience);
                                    break;
                            }
                        }
                    }
                )
            }
            this.setState({eawordlist: eawordlist});
            this.setState({eprivatelist: eprivatelist});
            this.setState({eexperiencelist: eexperiencelist});
        }
    }

    render() {
        const {category, posttype, archiveFilterRegion, archiveFilterCategory, eventOriginalPosttype,slug} = this.props.store;
        let archiveList = this.props.store.archiveList;
        let eventTitle = '';
        let archiveFilterBlock = "";
        if (posttype == 'executive-interviews') {
            archiveFilterBlock = <ArchiveFilterBlock subCategory={category} archiveFilterRegion={archiveFilterRegion}
                                                     archiveFilterCategory={archiveFilterCategory} slug={slug}/>
        }

        if (posttype == 'events' && eventOriginalPosttype == 'events') {

            return <EventsArchiveJs posttype={posttype}/>
        }
        if (posttype == 'executive-interviews') {
            let interExec = "Executive Interviews";
            return (
                <div className="archive-view">
                    <div className="archive-view-inner container">
                        <div className="floatChose">
                            {archiveFilterBlock}
                        </div>
                        <h2 className="floatleft">{helper.formatSlugToName(interExec)} </h2>
                    </div>
                    <HomeInterviews archiveList={archiveList} posttype={posttype}/>
                    <ArchivePagination/>
                    <style global jsx>{`
                         h1 {
                            font-wight:300;
                         }
                         h2 {
                             font-wight:100;
                          }
                        .section-title-sm {
                          margin-top:10px;
                          border-bottom:1px solid #ccc;
                        }
                        .floatChose {
                           margin-top:20px;
                           float:right;
                        }
                        .floatleft {
                           font-wight:100;
                           float:left;
                        }
				    	.archive-view .widget-pav {
				    		padding-top: 20px;
				    	}
						.archive-view .widget-pav .widget-item {
							border-bottom: 1px dashed #CCC;
							padding-bottom: 20px;
							margin-bottom: 20px;

						}
						.archive-view .widget-pav .widget-item .widget-inner > a {
							position: relative;
							display: table;
						}
						.archive-view .widget-pav .widget-item .post-img {
						 	display: table-cell;
							width: 250px;
						}
						.archive-view .widget-pav .widget-item .widget-inner {
							overflow: hidden;
						}
						.archive-view .widget-pav .widget-item .widget-body {
							 display: table-cell;
							 padding-left: 20px;
							  vertical-align: top;
						}
						.archive-view .widget-pav .widget-item .widget-body .post-title {
							margin-top: 0px;
						}
						.archive-view .widget-pav .widget-item .widget-body .post-excerpt {
							font-size: 16px;
							line-height: 22px;
						}
						@media ( min-width: 440px ) and ( max-width: 769px) {
							.archive-view .widget-pav .widget-item {
								text-align: center;
							}
							.archive-view .widget-pav .widget-inner {
								max-width: 400px;
								margin: 0 auto;
							}
						}
						@media ( max-width: 769px ) {
							.archive-view .widget-pav {
								overflow: hidden;
								margin-left: -10px;
								margin-right: -10px;
								padding-top: 10px;
							}
							.archive-view .widget-pav .widget-item {
								padding: 10px;
								border: 0px;
								height: 100%;
							}
							.archive-view .widget-pav .widget-item .post-img {
								width: 100%;
								display: block;
							}
							.archive-view .widget-pav .widget-item .widget-body {
								display: block;
								width: 100%;
								padding-top: 20px;
								padding-left: 0px;
							}
						}
						@media ( max-width: 520px ) {
							.archive-view .widget-pav {
								margin: 0px;
							}
							.archive-view .widget-pav .widget-item {
								width: 100%;
								float: none;
								padding: 0px;
								border-bottom: 1px dashed #CCC;
							}
							.archive-view .widget-pav .widget-item .widget-body {
								padding-bottom: 20px;
							}
						}
				    `}</style>
                </div>
            )

        }
        if (posttype == 'globalShop') {
            return <ShopPostJs/>
        }
        if (posttype == 'company-directory') {
            return <CompanyDirectoryArchive/>
        }
        if (posttype == 'careers') {
            return <CareersArchive/>
        }

        if (!archiveList || archiveList.length < 1) {
            return <p>NOTHING - Archive List</p>
        }
        if (posttype == 'events' && eventOriginalPosttype != 'events') {

            switch (eventOriginalPosttype) {
                case this.eventFilter.EVENTEXEUCTIVE:
                    archiveList = this.state.eawordlist;
                    eventTitle = 'EXECUTIVE-OF-THE-YEAR-AWARDS';
                    break;

                case this.eventFilter.EVENTEXPERIENCE:
                    archiveList = this.state.eexperiencelist;
                    eventTitle = eventOriginalPosttype;
                    break;

                case this.eventFilter.EVENTPRIVATE:
                    eventTitle = 'EVENTS';
                    archiveList = this.state.eprivatelist;
                    break;
            }
        }

        return <div className="archive-view">
            <div className="archive-view-inner container">
                <div id="content-entry" className="entry">
                    <h1 className="section-title-sm">{helper.formatSlugToName(eventTitle)} {helper.formatSlugToName(category)}</h1>
                    {archiveFilterBlock}
                    {
                        this.props.store.posttype == "events" ? <EventApp {...this.props} {...this.state}/> :
                            <ArchiveListBlock showTags="true" classNamesUl="widget-pav" imgSize="medium"
                                              archiveList={archiveList} posttype={posttype}/>
                    }
                    {
                        this.props.store.posttype != "events" ? <ArchivePagination/> : null
                    }
                </div>
                <Sidebar/>
            </div>
            <style global jsx>{`
				    	.archive-view .widget-pav {
				    		padding-top: 20px;
				    	}
						.archive-view .widget-pav .widget-item {
							border-bottom: 1px dashed #CCC;
							padding-bottom: 20px;
							margin-bottom: 20px;

						}
						.archive-view .widget-pav .widget-item .widget-inner > a {
							position: relative;
							display: table;
						}
						.archive-view .widget-pav .widget-item .post-img {
						 	display: table-cell;
							width: 250px;
						}
						.archive-view .widget-pav .widget-item .widget-inner {
							overflow: hidden;
						}
						.archive-view .widget-pav .widget-item .widget-body {
							 display: table-cell;
							 padding-left: 20px;
							  vertical-align: top;
						}
						.archive-view .widget-pav .widget-item .widget-body .post-title {
							margin-top: 0px;
						}
						.archive-view .widget-pav .widget-item .widget-body .post-excerpt {
							font-size: 16px;
							line-height: 22px;
						}
						@media ( min-width: 440px ) and ( max-width: 769px) {
							.archive-view .widget-pav .widget-item {
								text-align: center;
							}
							.archive-view .widget-pav .widget-inner {
								max-width: 400px;
								margin: 0 auto;
							}
						}
						@media ( max-width: 769px ) {
							.archive-view .widget-pav {
								overflow: hidden;
								margin-left: -10px;
								margin-right: -10px;
								padding-top: 10px;
							}
							.archive-view .widget-pav .widget-item {
								padding: 10px;
								border: 0px;
								height: 100%;
							}
							.archive-view .widget-pav .widget-item .post-img {
								width: 100%;
								display: block;
							}
							.archive-view .widget-pav .widget-item .widget-body {
								display: block;
								width: 100%;
								padding-top: 20px;
								padding-left: 0px;
							}
						}
						@media ( max-width: 520px ) {
							.archive-view .widget-pav {
								margin: 0px;
							}
							.archive-view .widget-pav .widget-item {
								width: 100%;
								float: none;
								padding: 0px;
								border-bottom: 1px dashed #CCC;
							}
							.archive-view .widget-pav .widget-item .widget-body {
								padding-bottom: 20px;
							}
						}
				    `}</style>
        </div>
    }
}
