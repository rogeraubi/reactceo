import { PureComponent } from 'react';
import { inject, observer } from 'mobx-react'
//import ReactModal from 'react-modal';
import { Modal } from 'react-bootstrap';
import { Grid, Row, Col, Image} from 'react-bootstrap';
import { ClipLoader } from 'react-spinners';
                      
import service from '../../helpers/service';

import ArchiveLoadMore from './Blocks/ArchiveLoadMore'
import ArchiveListBlock from './Blocks/ArchiveListBlock'

@inject('store') @observer
export default class Search extends PureComponent{
  
    constructor(props){
        super(props);
        this.state = {
        	isSearching: true
        }
    }
	
	async componentWillMount(){
	 	this.fetchSearch();
	}

	async fetchSearch(){
    const { searchText } = this.props;
     if(searchText == ""){
      this.setState({
        isSearching: false
      })
       return false;
     }
    const searchDataPromise = await service.getArchive('search', { q: searchText, limit:9, offset:0 });
    const searchData = await searchDataPromise.json();

   	this.props.store.setArchive(searchData);
   	this.setState({
       isSearching: false
    })
  }

    render(){
    const { archiveList } = this.props.store;
    const { searchText } = this.props;
		return <div className="search-view">
					<div className="post-view-inner container"> 
						<div className="entry text-center">
							<h1 className="page-title text-center">Search Results for "{searchText }"</h1>
							<ClipLoader color={'#123abc'} loading={this.state.isSearching}  size={100} />
							{ !this.state.isSearching ? <ArchiveListBlock classNamesUl="widget-pab widget-pab-search row" classNamesLi="col-sm-4 col-xs-12" showExcerpt="true" showDate="true" showTags="true" imgSize="medium" archiveList={archiveList}/> : '' }
			        { !this.state.isSearching ? <ArchiveLoadMore searchText={searchText} /> : '' }
						</div>
				  </div>
				<style global jsx>{`
            .text-center{
              text-align: center;
            }
			    `}</style>
			</div>
            
    }
}
