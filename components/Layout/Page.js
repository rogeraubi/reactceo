import { PureComponent } from 'react';
import { inject, observer } from 'mobx-react'
import GalleryScript from '../../static/js/GalleryScript'
import helper from '../../helpers/helper'
import CarouselScript from '../../static/js/CarouselScript'
import {Image} from "react-bootstrap";

@inject('store') @observer
export default class Page extends PureComponent{
	componentDidMount() {
	}

	render(){
		const layoutStyle = {

		}
		const { slug, page} = this.props.store;

		if(!page || (page && !page.title) ){
			return <p>empty page</p>
		}
		const media = helper.structureFeaturedMedia(page);
		const img = helper.getImageUrl(media, 'full'); //default post-thumbnail, pass second param for type - eg: thumbnail, small
		const imgSrcSet = helper.getImageSrcSet(media);
		const mediaTitle = media.title ? media.title.rendered : '';
		// const pageContent = page.content.rendered; //helper.extractShortcode(page.content.rendered);
		const pageContent  = helper.extractShortcode(page.content.rendered);

		return <div className="post-view" style={layoutStyle}>
					<div className="post-view-inner container">
						<div className="entry">
							<section className="entry-content">
								<Image srcSet={imgSrcSet} itemProp="image" className="post-featured-author" src={img.source_url} alt={media.alt_text} title={mediaTitle} responsive/>
								<div id="div-article-body" className="post-content speakable-paragraph" itemProp="articleBody" dangerouslySetInnerHTML={{__html: pageContent }} />
							</section>
							{/*<div className="post-content" dangerouslySetInnerHTML={{__html: pageContent }} />*/}


							{ (slug.indexOf('gallery') > -1 ? <GalleryScript/> : '') }

							<style global jsx>{`
								.gallery .gallery-item{
									float: left;
									width: 20%;
									margin: 5px 0px;
									text-align:center;
								}
								.gallery .gallery-item img{
									cursor: pointer;
								}
								.gallery .gallery-caption{
									display: none;
								}
						    `}</style>
						</div>
				  </div>
			    { pageContent.includes('slick-carousel') ? <CarouselScript/> : '' }
			</div>
	}
}
