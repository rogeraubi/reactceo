import { PureComponent } from 'react';
import { inject, observer } from 'mobx-react'
import { Grid, Row, Col, Image} from 'react-bootstrap';

import CarouselScript from '../../static/js/CarouselScript'
import AuthorBlock from './Blocks/AuthorBlock'
//import PostListBlock from './Blocks/PostListBlock'
import ArchiveListBlock from './Blocks/ArchiveListBlock'
import TagBlock from './Blocks/Post/TagBlock'
import SocialBlock from './Blocks/Post/SocialBlock'
import GoogleDoubleClick from '../Advertising/GoogleDoubleClick'
import StickyTitleBlock from './Blocks/StickyTitleBlock';
import DisplayCommentBlock from './Blocks/Post/DisplayCommentBlock'

import helper from '../../helpers/helper'
import service from '../../helpers/service'


@inject('store') @observer
export default class AuthorsPost extends PureComponent{
	constructor(props){
		super(props);

		this.state = {
			comments: [],
            previousPost: {},
            nextPost: {}
        }
	}
	
	async componentDidMount(){
		const { singlePost } = this.props.store;
		if(singlePost && singlePost.status == 'publish'){
			const commentsPromise = await service.getArchive( 'comments', {post: this.props.store.singlePost.id, per_page: 100});
			const comments = await commentsPromise.json();
			if(comments){
				this.setState({
					comments: comments
				})
			}   
		}
	}

	render(){
		const { singlePost, posttype, postList } = this.props.store;
		if(!singlePost){
			return <p>Empty single post</p>
		}

		const media = helper.structureFeaturedMedia(singlePost);
    	// const img = helper.getImageUrl(media, 'full'); //default post-thumbnail, pass second param for type - eg: thumbnail, small
    	const img = helper.getImageUrl(media, 'small'); //default post-thumbnail, pass second param for type - eg: thumbnail, small
        const imgSrcSet = helper.getImageSrcSet(media);

		const mediaTitle = media.title ? media.title.rendered : '';
		const singlePostContent = helper.extractShortcode(singlePost.content.rendered);
		const tags = helper.structurePostTags(AuthorsPost);

		let relatedType = "Related Articles"
		if ( singlePost.type == "executive-interviews" ) {
			relatedType = "More Profiles"
		}

		return <div className={"post-view post-view-sidebar post-type-" + singlePost.type }>
					<div className="post-view-inner container"> 
						<article id="content-entry" className="entry" itemProp="mainEntity" itemScope="itemscope" itemType="http://schema.org/Article"> 
							<link itemProp="mainEntityOfPage" href={singlePost.link} />
							<header id="post-header">
								<Row className="header-meta"> 
									<Col xs={4} className="header-meta-block">
                                        <span itemProp="dateModified" className="hidden" content={ helper.formatDate({date: singlePost.date_modified, type:'full'} ) }/>
                                        <span itemProp="datePublished" className="hidden" content={ helper.formatDate({date: singlePost.date, type:'full'} ) }/>
										<span className="post-date post-meta text-upper">{ helper.formatDate({date: singlePost.date}) } </span>
									</Col>
									<Col xs={8} className="header-meta-block">
										<TagBlock classNameUl="post-meta" classNameLi="" tags={tags}/>
									</Col>
								</Row>

								<h1 className="post-title speakable-headline" itemProp="name headline" dangerouslySetInnerHTML={{__html: singlePost.title.rendered }} />
								<div className="lead" dangerouslySetInnerHTML={{__html: singlePost.excerpt.rendered }}/>
							</header>
							<StickyTitleBlock singlePost={singlePost}/>
							<div className="entry-inner">
								<aside className="entry-sidebar">
									<div id="entry-sidebar-before-inner" className="sidebar-inner">
									</div>
								</aside>
								<section className="entry-content">
									<Image srcSet={imgSrcSet} itemProp="image" className="post-author-img" src={img.source_url} alt={media.alt_text} title={mediaTitle} responsive/>
									<div id="div-article-body" className="post-content speakable-paragraph" itemProp="articleBody" dangerouslySetInnerHTML={{__html: singlePostContent }} />
								</section>
								<aside id="entry-sidebar-after" className="entry-sidebar">
									<div id="sidebar-inner" className="sidebar-inner">
										<AuthorBlock author={singlePost.custom_meta.author} />
										<div id="post-comment" className="widget post-widget">
											<DisplayCommentBlock singlePost={singlePost} comments={this.state.comments} />
										</div>
										<SocialBlock singlePost={singlePost} />
                                        <div className="widget widget-sidebar-mrec">
                                        	<GoogleDoubleClick adslot={helper.displayAds({posttype:posttype}).mrec}  
					    					   type="mrec" id={helper.displayAds({posttype:posttype}).mrecId} width={300} height={250} />
										</div>
									</div>
								</aside>
							</div>
						</article>
						<div className="post-related-wrap widget clear">
							<h2 className="section-title-sm">{relatedType}</h2>
							<ArchiveListBlock showExcerpt="false" classNamesUl="widget-pab widget-pab-table row" headingType="h3" classNamesLi="col-sm-3 col-xs-12" archiveList={postList} imgSize="medium" posttype={posttype}/>
						</div>
					</div>
					{ singlePostContent.includes('slick-carousel') ? <CarouselScript/> : '' }
				</div>
	}
}