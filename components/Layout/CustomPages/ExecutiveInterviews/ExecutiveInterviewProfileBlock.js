import { PureComponent } from 'react';
import helper from '../../../../helpers/helper'
import { Grid, Row, Col, Image} from 'react-bootstrap';

export default class ExecutiveInterviewProfileBlock extends PureComponent{

  constructor(props){
    super(props);
  }

  componentDidMount(){
  	if( helper.getCookie('cookieconsent_status') == 'deny' ){
  		document.getElementById("item-pdf-link").remove();
  	}
  }

  render(){
	const { singlePost} = this.props;
	const media = helper.structureFeaturedMedia(singlePost);
	const img = helper.getImageUrl(media, 'thumbnail'); //default post-thumbnail, pass second param for type - eg: thumbnail, small	
	
	let pdfLink = ''
	if ( singlePost.custom_meta.custom_meta_pdf ) {
		let pdfMetaLink = singlePost.custom_meta.custom_meta_pdf;
		if( pdfMetaLink.indexOf('now.theceomagazine.com') < 0 ){
			pdfMetaLink = ( '//static.theceomagazine.net' + pdfMetaLink);
		}
		pdfLink = <li id="item-pdf-link" className="item item-last text-upper"><a href={ pdfMetaLink } title={singlePost.title.rendered} ><i className="fa fa-file-pdf-o"></i> Download PDF</a></li>
	}
	const mediaTitle = media.title ? media.title.rendered : '';

    return <div id="exec-profile-wrap" className="widget widget-exec-profile-bio">
    		<div className="exec-profile-wrap-inner">
    			<div className="exec-profile-img">
    				<Image className="img-circle" height="150px" itemProp="image" src={img.source_url} alt={media.alt_text} title={mediaTitle} responsive/>
    			</div>
    			<div className="exec-profile-body">
	    			<ul className="list-unstyled">
	    				<li className="item"><h2 className="exec-profile-title">{singlePost.custom_meta.custom_meta_execinterviews_name}</h2></li>
	    				<li className="item">{singlePost.custom_meta.custom_meta_execinterviews_role}, {singlePost.custom_meta.custom_meta_exec_company}</li>
	    				{pdfLink}
	    			</ul> 
	    		</div>
                <style global jsx>{`
					.widget-exec-profile-bio .exec-profile-wrap-inner {
						padding: 25px 15px 10px;
						background: #EEE;
						font-size: 16px;
					}
					.widget-exec-profile-bio .exec-profile-img {
						height: 150px;
						margin-bottom: 10px;
						text-align: center;
					}
					.widget-exec-profile-bio img {
						margin: 0 auto;
						max-width: 150px;
					}
					.widget-exec-profile-bio .exec-profile-body {
						text-align: center;
					}
					.widget-exec-profile-bio .item-last {
						margin-top: 20px;
					}
					.widget-exec-profile-bio .item-last a {
						font-weight: bold;
						font-size: 14px;
						letter-spacing: -0.5px;
					}
					.widget-exec-profile-bio .item-last .fa {
						color: red;
						padding-right: 5px;
					}
					@media ( max-width: 767px ) {
						.widget-exec-profile-bio { 
							margin: 0px;
						}
						.widget-exec-profile-bio .exec-profile-wrap-inner {
							overflow: hidden;
							padding: 20px 15px;
						}
						.widget-exec-profile-bio .exec-profile-img {
							height: 100px;
							float: left;
							margin: 0px;
							padding-right: 15px;
						}
						.widget-exec-profile-bio img {
							max-width: 100px;
						}
						
						.widget-exec-profile-bio .exec-profile-body {
							text-align: left;
						}
						.widget-exec-profile-bio .exec-profile-title {
							margin-top: 0px;
							margin-bottom: 0px;
							font-size: 20px;
						}
						.widget-exec-profile-bio .item-last {
							margin-top: 10px;
						}
					}
                `}</style>
            </div>
    	</div>

  	}
}