import { Component } from 'react';
import { inject, observer } from 'mobx-react'
import { Grid, Row, Col, Image} from 'react-bootstrap';

import CarouselScript from '../../../../static/js/CarouselScript'
import AuthorBlock from '../../Blocks/AuthorBlock'
import ArchiveListBlock from '../../Blocks/ArchiveListBlock'
import TagBlock from '../../Blocks/Post/TagBlock'
import SocialBlock from '../../Blocks/Post/SocialBlock'
import DisplayCommentBlock from '../../Blocks/Post/DisplayCommentBlock'

import GoogleDoubleClick from '../../../Advertising/GoogleDoubleClick'
import ExecutiveInterviewProfileBlock from './ExecutiveInterviewProfileBlock'
import StickyTitleBlock from '../../Blocks/StickyTitleBlock';

import helper from '../../../../helpers/helper'
import service from '../../../../helpers/service'


@inject('store') @observer
export default class ExecutiveInterviewsPost extends Component{
    constructor(props){
        super(props);

        this.state = {
            comments: [],
            previousPost: {},
            nextPost: {},
            firstAside: 350,
            hideSidebarAfter: 0
        }
    }

    async componentDidMount(){
        let firstAside = "";
        if(document.getElementById('entry-sidebar-before-inner')){
            firstAside = document.getElementById('entry-sidebar-before-inner').clientHeight + 20;
        }
        this.setState({
        	firstAside: firstAside,
        	hideSidebarAfter: 1
        })

        const { singlePost } = this.props.store;
        if(singlePost && singlePost.status == 'publish'){
            const commentsPromise = await service.getArchive( 'comments', {post: singlePost.id});
            const comments = await commentsPromise.json();

            if(comments){
                this.setState({
                    comments: comments
                })
            }
        }
    }

    render(){
    	const { firstAside } = this.state;
        const layoutStyle = {
        	asideTop: {
        		top: firstAside + 'px'
        	}
        }
        const { singlePost } = this.props.store;
        if(!singlePost){
            return <p>Empty single post exec interview post js</p>
        }
        //console.log(singlePost);
        const media = helper.structureFeaturedMedia(singlePost);
        const terms = helper.structurePostTags(singlePost);
        const img = helper.getImageUrl(media, 'thumbnail'); //default post-thumbnail, pass second param for type - eg: thumbnail, small
        const imgSrcSet = helper.getImageSrcSet(media);
        const mediaTitle = media.title ? media.title.rendered : '';
        const singlePostContent = helper.extractShortcode(singlePost.content.rendered);
        const tags = helper.structurePostTags(singlePost);

        const titleRendered = singlePost.title ? singlePost.title.rendered : '';
        const excerptRendered = singlePost.excerpt ? singlePost.excerpt.rendered : '';
        let Executivetitle = '';
        let singlePostAuthor = '';

        if (singlePost.custom_meta.author) {
            let singlePostAuthor ='exists';
           Executivetitle =   <span className="post-meta post-author">By {singlePost.custom_meta.author.post_title}</span>
        }

        return <div className={"post-view post-view-sidebar post-type-" + singlePost.type }>
                    <div className="post-view-inner container">
                            <article id="content-entry" className="entry" style={layoutStyle} itemScope="itemscope" itemProp="mainEntity" itemType="http://schema.org/Article">
                            <link itemProp="mainEntityOfPage" href={singlePost.link} />
                                <div id="post-header" className="executive-header">
                                    <Row className="header-meta">
                                        <Col xs={4} className="header-meta-block">
                                            <span itemProp="dateModified" className="hidden" content={ helper.formatDate({date: singlePost.date_modified, type:'full'} ) }/>
                                            <span itemProp="datePublished" className="hidden" content={ helper.formatDate({date: singlePost.date, type:'full'} ) }/>
                                            <span className="post-date post-meta text-upper">{ helper.formatDate({date: singlePost.date}) } </span>
                                        </Col>
                                        <Col xs={8} className="header-meta-block">
                                            <TagBlock classNameUl="post-meta" classNameLi="" tags={tags}/>
                                        </Col>
                                    </Row>
                                    <h1 className="post-title speakable-headline" itemProp="name headline" dangerouslySetInnerHTML={{__html: titleRendered }} />
                                    <div className="lead"  dangerouslySetInnerHTML={{__html: excerptRendered }}/>
                                       { singlePostAuthor ?  <span className="post-meta post-author">By {singlePost.custom_meta.author.post_title}</span>:'' }
                                </div>
                                <StickyTitleBlock singlePost={singlePost}/>
                                <div className="entry-inner">
									<aside id="entry-sidebar-before" className="entry-sidebar">
										<div id="entry-sidebar-before-inner" className="sidebar-inner">
											<ExecutiveInterviewProfileBlock singlePost={singlePost}/>
										</div>
									</aside>
                                    <section className="entry-content">
                                        <div className="entry-content-body speakable-paragraph" className="post-content" itemProp="articleBody" dangerouslySetInnerHTML={{__html: singlePostContent }} />
                                        { singlePostAuthor ?  <AuthorBlock author={singlePost.custom_meta.author} />:''}
                                    </section>
                                    <aside style={layoutStyle.asideTop} id="entry-sidebar-after" className={"entry-sidebar " + (this.state.hideSidebarAfter ? "" : '')}>
                                        <div id="sidebar-inner" className="sidebar-inner">
                                        	<SocialBlock singlePost={singlePost} />
                                            <div id="post-comment" className="widget post-widget">
                                                <DisplayCommentBlock singlePost={singlePost} comments={this.state.comments} />
                                            </div>
                                            <GoogleDoubleClick adslot="/77678524/CEO_Business_RSB_ATF_300X250" type="mrec" id="div-gpt-ad-1523593494978-0" width={300} height={250} />
                                        </div>
                                    </aside>
                                </div>
                            </article>

                    </div>
                    { singlePostContent.includes('slick-carousel') ? <CarouselScript/> : '' }
                    <style global jsx>{`
                    	.post-view-sidebar header .lead {
                    		margin: 0px;
                    	}
                    	.post-view-sidebar header .lead p {
                    		margin: 0px;
                    	}
						.post-view-sidebar header .post-author {
							display: block;
							margin-bottom: 20px;
						}
	              		@media ( min-width: 961px ) {
                   			#entry-sidebar-after {
                   				top: 0px;
                   			}
						}

                       .fadeIn {
                          -webkit-animation-name: fadeIn;
                          animation-name: fadeIn;
                          -webkit-animation-duration: 1s;
                          animation-duration: 1s;
                          -webkit-animation-fill-mode: both;
                          animation-fill-mode: both;
                          }
                          @-webkit-keyframes fadeIn {
                          0% {opacity: 0;}
                          100% {opacity: 1;}
                          }
                          @keyframes fadeIn {
                          0% {opacity: 0;}
                          100% {opacity: 1;}
                        }
                        .executive-header {
                         padding-top:20px;
                         border-top:none;
                         overflow:hidden;
                        }
                    `}</style>
                </div>
    }
}
