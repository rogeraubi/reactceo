import { Component } from 'react';
import { inject, observer } from 'mobx-react'
import { Grid, Row, Col, Image} from 'react-bootstrap';

import CarouselScript from '../../../../static/js/CarouselScript'
import AuthorBlock from '../../Blocks/AuthorBlock'
//import PostListBlock from './Blocks/PostListBlock'
import ArchiveListBlock from '../../Blocks/ArchiveListBlock'
import TagBlock from '../../Blocks/Post/TagBlock'
import GoogleDoubleClick from '../../../Advertising/GoogleDoubleClick'

import helper from '../../../../helpers/helper'
import service from '../../../../helpers/service'

@inject('store') @observer
export default class SinglePost extends Component{
	constructor(props){
		super(props);

		this.state = {
            headerFixed: false,
            reachedBottomArticle: false
        }
	}
	
	render(){
		const { singlePost } = this.props.store;
		if(!singlePost){
			return <p>Empty single post</p>
		}

		const media = helper.structureFeaturedMedia(singlePost);
    	const img = helper.getImageUrl(media, 'full'); //default post-thumbnail, pass second param for type - eg: thumbnail, small
        const imgSrcSet = helper.getImageSrcSet(media);
		const mediaTitle = media.title ? media.title.rendered : '';
		const singlePostContent = helper.extractShortcode(singlePost.content.rendered);
		const tags = helper.structurePostTags(singlePost);
    
		return <div className={"post-view post-type-" + singlePost.type }>
					<div className="post-view-inner container"> 
						<article id="content-entry" className="entry" itemProp="mainEntity" itemScope="itemscope" itemType="http://schema.org/Article"> 
							<link itemProp="mainEntityOfPage" href={singlePost.link} />
							<header id="post-header">
								<Row className="header-meta"> 
									<Col xs={4} className="header-meta-block">
                                        <span itemProp="dateModified" className="hidden" content={ helper.formatDate({date: singlePost.date_modified, type:'full'} ) }/>
                                        <span itemProp="datePublished" className="hidden" content={ helper.formatDate({date: singlePost.date, type:'full'} ) }/>
										<span className="post-date post-meta text-upper">{ helper.formatDate({date: singlePost.date}) } </span>
									</Col>
									<Col xs={8} className="header-meta-block">
										<TagBlock classNameUl="post-meta" classNameLi="" tags={tags}/>
									</Col>
								</Row>

								<h1 className="post-title speakable-headline" itemProp="name headline" dangerouslySetInnerHTML={{__html: singlePost.title.rendered }} />
								<div className="lead" dangerouslySetInnerHTML={{__html: singlePost.excerpt.rendered }}/>

							</header> 
							<div id="header-fixed-wrap" className={(this.state.headerFixed ? "header-fixed fadeIn " : '') + (this.state.reachedBottomArticle ? " header-fixed-bottom fadeOut " : "") + "hidden header-fixed-wrap"}>
								<div className="header-fixed-title" dangerouslySetInnerHTML={{__html: singlePost.title.rendered }} />
								<div className="header-fixed-lead" dangerouslySetInnerHTML={{__html: singlePost.excerpt.rendered }}/>
								<div className="header-fixed-author post-meta">By {singlePost.custom_meta.author.post_title}</div>
							</div>
							<div className="entry-inner">
	
								<section className="entry-content">
									<Image srcSet={imgSrcSet} itemProp="image" className="post-featured-img" src={img.source_url} alt={media.alt_text} title={mediaTitle} responsive/>
									<div id="div-article-body" className="post-content speakable-paragraph" itemProp="articleBody" dangerouslySetInnerHTML={{__html: singlePostContent }} />
								</section>

							</div>
						</article>

					</div>
					{ singlePostContent.includes('slick-carousel') ? <CarouselScript/> : '' }
					<style global jsx>{`

	              		@media ( min-width: 961px ) {
                   			#entry-sidebar-after {
                   				top: 0px;
                   			} 
						}
						
				       .fadeIn {
						  -webkit-animation-name: fadeIn;
						  animation-name: fadeIn;
						  -webkit-animation-duration: 1s;
						  animation-duration: 1s;
						  -webkit-animation-fill-mode: both;
						  animation-fill-mode: both;
						  }
						  @-webkit-keyframes fadeIn {
						  0% {opacity: 0;}
						  100% {opacity: 1;}
						  }
						  @keyframes fadeIn {
						  0% {opacity: 0;}
						  100% {opacity: 1;}
				  		} 
  
  
                	`}</style>
                
				</div>
	}
}