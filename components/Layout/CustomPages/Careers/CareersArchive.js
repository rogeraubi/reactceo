import { Component } from 'react';
import { inject, observer } from 'mobx-react'

import FeaturedBlock from '../../Blocks/FeaturedBlock'
import ArchiveLoadMore from '../../Blocks/ArchiveLoadMore'
import ArchiveListBlock from '../../Blocks/ArchiveListBlock'

@inject('store') @observer
export default class EventsArchive extends Component{
    constructor(props){
        super(props);
    }
    
    render(){
        const {archiveList} = this.props.store;
        
        if( !archiveList || archiveList.length < 1){
            return <p>Nothing Found</p>
        }

        return <div className="page-view">
                    <div className="page-view-inner"> 
                        <div id="content-entry" className="entry">
                            <section id="section-career-list" className="container max-content">
                                <h1 className="text-center page-title mb20">Careers</h1>
                                <p className="lead text-center">We're always looking for talented, creative and passionate people to join our team.</p>
                                <h3 className="text-center">Current Openings:</h3>
                                <ArchiveListBlock showReadMore="true" readMoreText="Apply Now" showTags="true" classNamesUl="widget-pav-simple widget-pav-careers text-center" showExcerpt="true" showAuthor="false" imgSize="medium" archiveList={archiveList}/>
                            </section>
                        </div>
                    </div>
                    <style global jsx>{`
                        .widget-pav-simple .widget-item:first-child {
                        	border-top: 1px dashed #CCC;
                        	padding-top: 20px;
                        }
                        .widget-pav-simple .widget-item {
                            border-bottom: 1px dashed #CCC;
                            padding-bottom: 20px;
                            margin-bottom: 20px;
                            
                        }
                        .widget-pav-simple .widget-item > a {
                            position: relative;
                            display: table;
                        }
                        .widget-pav-simple.widget-item .post-img {
                            display: table-cell
                            width: 250px;
                        }
                        .widget-pav-simple .widget-item .widget-inner {
                            overflow: hidden;
                        }
                        .widget-pav-simple .widget-item .widget-body {
                             display: table-cell
                             padding-left: 20px;
                              vertical-align: top;
                        }
                        .widget-pav-simple .widget-item .post-title {
                            margin-top: 0px;
                        }
                        .widget-pav-simple .widget-item .post-excerpt {
                            font-size: 16px;
                            line-height: 22px;
                        }
                        @media ( max-width: 767px ) {
                            .widget-pav-simple{
                                overflow: hidden;
                                margin-left: -10px;
                                margin-right: -10px;
                                padding-top: 10px;
                            }
                            .widget-pav-simple .widget-item {
                                float: left;
                                width: 50%;
                                padding: 10px;
                                border: 0px;
                                height: 100%;
                            }
                            .widget-pav-simple .widget-item:nth-child(2n+1) {
                                clear: both;
                            }
                            .widget-pav-simple.widget-item .post-img {
                                width: 100%;
                                display: block;
                            }
                            .widget-pav-simple .widget-item .widget-body {
                                display: block;
                                width: 100%;
                                padding-top: 20px;
                                padding-left: 0px;
                            }
                        }
                        @media ( max-width: 520px ) {
                            .widget-pav-simple {
                                margin: 0px;
                            }
                            .widget-pav-simple .widget-item {
                                width: 100%;
                                float: none;
                                padding: 0px;
                                border-bottom: 1px dashed #CCC;
                            }
                            .widget-pav-simple .widget-item .widget-body {
                                padding-bottom: 20px;   
                            }
                        }
                    `}</style>
                </div>
    }
}