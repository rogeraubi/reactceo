import { Component } from 'react';
import ReactEcharts from 'echarts-for-react';
require('echarts/map/js/world');

export default class ShopPost extends Component {
    constructor(props) {
        super(props);
        this.state = this.getInitialState();

    }
    timeTicket = null;
    getInitialState = () => ({option: this.getOption()});
    componentDidMount() {
        if (this.timeTicket) {
            clearInterval(this.timeTicket);
        }
        this.timeTicket = setInterval(() => {
            const option = this.state.option;
            const r = new Date().getSeconds();
            option.title.text = 'CEO' + r;
            option.series[0].name = 'CEO' + r;
            option.legend.data[0] = 'CEO' + r;
            this.setState({option: option});
        }, 1000);
    };

    componentWillUnmount() {
        if (this.timeTicket) {
            clearInterval(this.timeTicket);
        }
    };

    randomData() {
        return Math.round(Math.random() * 1000);
    };

    getOption = () => {
        return {
            title: {
               data: "The CEO Magazine Branch",
            },
            tooltip: {
                trigger: 'item'
            },
            legend: {
                orient: 'vertical',
                left: 'left',
                data: ['CEO1', 'CEO2', 'CEO3']
            },
            // visualMap: {
            //     min: 1,
            //     max: 5,
            //     left: 'left',
            //     top: 'bottom',
            //     text: ['高','低'],
            //     inRange: {
            //       color:['#103750']
            //     },
            //     // calculable: false
            // },
            series: [
                {
                    name: 'The CEO Magazine',
                    type: 'map',
                    mapType: 'world',
                    roam: false,
                    label: {
                        normal: {
                            show: false
                        },
                        emphasis: {
                            show: false
                        }
                    },
                    data:[
                        {name: 'China',value:3},
                        {name: 'Singapore',value:3},
                        {name: 'Australia',value: 4},
                        {name: 'India',value: 2},
                        {name: 'Sweden',value:5 },
                        {name: 'Ukraine',value: 5},
                        {name: 'Spain',value: 5},
                        {name: 'Finland',value: 5},
                        {name: 'Poland',value: 5},
                        {name: 'Italy',value: 5},
                        {name: 'Russia',value: 5},
                        {name: 'Germany',value: 5},
                        {name: 'United Kingdom',value:5},
                        {name: 'France',value: 5},
                        {name: 'Italy',value: 5},
                        {name: 'Middle East',value: 5},
                        {name: 'Africa',value: 5},
                        {name: 'Canada',value: 1},
                        {name: 'United States',value: 1},
                        {name: 'Bangladesh',value: 2},
                        {name: 'Sri Lanka',value: 2},
                        {name: 'Japan',value: 3},
                        {name: 'Algeria',value:5},
                        {name: 'Egypt',value:5}

                    ]
                }
            ]
        };
    };
    onChartClick = (param,echarts) => {
           if ((param.name == 'Australia') || (param.name == 'Japan') || (param.name == 'China')){
              window.location.href = "https://shop.theceomagazine.com/"
           }
           else {
               window.location.href = "https://shop.theceomagazine.com/"
           }
     };
    onChartLegendselectchanged = (param, echart) => {
        console.log(param, echart);
    };

    render() {
        let onEvents = {
            'click':this.onChartClick,
            'legendselectchanged':this.onChartLegendselectchanged
        };
        let nalink = "13.55.146.44";
        let anzlink ="https://shop.theceomagazine.com/";
        return (
              <div className='CEO-region' >
                    <ReactEcharts
                        option={this.state.option}
                        style={{height: '500px', width: '100%'}}
                        onChartReady={this.onChartReady}
                        onEvents = {onEvents}
                        className='react_for_echarts'/>
                         <div className="table-responsive">
                             <h4>Please select your region or country </h4>
                         <table className= "table" align="center" style={{height:'100px',width:'50%'}}>
                            <tbody>
                            <tr>
                                <th scope="col">NORTH AMERICA</th>
                                <th scope="col">EMEA</th>
                                <th scope="col">INDIA&SOUTH ASIA</th>
                                <th scope="col">ASIA</th>
                                <th scope="col">ANZ</th>
                            </tr>
                                <tr>
                                    <td><a href={anzlink}>United States of America</a></td>
                                    <td><a href={anzlink}>Europe</a></td>
                                    <td><a href={anzlink}>India</a></td>
                                    <td><a href={anzlink}>China</a></td>
                                    <td><a href={anzlink}>Australia</a></td>
                                </tr>
                                <tr>
                                    <td><a href={anzlink}>Canada</a></td>
                                    <td><a href={anzlink}>Middle East</a></td>
                                    <td><a href={anzlink}>Bangladesh</a></td>
                                    <td><a href={anzlink}>Japan</a></td>
                                    <td><a href={anzlink}>New Zealand</a></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td><a href={anzlink}>Africa</a></td>
                                    <td><a href={anzlink}>Sri Lanka</a></td>
                                    <td><a href={anzlink}>Singapore</a></td>
                                    <td> </td>
                                </tr>
                            </tbody>
                        </table>
                 </div>
                   <style global jsx>{`
                   	@media ( min-width: 520px ) {
							.CEO-region{
							    margin-top:-30px;
							    font-size:12px;
								text-align:center;
							}
							th {
							font-weight:200;
							text-align:center;
						}

                `}</style>
             </div>
        );
    };
}
