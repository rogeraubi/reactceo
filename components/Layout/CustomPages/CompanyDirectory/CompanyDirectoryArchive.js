import { Component } from 'react';
import { inject, observer } from 'mobx-react'
import ArchiveListBlock from '../../Blocks/ArchiveListBlock'
import ArchiveLoadMore from '../../Blocks/ArchiveLoadMore'
import CompanyDirectorySidebar from './CompanyDirectorySidebar'
import ArchiveFilterBlock from '../../Blocks/ArchiveFilterBlock'
import helper from '../../../../helpers/helper';
import ArchivePagination from '../../Blocks/ArchivePagination'

@inject('store') @observer
export default class CompanyDirectoryArchive extends Component{
	constructor(props){
		super(props);
	}
	
	render(){
		const {archiveList} = this.props.store;
		
	    //if( !archiveList || archiveList.length < 1){
	    // 	return <p>NOTHING company directory - Archive List</p>
	    //}
		

		return <div className="archive-view">
					<div className="archive-view-inner container"> 
						<div id="content-entry" className="entry">
							<h1 className="section-title-sm">Company Directory {helper.formatSlugToName(this.props.store.category)}</h1>
							<ArchiveFilterBlock subCategory={this.props.store.category} archiveFilterRegion={this.props.store.archiveFilterRegion} archiveFilterCategory={this.props.store.archiveFilterCategory} />
							<ArchiveListBlock classNamesUl="widget-pav" showAuthor="false" showExcerpt="true" imgSize="medium" archiveList={archiveList}/>
							<ArchivePagination />
						</div>
						<CompanyDirectorySidebar/>
					</div>
				    <style global jsx>{`
				    	.archive-view .widget-pav {
				    		padding-top: 20px;
				    	}
						.archive-view .widget-pav .widget-item {
							border-bottom: 1px dashed #CCC;
							padding-bottom: 20px;
							margin-bottom: 20px;
							
						}
						.archive-view .widget-pav .widget-item a {
							position: relative;
							display: table;
						}
						.archive-view .widget-pav .widget-item .post-img {
						 	display: table-cell;
							width: 250px;
						}
						.archive-view .widget-pav .widget-item .widget-inner {
							overflow: hidden;
						}
						.archive-view .widget-pav .widget-item .widget-body {
							 display: table-cell;
							 padding-left: 20px;
							  vertical-align: top;
						}
						.archive-view .widget-pav .widget-item .widget-body h2 {
							margin-top: 0px;
						}
						.archive-view .widget-pav .widget-item .widget-body .post-excerpt {
							font-size: 16px;
							line-height: 22px;
						}
						@media ( max-width: 767px ) {
							.archive-view .widget-pav {
								overflow: hidden;
								margin-left: -10px;
								margin-right: -10px;
								padding-top: 10px;
							}
							.archive-view .widget-pav .widget-item {
								float: left;
								width: 50%;
								padding: 10px;
								border: 0px;
								height: 100%;
							}
							.archive-view .widget-pav .widget-item:nth-child(2n+1) {
								clear: both;
							}
							.archive-view .widget-pav .widget-item .post-img {
								width: 100%;
								display: block;
							}
							.archive-view .widget-pav .widget-item .widget-body {
								display: block;
								width: 100%;
								padding-top: 20px;
								padding-left: 0px;
							}
						}
						@media ( max-width: 520px ) {
							.archive-view .widget-pav {
								margin: 0px;
							}
							.archive-view .widget-pav .widget-item {
								width: 100%;
								float: none;
								padding: 0px;
								border-bottom: 1px dashed #CCC;
							}
							.archive-view .widget-pav .widget-item .widget-body {
								padding-bottom: 20px;	
							}
						}
				    `}</style>
				</div>
	}
}