import { Component } from 'react';
import { inject, observer } from 'mobx-react'
import { Grid, Row, Col, Image} from 'react-bootstrap';

import CarouselScript from '../../../../static/js/CarouselScript'
import AuthorBlock from '../../Blocks/AuthorBlock'
import ArchiveListBlock from '../../Blocks/ArchiveListBlock'
import TagBlock from '../../Blocks/Post/TagBlock'
import SocialBlock from '../../Blocks/Post/SocialBlock'
import DisplayCommentBlock from '../../Blocks/Post/DisplayCommentBlock'

import GoogleTagManager from '../../../Advertising/GoogleTagManager'
import CompanyDetailsBlock from './CompanyDetailsBlock'
import StickyTitleBlock from '../../Blocks/StickyTitleBlock';

import helper from '../../../../helpers/helper'
import service from '../../../../helpers/service'


@inject('store') @observer
export default class CompanyDirectoryPost extends Component{
    constructor(props){
        super(props);

        this.state = {
            comments: [],
            previousPost: {},
            nextPost: {},
            firstAside: 0,
            hideSidebarAfter: 0
        }
    }
    
    async componentDidMount(){
        let firstAside = "";
        if(document.getElementById('entry-sidebar-before-inner') && document.getElementById('entry-sidebar-before-inner').clientHeight){
            firstAside = document.getElementById('entry-sidebar-before-inner').clientHeight + 20; 
        }
        this.setState({
        	firstAside: firstAside,
        	hideSidebarAfter: 1
        })
        
        if(this.props.store && this.props.store.singlePost){
            const commentsPromise = await service.getArchive( 'comments', {post: this.props.store.singlePost.id}); 
            const comments = await commentsPromise.json();
            if(comments){
                this.setState({
                    comments: comments
                })
            }
        }
    }

    render(){
    	
        const layoutStyle = {
        	asideTop: {
        		top: this.state.firstAside + 'px'
        	}
        }
        const { singlePost } = this.props.store;
        if(!singlePost){
            return <p>Empty single post</p>
        }

        const media = helper.structureFeaturedMedia(singlePost);
        const terms = helper.structurePostTags(singlePost);
        const img = helper.getImageUrl(media, 'thumbnail'); //default post-thumbnail, pass second param for type - eg: thumbnail, small
        const imgSrcSet = helper.getImageSrcSet(media);
        const mediaTitle = media.title ? media.title.rendered : '';
        const singlePostContent = helper.extractShortcode(singlePost.content.rendered);
        const tags = helper.structurePostTags(singlePost);
        

        return <div className={"company-view post-type-" + singlePost.type }>
                    <div className="post-view-inner container"> 
                            <article id="content-entry" className="entry" style={layoutStyle.asideTop} itemProp="mainEntity" itemScope="itemscope" itemType="http://schema.org/Article"> 
                                <link itemProp="mainEntityOfPage" href={singlePost.link} />
                                <div className="entry-inner">
                                
                                    <section className="entry-content">
    	                                <header id="post-header">
		                                    <Row className="header-meta"> 
		                                        <Col xs={12} className="header-meta-block">
		                                            <TagBlock classNameUl="post-meta" classNameLi="" tags={tags}/>
		                                        </Col>
		                                    </Row>
		                                    <div className="header-meta-content">
		                                    	<Image srcSet={imgSrcSet} itemProp="image" className="post-featured-img" src={img.source_url} alt={media.alt_text} title={mediaTitle} responsive/>
		                                    	<h1 className="post-title speakable-headline" itemProp="name headline" dangerouslySetInnerHTML={{__html: singlePost.title.rendered }} />
		                                    </div>
		                                    <div className="lead"  dangerouslySetInnerHTML={{__html: singlePost.excerpt.rendered }}/>		
		                                </header> 
		                                <div id="header-fixed-wrap" className={(this.state.headerFixed ? "header-fixed fadeIn " : '') + (this.state.reachedBottomArticle ? " header-fixed-bottom fadeOut " : "") + "hidden header-fixed-wrap"}>
		                                    <div className="header-fixed-title" dangerouslySetInnerHTML={{__html: singlePost.title.rendered }} />
		                                    <div className="header-fixed-lead" dangerouslySetInnerHTML={{__html: singlePost.excerpt.rendered }}/>
		                                </div>
                                        <div className="entry-content-body" className="post-content speakable-paragraph" itemProp="articleBody" dangerouslySetInnerHTML={{__html: singlePostContent }} />
                                    </section>
                                    <aside style={layoutStyle.asideTop} id="entry-sidebar-after" className={"entry-sidebar " + (this.state.hideSidebarAfter ? "" : '')}>
                                        <div id="sidebar-inner" className="sidebar-inner">
                                        	<CompanyDetailsBlock singlePost={singlePost}/>
                                        	<SocialBlock singlePost={singlePost} />
                                            <div id="post-comment" className="widget post-widget">
                                                <DisplayCommentBlock singlePost={singlePost} comments={this.state.comments} />                    
                                            </div>
                                        </div>
                                    </aside>
                                </div>
                            </article>
                    </div>
                    { singlePostContent.includes('slick-carousel') ? <CarouselScript/> : '' }
                    <style global jsx>{`
                    	.company-view .post-featured-img {
                    		max-width: 80px;
                    		position: absolute;
                    		left: 0px;
                    		top: 0px;
                    	}
                    	.company-view .header-meta-content {
                    		position: relative;
                    		margin-bottom: 20px;
                    	}
                    	.company-view .post-title {
                    		padding-left: 100px;
                    	}
						.company-view .entry-content {
							padding-right: 320px;
						}
                        .company-view .entry-sidebar {
                        	position: absolute;
                        	max-width: 300px;
                        	right: 0px;
                        	top: 0px;
                        }
                        .company-view .entry-inner {
                        	position: relative;
                        }
                        .widget-company-info {
                        	font-size: 14px;
                        }
                       .fadeIn {
                          -webkit-animation-name: fadeIn;
                          animation-name: fadeIn;
                          -webkit-animation-duration: 1s;
                          animation-duration: 1s;
                          -webkit-animation-fill-mode: both;
                          animation-fill-mode: both;
                          }
                          @-webkit-keyframes fadeIn {
                          0% {opacity: 0;}
                          100% {opacity: 1;}
                          }
                          @keyframes fadeIn {
                          0% {opacity: 0;}
                          100% {opacity: 1;}
                        } 

	              		@media ( min-width: 961px ) {
                   			#entry-sidebar-after {
                   				top: 0px;
                   			} 
                            .entry-inner .entry-content {
                                min-height: 750px;
                            } 
						}
                        @media ( min-width: 769px ) {
                            .company-view .post-featured-img {
                                max-width: 120px;
                            }
                            .company-view .post-title {
                                padding-left: 150px;
                            }
                            .company-view .header-meta-content {
                                margin-bottom: 40px;
                            }
                        }
						@media ( max-width: 769px ) {
							.company-view .entry-content {
								padding-right: 0px;
								padding-bottom: 20px;
							}
							.company-view .entry-sidebar {
								position: static;
								clear: both;
								max-width: 100%;
							}
						}
						
  
                    `}</style>
                
                </div>
    }
}