import { PureComponent } from 'react';
import { inject, observer } from 'mobx-react'
import ArchiveListBlock from '../../Blocks/ArchiveListBlock'
import { Image} from 'react-bootstrap'

@inject('store') @observer
export default class Sidebar extends PureComponent{
	
	render(){
		const {sidebarList} = this.props.store;

		if(!sidebarList){
			return <div>Empty sidebar</div>
		}

		return <div className="sidebar" >
					<div id="sidebar-inner" className="sidebar-inner">
						<div className="widget post-widget widget-company-add">
							<a className="text-upper btn btn-lg btn-main btn-block" href="/add-your-company">Add Your Company</a>
						</div>
						<div className="widget">
							<ArchiveListBlock headingType="h3" showExcerpt="false" classNamesUl="widget-pal widget-pab-business" archiveList={sidebarList} imgSize="thumbnail" />
						</div>
					</div>
					<style global jsx>{`
                   		.widget-company-add a {
                   			font-weight: bold;
                   		}
                	`}</style>
			  </div>
	}
}