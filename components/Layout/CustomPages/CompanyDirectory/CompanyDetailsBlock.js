import { Component } from 'react';
import helper from '../../../../helpers/helper'
import { Grid, Row, Col, Image} from 'react-bootstrap';
import MapBlock from '../../Blocks/Post/MapBlock'


export default class CompanyDetailsBlock extends Component{

  constructor(props){
    super(props);

  }

  render(){
    const layoutStyle = {
        
    }
        
    const { singlePost } = this.props;
    const singlePostCustomMeta = singlePost.custom_meta;

    let companyAddress2 = '';
    if ( singlePostCustomMeta.custom_meta_company_address2 ) {
    	companyAddress2 = <li className="widget-company-info-address2"><p>{singlePostCustomMeta.custom_meta_company_address2}</p></li>
    }

    return <div id="post-company-info">
    
    			<MapBlock singlePostCustomMeta={singlePostCustomMeta} />
    			
    			<div className="post-widget widget widget-company-info">
    				<div className="widget-body">
    					<ul className="list-unstyled">
    						<li className="widget-company-info-url"><a rel="nofollow" title={singlePostCustomMeta.custom_meta_logo_link} href={singlePostCustomMeta.custom_meta_logo_link + "?utm_source=www.theceomagazine.com&utm_medium=company_article" } target="_blank">{singlePostCustomMeta.custom_meta_logo_link}</a></li>
    						<li className="widget-company-info-address"><span>{singlePostCustomMeta.custom_meta_company_address}</span></li>
    						{companyAddress2}
    					</ul>
    				</div>
                	
                </div>
				<style global jsx>{`
					.widget-company-info {
						border-bottom: 1px dashed #CCC;
					}
					.widget-company-info li {
						position: relative;
						padding: 7px 10px 7px 30px;
					}
  					.widget-company-info-address span::after {
	                	content: "\f01d";
						font-family: "Global";
						width: 30px;
						text-align: left;
						font-size: 20px;
						position: absolute;
						left: 5px;
						top: 3px;
  					}
  					.widget-company-info-url a::after {
	                	content: "\f016";
						font-family: "Global";
						width: 30px;
						text-align: left;
						font-size: 20px;
						position: absolute;
						left: 5px;
						top: 3px;
  					}
                `}</style>
            </div>

  }
}