import {PureComponent} from 'react';
import {inject, observer} from 'mobx-react'

import ArchiveLoadMore from '../../Blocks/ArchiveLoadMore'
import EventListBlock from '../../Blocks/EventListBlock'
import FeatureBlock from '../../Blocks/FeaturedBlock'
import HomeEvent from '../../HomeEvent'
import helper from '../../../../helpers/helper'

@inject('store') @observer

export default class NewEventsArchive extends PureComponent {

    eventTag = {
        EXPERIENCEEVENT: 'experiencevent',
        MAINEXPERIENCE: 'mainexperience',
        MAINPRIVATE: 'mainprivate',
        MAINREWARDS: 'mainrewards',
        PRIVATEEVENT: 'privateevent',
        REWARD2017: 'reward2017',
        REWARD2018: 'reward2018',
        REWARD2019: 'reward2019',
        REWARD2020: 'reward2020',
        REWARD2021: 'reward2021',
        REWARD2022: 'reward2022',
        REWARD2023: 'reward2023',
        REWARD2024: 'reward2024',
    };

    constructor(props) {
        super(props);
        this.state = {
            eventcategory: this.eventTag,
            results: null,
            error: null,
            isLoading: false,
            archiveList: this.props.store.archiveList,
            eexperiencelist: null,
            eawordlist: null,
            eprivatelist: null,
            mawardlist:null,
            mprivatelist:null,
            mexperiencelist:null
        };
    }

    componentDidMount() {
        this.setState({isLoading: true});
    }

    isEmpty(obj) {
         for(let key in obj )
          {
             if(obj.hasOwnProperty(key))
                return false;

            }
           return true
    }
    componentWillMount() {
        const {archiveList} = this.state;
        let eprivatelist = [];
        let eexperiencelist = [];
        let eawordlist = [];
        let mawardlist = [];
        let mprivatelist = [];
        let mexperiencelist = [];

        if (archiveList.length > 0) {

            archiveList.map(singleArchive => {
                    let eprivate = {};
                    let maward = {};
                    let mprivate = {};
                    let mexperience = {};
                    let eexperience = {};
                    let eaword = {};
                    let slugeevent = {};
                    let slugtype = "";

                    slugeevent = helper.structurePostTags(singleArchive);
                    if (!this.isEmpty(slugeevent)) {
                        slugtype = slugeevent[0]['slug'];
                        switch (slugtype) {
                            case this.eventTag.PRIVATEEVENT:
                                Object.assign(eprivate, singleArchive);
                                eprivatelist.push(eprivate);
                                 break;
                            case this.eventTag.REWARD2017:
                            case this.eventTag.REWARD2018:
                            case this.eventTag.REWARD2019:
                            case this.eventTag.REWARD2020:
                            case this.eventTag.REWARD2021:
                            case this.eventTag.REWARD2022:
                            case this.eventTag.REWARD2023:
                            case this.eventTag.REWARD2024:
                                Object.assign(eaword, singleArchive);
                                eawordlist.push(eaword);
                                break;
                            case this.eventTag.EXPERIENCEEVENT:
                                Object.assign(eexperience, singleArchive);
                                eexperiencelist.push(eexperience);
                                break;
                            case this.eventTag.MAINREWARDS:
                                Object.assign(maward, singleArchive);
                                mawardlist.push(maward);
                                break;
                            case this.eventTag.MAINPRIVATE:
                                Object.assign(mprivate, singleArchive);
                                mprivatelist.push(mprivate);
                                break;
                            case this.eventTag.MAINEXPERIENCE:
                                Object.assign(mexperience, singleArchive);
                                mexperiencelist.push(mexperience);
                                break;
                        }
                    }
                }
            )
        }
        this.setState({eawordlist: eawordlist});
        this.setState({eprivatelist: eprivatelist});
        this.setState({eexperiencelist: eexperiencelist});
        this.setState({mawardlist: mawardlist});
        this.setState({mexperiencelist: mexperiencelist});
        this.setState({mprivatelist: mprivatelist});
    }


    render() {
        const {archiveList, featuredList ,posttype} = this.props.store;

        if (!archiveList || archiveList.length < 1) {
            return <p>Nothing Found</p>
        }
        return (

            <HomeEvent archiveList={archiveList} experienceList={this.state.eexperiencelist}
                       privateList={this.state.eprivatelist} posttype = {this.props.posttype}
                       awardList={this.state.eawordlist} maward = {this.state.mawardlist}  mexperience = {this.state.mexperiencelist} mprivate = {this.state.mprivatelist}/>
        )

    }
}