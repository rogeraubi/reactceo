import { PureComponent } from 'react';
import { inject, observer } from 'mobx-react'

import ArchiveLoadMore from '../../Blocks/ArchiveLoadMore'
import EventListBlock from '../../Blocks/EventListBlock'
import Home from '../../Home'
@inject('store') @observer
export default class EventsArchive extends PureComponent{
    render(){
        const {archiveList, featuredList} = this.props.store;

        if( !archiveList || archiveList.length < 1){
            return <p>Nothing Found</p>
        }
        return <div className="page-view">
                    <div className="page-view-inner"> 
                        <div id="content-entry" className="entry">  
                            <section id="section-pal" className="container">
                                {/*<h1 className="text-center page-title mb20">Executive Of The Year Awards</h1>*/}
                                <EventListBlock showTags="true" classNamesUl="widget-pab widget-pab-business row" classNamesLi="col-sm-4 col-xs-12" showExcerpt="false" showAuthor="false" imgSize="medium" archiveList={archiveList}/>
                            </section>
                        </div>
                    </div>
                    <style global jsx>{`
                        .archive-view .widget-pav {
                            padding-top: 20px;
                        }
                        .archive-view .widget-pav .widget-item {
                            border-bottom: 1px dashed #CCC;
                            padding-bottom: 20px;
                            margin-bottom: 20px;
                            
                        }
                        .archive-view .widget-pav .widget-item a {
                            position: relative;
                            display: table;
                        }
                        .archive-view .widget-pav .widget-item .post-img {
                            display: table-cell
                            width: 250px;
                        }
                        .archive-view .widget-pav .widget-item .widget-inner {
                            overflow: hidden;
                        }
                        .archive-view .widget-pav .widget-item .widget-body {
                             display: table-cell
                             padding-left: 20px;
                              vertical-align: top;
                        }
                        .archive-view .widget-pav .widget-item .widget-body h2 {
                            margin-top: 0px;
                        }
                        .archive-view .widget-pav .widget-item .widget-body .post-excerpt {
                            font-size: 16px;
                            line-height: 22px;
                        }
                        @media ( max-width: 767px ) {
                            .archive-view .widget-pav {
                                overflow: hidden;
                                margin-left: -10px;
                                margin-right: -10px;
                                padding-top: 10px;
                            }
                            .archive-view .widget-pav .widget-item {
                                float: left;
                                width: 50%;
                                padding: 10px;
                                border: 0px;
                                height: 100%;
                            }
                            .archive-view .widget-pav .widget-item:nth-child(2n+1) {
                                clear: both;
                            }
                            .archive-view .widget-pav .widget-item .post-img {
                                width: 100%;
                                display: block;
                            }
                            .archive-view .widget-pav .widget-item .widget-body {
                                display: block;
                                width: 100%;
                                padding-top: 20px;
                                padding-left: 0px;
                            }
                        }
                        @media ( max-width: 520px ) {
                            .archive-view .widget-pav {
                                margin: 0px;
                            }
                            .archive-view .widget-pav .widget-item {
                                width: 100%;
                                float: none;
                                padding: 0px;
                                border-bottom: 1px dashed #CCC;
                            }
                            .archive-view .widget-pav .widget-item .widget-body {
                                padding-bottom: 20px;   
                            }
                        }
                    `}</style>
                </div>
    }
}