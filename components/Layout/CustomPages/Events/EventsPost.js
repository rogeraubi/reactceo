import { Component } from 'react';
import { inject, observer } from 'mobx-react'
import { Grid, Row, Col, Image} from 'react-bootstrap';

import CarouselScript from '../../../../static/js/CarouselScript'
import SocialBlock from '../../Blocks/Post/SocialBlock'
import Home from '../../Home.js'

import helper from '../../../../helpers/helper'

@inject('store') @observer
export default class EventsPost extends Component{
    constructor(props){
        super(props);

        this.state = {
            previousPost: {},
            nextPost: {},
            headerFixed: false,
            reachedBottomArticle: false
        }
     
    }

    render(){
      
        const { singlePost } = this.props.store;
        if(!singlePost){
            return <p>Empty single post</p>
        }
        
        const media = helper.structureFeaturedMedia(singlePost);
        const img = helper.getImageUrl(media, 'full'); //default post-thumbnail, pass second param for type - eg: thumbnail, small
        const imgSrcSet = helper.getImageSrcSet(media);
        const mediaTitle = media.title ? media.title.rendered : '';
        const singlePostContent = helper.extractShortcode(singlePost.content.rendered);
      
        let relatedType = "Related Articles";
        if ( singlePost.type == "executive-interviews" ) {
            relatedType = "More Profiles"
        }

        return <div className={"post-view post-type-" + singlePost.type }>
                    <div className="post-view-inner container"> 
                        <article id="content-entry" className="entry" itemProp="mainEntity" itemScope="itemscope" itemType="http://schema.org/Article"> 
                            <link itemProp="mainEntityOfPage" href={singlePost.link} />
                            <header id="post-header">
                                <h1 className="post-title speakable-headline" itemProp="name headline" dangerouslySetInnerHTML={{__html: singlePost.title.rendered }} />
								<SocialBlock singlePost={singlePost} />
                            </header> 
                            <div className="entry-inner">
                                <section className="entry-content">
                                    <Image srcSet={imgSrcSet} itemProp="image" className="post-featured-img" src={img.source_url} alt={media.alt_text} title={mediaTitle} responsive/>
                                    <div id="div-article-body" className="post-content" itemProp="articleBody" dangerouslySetInnerHTML={{__html: singlePostContent }} />
                                </section>
                            </div>
                        </article>
                    </div>
                    { singlePostContent.includes('slick-carousel') ? <CarouselScript/> : '' }
                    <style global jsx>{`

                        @media ( min-width: 961px ) {
                            #entry-sidebar-after {
                                top: 0px;
                            } 
                        }
  
                    `}</style>
                
                </div>
    }
}
