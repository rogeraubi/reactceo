import { Component } from 'react';
import { inject, observer } from 'mobx-react'
import { slide as Menu } from 'react-burger-menu'
import TopMenu from './TopMenu'
import helper from '../../helpers/helper'
import { Grid, Row, Col, Image} from 'react-bootstrap'

@inject('store') @observer
export default class Header extends Component{

  constructor(props){
    super(props);
    
    this.state = {
        menuActive: {}
    }
    this.openChildMenu = this.openChildMenu.bind(this);
  }


  openChildMenu(event){
    const clickedMenuId = event.target.getAttribute('data-menu-id');
    let tempMenuActive = this.state.menuActive;

    tempMenuActive[clickedMenuId] = !tempMenuActive[clickedMenuId];
    this.setState({
        menuActive: tempMenuActive
    })
  }
  
  render(){
 
    let productImageUrl = "https://shop.theceomagazine.com/media/catalog/product/cache/1/small_image/320x/17f82f742ffe127f42dca9de82fb58b1/1/_/1_1.jpg";
    let productLinkUrl = "https://shop.theceomagazine.com";

    const { shopProduct, viewerCountry, viewerRegion, isBmOpen, bmChangeState, menu } = this.props.store;

    const { menuActive } = this.state;
    if(shopProduct && shopProduct.entity_id){
      productImageUrl = shopProduct.image_url;
      productLinkUrl = shopProduct.url;
    }
    if(menuActive && !menuActive[0]){
       menuActive[0] = false;
    }

    let menuNav = "";
    if(menu && menu.length > 0){
      menuNav = menu.map( menu => {
                      if(!menuActive[menu.id]){
                        menuActive[menu.id] = false;
                      }
                      
                      let menuCloseClass = menuActive[menu.id] == true ? '' : 'header-menu-hide';
                      let menuOpenClass = menuActive[menu.id] == true ? 'header-menu-hide' : '';

                      let childMenu = "";
                      let withChildren = "";
                      let hasChildrenMenuLink = <a href={menu.url} title={menu.title}>{menu.title}</a>;
                      if(menu.children != null && menu.children.length > 0){
                        hasChildrenMenuLink = <a className="pointer" data-menu-id={menu.id} onClick={ this.openChildMenu } title={menu.title}>{menu.title}</a>;
                        withChildren = <span className="item-caret">
                                        <span onClick={ this.openChildMenu } >
                                            <i data-menu-id={menu.id} className={menuOpenClass + " fa fa-chevron-right"}></i>
                                        </span>
                                        <span onClick={ this.openChildMenu } >
                                            <i data-menu-id={menu.id} className={menuCloseClass + " fa fa-chevron-up"}></i>
                                        </span>
                                       </span>
                                        
                        childMenu = menu.children.map( child => {
                            return <li className="item" key={child.id} >
                                        <a href={child.url} title={child.title} className="menu-item">{child.title}</a>
                                   </li>
                        })
                      }
                    
                      return <li key={menu.id} >
                                <span  key={menu.id}  >
                                    { hasChildrenMenuLink }
                                    { withChildren }
                                    <ul className={menuCloseClass + " list-unstyled "} >
                                        { childMenu }
                                    </ul>
                                </span>
                            </li>
                    })
    }
    
        
      return <header id="header-wrapper" className="clear">
		              <Menu id={ "menu-sidebar" } 
		              		className={ "top-side-menu" } 
		              		isOpen={ isBmOpen } 
		              		onStateChange={ bmChangeState }
							         customBurgerIcon={ false } >
  		                  <ul className="list-side-menu list-unstyled mb0">
  		                  	<li className="item">
                              <a href="/">Home</a>
  		                  	</li>
  		                  </ul>
  		                  <ul className="list-side-menu list-unstyled mb0">
                              { menuNav }
  		                  </ul>
				      	  <ul className="list-social-icon list-unstyled mb0">
					      		<li className="item"><a className="fb-bg-hover" href={helper.getSocialLink({type:'facebook', region:viewerRegion, country: viewerCountry})} target="_blank" title="The CEO Magazine Australia Facebook"><i className="fa fa-social-facebook"></i></a></li>
					      		<li className="item"><a className="twitter-bg-hover" href={helper.getSocialLink({type:'twitter', region:viewerRegion, country: viewerCountry})} target="_blank" title="The CEO Magazine Australia Twitter"><i className="fa fa-social-twitter"></i></a></li>
					      		<li className="item"><a className="linkedin-bg-hover" href={helper.getSocialLink({type:'linkedin', region:viewerRegion, country: viewerCountry})} target="_blank" title="The CEO Magazine Australia LinkedIn"><i className="fa fa-social-linkedin"></i></a></li>
					      		<li className="item"><a className="instagram-bg-hover" href={helper.getSocialLink({type:'instagram', region: viewerRegion, country: viewerCountry})} target="_blank" title="The CEO Magazine Australia Instagram"><i className="fa fa-social-instagram"></i></a></li> 
					      </ul>
					      <div className="sidemenu-subs clear">
					      	<Row>
					      		<Col xs={7}>
					      			<Image src={productImageUrl} responsive />
					      		</Col>
					      		<Col xs={5} className="sidemenu-subs-action"> 
									<ul className="list-unstyled text-upper">
							      		<li className="item"><a href={productLinkUrl} target="_blank" title="Print Edition"><strong>Print Edition</strong></a></li>
							      		<li className="item item-img-first item-img"><a href="https://itunes.apple.com/au/app/the-ceo-magazine-the-choice-for-high-level-executives/id638760987?mt=8" target="_blank" title="Download Apple App"><Image src="https://static.theceomagazine.net/content/web/app-apple.png" alt="Apple App" responsive/></a></li>
							      		<li className="item item-img"><a href="https://play.google.com/store/apps/details?id=com.ilovemags.ceomagazine" target="_blank" title="Download Android App"><Image src="https://static.theceomagazine.net/content/web/app-android.png" alt="Android App" responsive/></a></li>
							      	</ul>
					      		</Col>
					      	</Row>

					      </div>
		              </Menu>
		              
                  <div id="header-wrapper-inside" className="container">
		              <TopMenu />
		    	     </div>
            </header>
  }
}