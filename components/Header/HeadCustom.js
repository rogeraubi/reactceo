import Head from 'next/head'
import { inject, observer } from 'mobx-react'
import { Component } from 'react';
import GoogleTagManager from '../Advertising/GoogleTagManager'
import moment from 'moment';
import 'moment-timezone';
import ReactDOMServer from 'react-dom/server'
import helper from '../../helpers/helper'
import config from '../../helpers/config'

import getConfig from 'next/config'
const { publicRuntimeConfig } = getConfig();

@inject('store') @observer
export default class HeadCustom extends Component{
  componentDidMount(){
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'UA-37048409-17');
  }


  render(){
      const { viewerRegion, viewerCountry, category, posttype, page, yoastPageSEO } = this.props.store;

      let { singlePost, archiveTotalPosts, archivePageActive } = this.props.store;
      let metaObject = singlePost ? singlePost.custom_meta : null; 
      
      let postTypeDataLayer = "article";

      let ogType = "article";

      if(!metaObject){
        metaObject = page ? page.custom_meta : null;
      }
      if(!metaObject){
        metaObject = page || null; //make custom
      }
      if(!singlePost || !singlePost.id){
        singlePost = page || null;
        postTypeDataLayer = "page";
      }

      if(!metaObject){
        ogType = "blog";
        metaObject = [];
      }

      let metaTitle = metaObject['_yoast_wpseo_title'] || '';
      let metaDescription = metaObject['_yoast_wpseo_metadesc'] || '';

      let ogTitle = helper.replaceRegex(metaObject['_yoast_wpseo_opengraph-title']);
      let ogDesc = helper.replaceRegex(metaObject['_yoast_wpseo_opengraph-description']);
      let ogImg = metaObject['_yoast_wpseo_opengraph-image'] || '';
      
      let ogUrl = singlePost ? singlePost.link : 'https://www.theceomagazine.com';
      let ogSiteName = "The CEO Magazine";
      let ogUpdatedTime = singlePost ? new Date(singlePost.modified) : new Date(); //'YYYY-MM-ddThh:mm:ss', 

      let articlePublisher = helper.getSocialLink({type:'facebook', region:viewerRegion, country:viewerCountry});;
      let articlePublishedTime = singlePost ? new Date(singlePost.date) : new Date();
      let articleModifiedTime = singlePost ? new Date(singlePost.modified) : new Date();
      
      let canonicalUrl = "";
      let categoryUrl = "";
      let tagsText = "";
      if(singlePost && singlePost.id){
        const tags = helper.structurePostTags(singlePost);  
        for(let i in tags){
          const tag = tags[i];
          tagsText = tag.name + ',';
        }
      }else{
        
        if(category && category != "" && category != "page"){
          categoryUrl = "/" + category;
        }
        canonicalUrl = config.sitename + '/' + posttype + categoryUrl + '/';
      }
      
      let articleTag = helper.replaceRegex(tagsText);

      if(yoastPageSEO && (!singlePost || (singlePost && !singlePost.id) )){
        const seoDetailsPostType = yoastPageSEO.post_type;
        const seoDetailsCategory = yoastPageSEO.category;
        
        if(posttype != ""){
          let archiveSeoMetaTitle = "title-home-wpseo";
          let archiveSeoMetaDesc = "metadesc-home-wpseo";
          if(posttype != "home"){
            archiveSeoMetaTitle = "title-" + posttype;
            archiveSeoMetaDesc = "metadesc-" + posttype;
          }
          if(seoDetailsPostType[archiveSeoMetaTitle] && seoDetailsPostType[archiveSeoMetaTitle] != ""){
            metaTitle = seoDetailsPostType[archiveSeoMetaTitle];
          }
          if(seoDetailsPostType[archiveSeoMetaDesc] && seoDetailsPostType[archiveSeoMetaDesc] != ""){
            metaDescription = seoDetailsPostType[archiveSeoMetaDesc];
          }
        }

        if(category != ""){
            const posttypeCategory = posttype + '-category';            
            const categoryId = categoryId;

            if( seoDetailsCategory[posttypeCategory] && seoDetailsCategory[posttypeCategory][categoryId] ){
              const categorySEODetails = seoDetailsCategory[posttypeCategory][categoryId];

              if(categorySEODetails.wpseo_title && categorySEODetails.wpseo_title != ""){
                metaTitle = categorySEODetails.wpseo_title;
              }
              if(categorySEODetails.wpseo_desc && categorySEODetails.wpseo_desc != ""){
                metaDescription = categorySEODetails.wpseo_desc;
              }
              if(categorySEODetails.wpseo_canonical && categorySEODetails.wpseo_canonical != ""){
                //metaTitle = categorySEODetails.wpseo_canonical;
              }
            }
        }
      }else{
        metaTitle = (metaTitle == "" ?  
                              ( singlePost ? (singlePost.title ? helper.replaceRegex(singlePost.title.rendered) : metaTitle) : metaTitle ) 
                      : metaTitle);
        metaDescription = (metaDescription == "" ? 
                            ( singlePost ? (singlePost.excerpt ? helper.replaceRegex(singlePost.excerpt.rendered) : metaDescription ) : metaDescription ) 
                      : metaDescription );
        if(ogImg == ""){
          const media = helper.structureFeaturedMedia(singlePost);
          const img = helper.getImageUrl(media, 'full'); //default post-thumbnail, pass second param for type - eg: thumbnail, small
          ogImg = img.source_url;
        }
      }
      
      metaTitle = (metaTitle == "" ? "The CEO Magazine | Inspiring the business world" : metaTitle);
      metaDescription = metaDescription == "" ? "The CEO Magazine is a business magazine with thought-provoking business and lifestyle articles that inspire you to change, innovate, and disrupt." : metaDescription;
      ogDesc = ogDesc == "" ? metaDescription : ogDesc;
      ogTitle = ogTitle == "" ? metaTitle : ogTitle;
      twitterTitle = twitterTitle == "" ? metaTitle : twitterTitle;

      /*** EXECUTIVE INTERVIEWS META DATA ****/
      let execIntName = "";
      let execIntRole = "";
      let execIntCompany = "";
      const postTypeSlug = helper.formatSlugToName(posttype);
      const postCategory = category ? helper.formatSlugToName(category) : '';
      if(posttype == 'executive-interviews'){
          if(metaObject){
            execIntName = metaObject.custom_meta_execinterviews_name;
            execIntRole = metaObject.custom_meta_execinterviews_role;
            execIntCompany = metaObject.custom_meta_exec_company;
          }
      }
      

      let authorName = "";
      if(metaObject && metaObject.author && metaObject.author.post_title){
        authorName = metaObject.author.post_title;
      }

      canonicalUrl = canonicalUrl == "" ? ogUrl : canonicalUrl;
      let dataLayer = `dataLayer = [{
                          "pageTitle": "${metaTitle}",
                          "pagePostType": "${postTypeDataLayer}",
                          "pagePostAuthor": "${authorName}",
                          "pagePostCategory": "${postTypeSlug}",
                          "pagePostSubCategory": "${postCategory}",
                          "pagePostRegion": "${viewerRegion}",
                          "pagePostPublished": "${articlePublishedTime}",
                          "pagePostModified": "${articleModifiedTime}",
                          "intervieweeName": "${execIntName}",
                          "intervieweeCompany": "${execIntCompany}",
                          "intervieweeRole": "${execIntRole}",
                          "companyName": "${execIntCompany}"
                        }]`;

      /**** TWITTER META TAGS ****/
      let twitterTitle = metaObject['_yoast_wpseo_twitter-title'] ? helper.replaceRegex(metaObject['_yoast_wpseo_twitter-title']) : ogTitle;
      let twitterDesc = metaObject['_yoast_wpseo_twitter-description'] ? helper.replaceRegex(metaObject['_yoast_wpseo_twitter-description']) : ogDesc;
      let twitterImg = metaObject['_yoast_wpseo_twitter-image'] ? metaObject['_yoast_wpseo_twitter-image'] : ogImg;
      let twitterCard = "summary_large_image";
      let twitterSite = helper.getSocialLink({type:'twitter', region:viewerRegion, country:viewerCountry});

      /*** BUSINESS, OPINION, LIFESTYLE META DATA ****/
      let googleNewsMeta = "noindex, nofollow";
      let articleMetaData = "";
      if(posttype == 'business' || posttype == 'lifestyle' || posttype == 'opinion'){
        googleNewsMeta = "index, follow";

        articleMetaData = `{
                            "@context":"http://schema.org",
                            "publisher": {
                                "@type":"Organization",
                                "name":" The CEO Magazine ",
                                "logo": { 
                                        "@type":"ImageObject",
                                        "url":"https://static.theceomagazine.net/content/web/ceomagazinelogo.png",
                                        "width":180,
                                        "height":60
                                      }
                            },
                            "@type":"NewsArticle",
                            "author":"${authorName}",
                            "image": {
                              "@type":"ImageObject",
                              "url":"${ogImg}",
                              "width": 1200,
                              "height": 800
                            },
                              "headline":"${ogTitle}",
                              "datePublished":"${articlePublishedTime}",
                              "dateModified":"${articleModifiedTime}",
                              "name":" "${ogTitle}",
                              "Keywords":"${tagsText}",
                              "mainEntityOfPage":"${canonicalUrl}"
                          }`;

      }

      let speakableArticleMetaData = `{ "@context":"http://schema.org",
                                        "publisher":{
                                          "@type":"Organization",
                                          "name":"The CEO Magazine",
                                          "logo":{
                                              "@type":"ImageObject",
                                              "url":"https://static.theceomagazine.net/content/web/ceomagazinelogo.png",
                                              "width":900,
                                              "height":306
                                          }
                                        },
                                        "@type":"WebPage",
                                        "name":"${ogTitle}",
                                        "speakable":{
                                          "@type":"SpeakableSpecification",
                                          "cssSelector":[".speakable-headline",".speakable-author",".speakable-paragraph"]
                                        },
                                        "url":"${canonicalUrl}"}`;



      let metaRobots = "";
      let gtmId = "";
      if(publicRuntimeConfig && publicRuntimeConfig.node_env === "production_com"){
        metaRobots = "index, follow";
        gtmId = "GTM-PZTM2K";
      }else{
        metaRobots = "noindex, nofollow";
        gtmId = "GTM-P9HJ7CP";
      }

      /****** PAGE CANONICAL NEXT/PREVIOUS *******/
      let prevLink = "";
      let nextLink = "";

      archiveTotalPosts = parseInt(archiveTotalPosts);
      archivePageActive = parseInt(archivePageActive);
      if( archiveTotalPosts > 9 ){ //more than one page
        const numPages = archiveTotalPosts / 9;

        let siteUrl = config.sitename + '/' + posttype + categoryUrl;
        if( numPages > archivePageActive ){
          nextLink = <link rel="next" href={siteUrl + "/page/" + ( archivePageActive + 1)}/>;
        }
        if( archivePageActive > 1 ){
          if( archivePageActive == 2){
            prevLink = <link rel="prev" href={siteUrl}/>
          }else{
            prevLink = <link rel="prev" href={siteUrl + "/page/" + ( archivePageActive - 1)}/>  
          }
          if(publicRuntimeConfig && publicRuntimeConfig.node_env === "production_com"){
            metaRobots = "noindex, follow";
          }
        }        
      }

      return <Head>
              <title>{metaTitle}</title>

              <script dangerouslySetInnerHTML={{__html: dataLayer }} />
              <script id="article-metadata" type="application/ld+json" dangerouslySetInnerHTML={{__html: articleMetaData }} /> 
              <script id="article-speakable" type="application/ld+json" dangerouslySetInnerHTML={{__html: speakableArticleMetaData }} /> 
			
              <script async src="//www.googletagmanager.com/gtag/js?id=UA-37048409-12"></script>           

              <script async='async' src='//www.googletagservices.com/tag/js/gpt.js'></script>
              <GoogleTagManager scriptId="google-tag-manager" gtmId={gtmId} type="script"/>

              <meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
              <meta name="dc.language" content="en-US" />
              <meta name="robots" content={ metaRobots } />
              <meta name="dcterms.rightsHolder" content="The CEO Magazine"/>
              <meta name="viewport" content="width=device-width, initial-scale=1.0" />
              <meta name="Googlebot-News" content={googleNewsMeta}/>
              <meta name="description" content={metaDescription} />
              <meta name="viewport" content="initial-scale=1.0, width=device-width" />

              <meta name="author" content={authorName} itemProp="author" />

              <meta name="twitter:site" content={twitterSite} />
              <meta name="twitter:creator" content={twitterSite} />
              <meta name="twitter:title" content={twitterTitle} />
              <meta name="twitter:image" itemProp="image" content={twitterImg} />
              <meta name="twitter:card" content={twitterCard} />
              <meta name="apple-itunes-app" content="app-id= 638760987" />

              <meta property="og:locale" content="en_AU" />
		      <meta property="fb:app_id" content="1380306898768779" />
              <meta property="og:type" content={ogType} />
              <meta property="og:title" content={ogTitle} />
              <meta property="og:description" content={ogDesc} />
              <meta property="og:image" content={ogImg} />
              <meta property="og:url" content={ogUrl} />
              <meta property="og:site_name" content={ogSiteName} />
              <meta property="og:updated_time" content={ogUpdatedTime} />

              <meta property="article:publisher" content={articlePublisher} />
              <meta property="article:published_time" content={articlePublishedTime} />
              <meta property="article:modified_time" content={articleModifiedTime} />
              <meta property="article:tag" content={articleTag} />
              <meta property="article:section" content={posttype} />
              <meta property="fb:admins" content="399754700104901" />

              <link rel="canonical" href={canonicalUrl} />
              { prevLink }
              { nextLink }

              <link rel="alternate" type="application/rss+xml" title="The CEO Magazine Feed" href="https://www.theceomagazine.com/feed/" /> 
              <link rel="icon" href="//static.theceomagazine.net/content/logos/favicon.ico" />

              <script src="//use.fortawesome.com/e6383ba4.js"></script>
              <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossOrigin="anonymous" />
              <link href="//fonts.googleapis.com/css?family=Montserrat:400,400i,600,600i,700,700i" rel="stylesheet" /> 
            </Head>
  }
}