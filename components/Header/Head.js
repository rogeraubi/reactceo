import Head from 'next/head'
//import '../../static/css/header.css';

export default (props) => (
  <Head>
    <title>The CEO Magazine</title>
    <meta name="viewport" content="initial-scale=1.0, width=device-width" />
    <link href="../../static/css/header.css" rel="stylesheet" />
  </Head>
)