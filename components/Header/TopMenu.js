import { Component } from 'react';
import { Link, Router } from '../../routes'
import { inject, observer } from 'mobx-react'

@inject('store') @observer
export default class TopMenu extends Component{
	render(){
		return <div id="top-header" >
					<div id="top-burger">
						<span className="top-menu-icon fa fa-bars-btm" onClick={() => this.props.store.setBmOpen(true)}></span>
					</div>
					<div id="top-logo">
						<a href="/" title="The CEO Magazine"><img src="https://static.theceomagazine.net/content/web/ceo-logo.svg" alt="The CEO Magazine" /></a>
					</div>
					<nav id="top-menu" role="navigation" className="hidden-xs">
						<ul className="list-unstyled list-inline no-margin">
							<li className="item"><a title="Business" href="/business">Business</a></li>
							<li className="item"><a title="News" href="//news.theceomagazine.com/">News</a></li>
							<li className="item"><a title="Executive Interviews" href="/executive-interviews">Executive Interviews</a></li>
							<li className="item"><a title="Opinion" href="/opinion">Opinion</a></li>
							<li className="item"><a title="Lifestyle" href="/lifestyle">Lifestyle</a></li>
							<li className="item"><a title="Events" href="/events">Events</a></li>
							<li className="item"><a title="Shop" href="/globalShop">Shop</a></li>
						</ul>
				   	</nav>
					<div id="top-search"  >
						<span onClick={() => this.props.store.openSearchModal() }>
							<i className="top-menu-icon fa fa-search"></i>
							<span className="text-upper top-search-txt hidden-sm hidden-xs">Search</span>
						</span>
					</div>
				</div>

	}
}
