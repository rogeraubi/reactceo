import { Component } from 'react';
import { Grid, Row, Col, Image} from 'react-bootstrap';
import { inject, observer } from 'mobx-react'
import service from '../../helpers/service';
import helper from '../../helpers/helper';

@inject('store') @observer


export default class Footer extends Component{


	render(){
		// let productImageUrl = "https://shop.theceomagazine.com/media/catalog/product/cache/1/small_image/320x/17f82f742ffe127f42dca9de82fb58b1/1/_/1_1.jpg";
		let productImageUrl = "https://shop.theceomagazine.com/media/catalog/product/cache/1/small_image/320x/17f82f742ffe127f42dca9de82fb58b1/_/a/_aa_ofc_anz0319_copy_1.jpg";
		let productLinkUrl = "https://shop.theceomagazine.com";
		const { shopProduct, viewerRegion, viewerCountry } = this.props.store;
		if(shopProduct && shopProduct.entity_id){
			productImageUrl = shopProduct.image_url;
			productLinkUrl = shopProduct.url;
		}

		return <footer id="footer-wrapper" >
					<div id="footer-content-wrapper">
						<div id="om-fpaekipf2b7a17h0sffe-holder"></div>
						<div id="footer-content" className="clearfix container">
							<Grid>
							  <Row>
							    <Col xs={12} md={7}>
							    	<div id="footer-logo-wrapper" className="clear">
    							  		<a className="footer-logo" href="/" title="The CEO Magazine"><img src="https://static.theceomagazine.net/content/web/ceo-logo.svg" alt="The CEO Magazine Logo" /></a>
							      		<span className="footer-tag text-upper convert2">Inspiring The Business World</span>
							      	</div>
							      <Row>
							      	<Col xs={12} sm={6}>
							      		<Row>
							      			<Col xs={12} sm={6}>
							      				<h3 className="footer-title convert2 visible-xs">Explore Content</h3>
									      		<ul className="footer-menu footer-menu-main list-unstyled text-upper">
										      		<li className="item"><a href="/business" title="Business">Business</a></li>
										      		<li className="item"><a href="/lifestyle" title="Lifestyle">Lifestyle</a></li>
										      		<li className="item"><a href="/executive-interviews" title="Profiles">Profiles</a></li>
										      		<li className="item"><a href="/opinion" title="Opinion">Opinion</a></li>
										      		<li className="item"><a href="/company-directory" title="Companies">Companies</a></li>
										      		<li className="item"><a href="/reviews" title="Reviews">Reviews</a></li>
										      	</ul>
										 	</Col>
										 	<Col xs={12} sm={6}>
										 		<h3 className="footer-title convert2 visible-xs">Site Navigation</h3>
									      		<ul className="footer-menu list-unstyled text-upper mb0">
										      		<li className="item"><a href="/about" title="About Us">About Us</a></li>
										      		<li className="item"><a href="/contact" title="Contact Us">Contact Us</a></li>
										      		<li className="item"><a href="/authors" title="Authors">Authors</a></li>
										      		<li className="item"><a href="/careers" title="Careers">Careers</a></li>
										      		<li className="item"><a href="/events" title="Events">Events</a></li>
										      		<li className="item"><a href="/faq" title="FAQ">FAQ</a></li>
										      		<li className="item"><a href="/press" title="Press">Press</a></li>
										      		<li className="item"><a href="/partners" title="Partners">Partners</a></li>
										      	</ul>
										 	</Col>

										 </Row>
							      	</Col>
							      	<Col xs={12} sm={6}>
							      		<Row>
										 	<Col xs={12} sm={6}>
										 		<ul className="footer-menu list-unstyled text-upper mb0">
										 			<li className="item"><a href="//news.theceomagazine.com/" title="News">News</a></li>
										      		<li className="item"><a href="/video" title="CEO TV">CEO TV</a></li>
										      		<li className="item"><a href="/advertising" title="Advertising">Advertising</a></li>
										      		<li className="item"><a href="/interviews" title="Interviews">Interviews</a></li>
										      		<li className="item"><a href="https://shop.theceomagazine.com" title="Shop">Shop</a></li>
										      	</ul>
										 	</Col>
										 	<Col xs={12} sm={6}>
												<h3 className="footer-title convert2 visible-xs">Social Media</h3>
									      		<ul className="footer-menu footer-menu-social list-unstyled text-upper mb20">
										      		<li className="item"><a href={helper.getSocialLink({type:'facebook', region:viewerRegion, country:viewerCountry})} target="_blank" title="The CEO Magazine Australia Facebook"><i className="fa fa-social-facebook"></i>Facebook</a></li>
										      		<li className="item"><a href={helper.getSocialLink({type:'twitter', region:viewerRegion, country:viewerCountry})} target="_blank" title="The CEO Magazine Australia Twitter"><i className="fa fa-social-twitter"></i>Twitter</a></li>
										      		<li className="item"><a href={helper.getSocialLink({type:'linkedin', region:viewerRegion, country:viewerCountry})} target="_blank" title="The CEO Magazine Australia LinkedIn"><i className="fa fa-social-linkedin"></i>LinkedIn</a></li>
										      		<li className="item"><a href="https://plus.google.com/+TheceomagazineAustralia" target="_blank" title="The CEO Magazine Australia Google+"><i className="fa fa-social-google-plus"></i>Google+</a></li>
										      		<li className="item"><a href={helper.getSocialLink({type:'instagram', region:viewerRegion, country:viewerCountry})} target="_blank" title="The CEO Magazine Australia Instagram"><i className="fa fa-social-instagram"></i>Instagram</a></li>
										      	</ul>

										 	</Col>
							      		</Row>
							      	</Col>
							      </Row>
							    </Col>
							    <Col xs={12} md={5} className="widget-footer widget-footer-sub">
							    	<h3 className="footer-title convert2">Subscriptions & Downloads</h3>
							    	<Row>
								      <Col xs={6} md={6}>
										<Image src={productImageUrl} responsive />
								      </Col>
								      <Col xs={6} md={6} className="mb10">
								      	<ul className="footer-menu list-unstyled list-footer-subs text-upper">
								      		<li className="item"><a href={productLinkUrl} target="_blank" title="Print Edition"><strong>Print Edition</strong></a></li>
								      		<li className="item item-img-first item-img"><a href="https://itunes.apple.com/au/app/the-ceo-magazine-the-choice-for-high-level-executives/id638760987?mt=8" target="_blank" title="Download Apple App"><Image src="https://static.theceomagazine.net/content/web/app-apple.png" alt="Apple App" responsive/></a></li>
								      		<li className="item item-img"><a href="https://play.google.com/store/apps/details?id=com.ilovemags.ceomagazine" target="_blank" title="Download Android App"><Image src="https://static.theceomagazine.net/content/web/app-android.png" alt="Android App" responsive/></a></li>
								      	</ul>
								      </Col>
								    </Row>
							    </Col>
							  </Row>
							</Grid>
							<div id="footer-info">
								<h3 className="convert2">Inspiring The Business World</h3>
								<p><em>The CEO Magazine</em> is more than a business title; it’s a source of information, inspiration and motivation for the world’s most successful leaders, executives, investors and entrepreneurs. Our content creates conversations, our voice is the one that matters.</p>
							</div>
						</div>
					</div>
					<div id="footer-meta">
						<ul className="list-unstyled list-inline text-small">
							<li className="item"><a href="/privacy" title="Privacy Policy">Privacy Policy</a></li>
							<li className="item"><a href="/terms" title="Terms">Terms & Conditions</a></li>
							<li className="item"><a href="/disclaimer" title="Disclaimer" >Disclaimer</a></li>
							<li className="item"><a href="/payment" title="Payment">Payment</a></li>
							<li className="item"><a href="/sitemap" title="Sitemap">Sitemap</a></li>
						</ul>
						<p className="footer-region text-center"><i className="fa fa-map-marker-btm"></i> {viewerRegion} - {viewerCountry}</p>
						<p className="text-center footer-copy"><small>&copy; 2019 <em>The CEO Magazine</em>. All rights reserved.</small></p>
					</div>
			  </footer>
	}
}
