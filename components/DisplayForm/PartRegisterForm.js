import {Component} from 'react';
import {Grid, Row, Col, Image} from 'react-bootstrap';
export default class PartRegisterForm extends Component {
    render() {
        const mmrec = <a href='https://www.theceomagazine.com/newsletter/' target='_blank'> <img style={{padding:1+'px',border:'1px solid #021a40'}}
             src = 'https://s3-ap-southeast-2.amazonaws.com/ceo-server/WebBanner_SignUp_Newsletter_April19_V2%5B3%5D.jpg' alt = 'WebBanner' title='The CEO Magazine Web Banner SignUp Newsletter' className= 'img-responsive'></img></a>
        return <div className="row subscribe-below grow">
                   {mmrec}
               <style global jsx>{`
                    .subscribe-below {
				       margin-bottom:25px;
				    }
                    .grow:hover{
				      -webkit-transform:scale(1.1);
				      -webkit-transform:scale(1.1);
				      transform:scale(1.1);
				      border-radius:40%;
				   }
                `}</style>
        </div>
    }
}
