const nextRoutes = require('next-routes')
const routes = module.exports = nextRoutes()

routes
.add('/', '/', 'index')

.add('posttype', '/:posttype', 'post')
.add('posttype/:category', '/:posttype/:category', 'post')
.add('posttype/:category/:slug', '/:posttype/:category/:slug', 'post')
.add('posttype/:category/:slug/:twoslug', '/:posttype/:category/:slug/:twoslug', 'post')
.add('posttype/:category/:slug/:twoslug/:threeslug', '/:posttype/:category/:slug/:twoslug/:threeslug', 'post')
