import { action, observable, extendObservable } from 'mobx'
import config from './helpers/config';
import service from './helpers/service';

let store = null;
class Store {
  /* MENU */
  @observable isBmOpen = false;
  /* SEARCH */
  @observable isSearchModalOpen = false;

  /** OBJECTS **/
  @observable menu= null;
  @observable page= null;

  @observable archiveList= [];
  @observable archiveTotalPosts = 0;
  @observable archivePageActive = 1;
  @observable archiveFilterCategory= [];
  @observable archiveFilterRegion= [];
  @observable page= null;
  @observable posttype= "";
  @observable eventOriginalPosttype ="";
  @observable category= "";
  @observable categoryId= 0;
  //@observable subCategory: null;
  @observable slug= null;
  @observable singlePost= null;
  @observable searchList= [];
  @observable sidebarList= [];
  @observable featuredList= [];
  @observable postList = [];
  @observable metaObject= null;
  @observable viewerRegion= null;
  @observable viewerCountry= null;
  @observable shopProduct= null;
  @observable yoastPageSEO= null;

  constructor (isServer, params) {
    this.menu = params.menu;
    this.page = params.page;

    this.archiveList = params.archiveList;
    this.archiveTotalPosts = params.archiveTotalPosts;
    this.archivePageActive = params.archivePageActive;
    this.posttype = params.posttype;
    this.eventOriginalPosttype = params.eventOriginalPosttype;
    this.category = params.category;
    this.region = params.region;
    this.categoryId = params.categoryId;
    //this.subCategory = params.subCategory;
    this.slug = params.slug;
    this.singlePost = params.singlePost;
    this.sidebarList = params.sidebarList;
    this.featuredList = params.featuredList;
    this.postList = params.postList;
    this.viewerRegion = params.viewerRegion;
    this.viewerCountry = params.viewerCountry;
    this.shopProduct = params.shopProduct;
    this.yoastPageSEO = params.yoastPageSEO;

    this.archiveFilterCategory = params.archiveFilterCategory;
    this.archiveFilterRegion = params.archiveFilterRegion;
  }

  @action fetchMenu = async() => {
    const menuData = await service.getMenu();
    this.setMenu(menuData);
  }

  @action fetchArchive = async(params) => {
    const archiveData = await service.getArchive(params.posttype, {});
    this.setArchive(archiveData);
  }

  @action fetchPage = async(params) => {
    const pageData = await service.getPage({slug:params.slug});
    this.setPage(pageData[0]);
  }

  @action fetchSinglePost = async(params) => {
    const postData = await service.getPost({posttype: params.posttype, slug:params.slug});
    this.setSinglePost(postData[0]);
  }

  @action setMenu = (menu) => { this.menu = menu; this.gettingMenu = false; } //[...menu]
  @action setArchive = (archive) => { this.archiveList = archive;  }
  @action setPage = (page) => { this.page = page;}
  @action setSinglePost = (singlePost) => { this.singlePost = singlePost;}
  @action setviewerRegion = (country) => { this.viewerRegion = country;}

  @action setMetaObject = (metaObj) => { this.metaObject = metaObj }

  @action setBmOpen = (isOpen) => { this.isBmOpen = true }
  @action openSearchModal = () => { this.isSearchModalOpen = true }
  @action closeSearchModal = () => { this.isSearchModalOpen = false }

  @action bmChangeState = (state) => { this.isBmOpen = state.isOpen; return state.isOpen; }
}

export function initStore (isServer, params = {}) {
  if (isServer) {
    return new Store(isServer, params)
  } else {
    if (store === null) {
      store = new Store(isServer, params)
    }else{
      store.slug = params.slug;
      store.archiveList = params.archiveList;
      store.archiveTotalPosts = params.archiveTotalPosts;
      store.archivePageActive = params.archivePageActive;
      store.category = params.category;
      store.categoryId = params.categoryId;
      store.region = params.region;
      store.posttype = params.posttype;
      store.eventOriginalPosttype = params.eventOriginalPosttype;
      store.page = params.page;

      store.singlePost = params.singlePost;
      store.postList = params.postList;
      store.sidebarList = params.sidebarList;
      store.featuredList = params.featuredList;

      store.viewerRegion = params.viewerRegion;
      store.viewerCountry = params.viewerCountry;
      store.shopProduct = params.shopProduct;
      store.yoastPageSEO = params.yoastPageSEO;

      store.archiveFilterCategory = params.archiveFilterCategory;
      store.archiveFilterRegion = params.archiveFilterRegion;
    }
    return store
  }
}
