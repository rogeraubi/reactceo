export default (props) => {
	const width = props.width;
	const classes = props.class;
	const content = props.content;

	return <div style={{width: width+"px"}} className={ classes + " wrap-box-parent"} >
				<div className="wrap-box-text">
					<strong>Fast facts:</strong>
					<div dangerouslySetInnerHTML={{__html: content }} />
				</div>
				<style global jsx>{`
					.wrap-box-parent {
						margin: 20px;
						clear: both;
					}
					.wrap-box-text{
						padding: 15px;
						font-size: 13px;
						background: #EEE; 
						line-height: 20px; 
					}

					@media (max-width: 540px) {
						.wrap-box-parent {
							clear: both;
							width: 100% !important;
							margin-top: 0px;
							margin: auto;
						}
					}
			    `}</style>
			</div>
}