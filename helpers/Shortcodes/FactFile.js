//import ReactDOMServer from 'react-dom/server'

export default (props) => {
	const width = props.width;
	const classes = props.class;
	const content = props.content;
	const bgColor = props.bgcolor ? props.bgcolor : 'white';
	const title = props.title ? props.title : 'no title';
	

	return <div style={{backgroundColor: bgColor}} className={ classes + " fact-container"} >
				<div className="fact-left-block"><strong><span><div dangerouslySetInnerHTML={{__html: title }} /></span></strong></div>
				<div className="fact-right-block">
					<div dangerouslySetInnerHTML={{__html: content }} />
				</div>
				<style global jsx>{`
					.fact-container{
						width:100%;
						border: solid 2px #595959;
						position: relative;
						line-height: 1.7;
						overflow: hidden;
					}
					.fact-container .fact-left-block{
						width:100%;	
						text-align:center;
					}
					.fact-container .fact-left-block span{
						font-size:18px;
						font-weight:600;
						
					}
					.fact-container .fact-right-block{
						width:100%;
						padding:5px 15px;
						background-color:white !important;	
					}
					.fact-container .fact-right-block p{
						margin-bottom: 0px;
					}
					.fact-container  .fact-caption{
						font-size: 16px !important;
						font-weight: 600 !important;
						padding-top:6px;
					}
					.fact-container .fact-description{
						display: inline-block; 
						padding: 3px 0;
						font-size: 13px !important;
					}
					.fact-container .fact-details .fact-caption .fact-caption-addr{
						font-weight: 300 !important;
					}

					@media(min-width: 768px){ /* NOT MOBILE */
						.fact-container .fact-left-block span{
							top: 45%;
							left:1%;
						}
						.fact-container .fact-left-block span{
							position:absolute;
						}
						.fact-container .fact-right-block{
							width:87%;
							float:right;
						}
						.fact-container .fact-left-block{
							width:20%;
							float:left;	
						}
						.fact-container{
							margin:20px 0;
						}
					}
					@media(max-width: 768px){ /* MOBILE */
						.fact-container .fact-left-block{
							padding:10px;
						}
					}
			    `}</style>
			</div>
}