//import Shortcode from 'wp-shortcodes.js';
//var out = new Shortcode(text, tags).get();
//https://github.com/ayc/wp-shortcodes.js
import ShortcodeParser from 'meta-shortcodes';
import ReactDOMServer from 'react-dom/server'
import moment from 'moment';
import 'moment-timezone';

//IMPORT SHORTCODES
import Img from './Shortcodes/Img'
import FastFact from './Shortcodes/FastFact'
import FactFile from './Shortcodes/FactFile'
import config from './config';

import getConfig from 'next/config'

const {publicRuntimeConfig} = getConfig();


function replaceRegex(text) {
    if (!text || text == "") {
        return "";
    }
    return text.replace(/\&#8221;/g, '"')//replace special character "
        .replace(/\&#8243;/g, '"')
        .replace(/\&#8217;/g, "'")
        .replace(/\&#8220;/g, '"');
}

function extractShortcode(text) {

    if (!text || text == "") {
        return '';
    }

    text = text.replace(/\&#8221;/g, '"')//replace special character "
        .replace(/\&#8243;/g, '"')
        .replace(/\&#8217;/g, "'")
        .replace(/\&#8220;/g, '"');


    var parser = ShortcodeParser();
    parser.add("img", function (opts, content) {

        return ReactDOMServer.renderToString(<Img content={content} {...opts}/>);
    });
    parser.add("owl_carousel", function (opts, content) {
        const parsedContent = content.replace(/<br\s*\/?>/gi, '')
        return ReactDOMServer.renderToString(<div className="slick-carousel"
                                                  dangerouslySetInnerHTML={{__html: parsedContent}}/>);
    });
    parser.add("fast_fact", function (opts, content) {
        return ReactDOMServer.renderToString(<FastFact {...opts} content={content}/>);
    });
    parser.add("fact_file", function (opts, content) {
        //let test = ReactDOMServer.renderToString(<FactFile {...opts} content={content}/>);
        return ReactDOMServer.renderToString(<FactFile {...opts} content={content}/>);
    });

    var output = parser.parse(text);
    return output;
}

function regexUrl(url) {
    if (!url || url == "") {
        return "";
    }
    return url.replace(new RegExp("(.*/)[^/]+$"), "$1");
}

function getReplacedUrlLink(url) {
    if (!url || url == "") {
        return url;
    }
    let replaceTo = "http://localhost:3000/";
    let replaceFrom = "https://www.theceomagazine.net/";

    if (publicRuntimeConfig && publicRuntimeConfig.node_env === "production_net") {
        replaceTo = "https://www.theceomagazine.net/";
        let replaceFrom = "https://www.theceomagazine.com/";
        return regexUrl(url.replace(replaceFrom, replaceTo));
        // return regexUrl(url);
    }
    if (publicRuntimeConfig && publicRuntimeConfig.node_env === "production_com") {
        replaceTo = "https://www.theceomagazine.com/";
        let replaceFrom = "https://www.theceomagazine.net/";
        url = regexUrl(url.replace('52.63.172.230', 'www.theceomagazine.com')
            .replace('http://52.63.172.230', 'https://www.theceomagazine.com'));
    }
    if (publicRuntimeConfig && publicRuntimeConfig.node_env === "development") {
        url = regexUrl(url.replace('https://www.theceomagazine.com/', replaceTo));
    }
    url = regexUrl(url.replace('https://cms.theceomagazine.net/', replaceTo));
    return regexUrl(url.replace(replaceFrom, replaceTo));
}

function structureFeaturedMedia(post) {

    let media = {};

    if (post && post._embedded
        && post._embedded['wp:featuredmedia']
        && post._embedded['wp:featuredmedia'][0]) {

        const featuredMedia = post._embedded['wp:featuredmedia'][0];

        media = featuredMedia;
        media.width = featuredMedia.width;
        media.height = featuredMedia.height;

        if (media.media_details && media.media_details.sizes) {
            const mediaDetails = media.media_details;

            media.small = mediaDetails.sizes.small;
            media.medium = mediaDetails.sizes.medium;
            media.mediumwide = mediaDetails.sizes.mediumwide;
            media.medium_large = mediaDetails.sizes.medium_large;
            media['post-thumbnail'] = mediaDetails.sizes['post-thumbnail'];
            media.xlarge = mediaDetails.sizes.xlarge;
            media.wide = mediaDetails.sizes.wide;
            media.thumbnail = mediaDetails.sizes.thumbnail;
            media.large = mediaDetails.sizes.large;
            media.full = mediaDetails.sizes.full;
        }
    }
    return media;
}

function getImageUrl(media, type) {
    //Default - post-thumbnail
    let img = {};
    img.source_url = "";

    if (type && type != "") {
        if (type == "thumbnail") {
            img = media.thumbnail;
        }
        if (type == "small") {
            img = media.small;
        }
        if (type == "medium") {
            img = media.medium;
        }
        if (type == "mediumwide") {
            img = media.mediumwide;
        }
        if (type == "medium_large") {
            img = media.medium_large;
        }
        if (type == "large") {
            img = media.large;
        }
        if (type == "full") {
            img = media.full;
        }
        if (type == "wide") {
            img = media.wide;
        }
        if (type == "xlarge") {
            img = media.xlarge;
        }
    }
    if (img && img.source_url == "") {
        img = media['post-thumbnail'];
    }
    if (img && img.source_url == "") {
        img = media.thumbnail;
    }
    if (img && img.source_url == "") {
        img = media.small;
    }
    if (img && img.source_url == "") {
        img = media.medium;
    }
    if (img && img.source_url == "") {
        img = media.mediumwide;
    }
    if (img && img.source_url == "") {
        img = media.medium_large;
    }
    if (img && img.source_url == "") {
        img = media.large;
    }
    if (img && img.source_url == "") {
        img = media.full;
    }
    if ((img && img.source_url == "") || !img) {
        img = {};
        img.source_url = media ? media.source_url : "https://www.wp.theceomagazine.com/wp-content/uploads/2018/03/Igor-Klimkin_SPLAT-400x246.jpg";
    }
    return img;

}

function getImageSrcSet(media) {
    let img = "";

    /*
    set_post_thumbnail_size( 75, 75, true ); // Normal post thumbnails
    add_image_size( 'small', 150, 0, true ); // Permalink thumbnail size
    add_image_size( 'medium', 400, 246, true );
    add_image_size( 'large', 640, 0, true );
    add_image_size( 'largewide', 640, 300, true );
    add_image_size( 'xlarge', 800, 0, true );
    add_image_size( 'wide', 657, 404, true );
    add_image_size( 'featured', 1900, 0 );

    //https://www.monpurse.com/wp-content/uploads/2017/11/homepage_desktop_dyo-min-450x450.jpg 450w https://www.monpurse.com/wp-content/uploads/2017/11/homepage_desktop_dyo-min-800x312.jpg 800w https://www.monpurse.com/wp-content/uploads/2017/11/homepage_desktop_dyo-min-1200x467.jpg 840w  1926w
    */
    if (media.thumbnail && media.thumbnail.source_url) {
        img += media.thumbnail.source_url + " 75w, ";
    }
    if (media.small && media.small.source_url) {
        img += media.small.source_url + " 150w, ";
    }
    if (media.medium && media.medium.source_url) {
        img += media.medium.source_url + " 440w, ";
    }
    if (media.mediumwide && media.mediumwide.source_url) {
        img += media.mediumwide.source_url + " 440w, ";
    }
    if (media.large && media.large.source_url) {
        img += media.large.source_url + " 640w, ";
    }
    if (media.largewide && media.largewide.source_url) {
        img += media.largewide.source_url + " 640w, ";
    }
    if (media.xlarge && media.xlarge.source_url) {
        img += media.xlarge.source_url + " 800w";
    }
    if (media.featured && media.featured.source_url) {
        img += ", " + media.featured.source_url + " 800w ";
    }
    if (media.full && media.full.source_url) {
        img += ", " + media.full.source_url + " 1200w ";
    }

    return img;
}

function structurePostTags(post) {
    let termList = [];

    if (post && post._embedded
        && post._embedded['wp:term']
        && post._embedded['wp:term']) {

        const taxonomies = post._embedded['wp:term'];
        if (taxonomies.length > 0) {
            taxonomies.map(tax => {
                if (tax.length > 0) {
                    tax.map(term => {
                        if (term.taxonomy != "post_tag") {
                            let tempTerm = {};
                            tempTerm.id = term.id;
                            tempTerm.name = term.name;
                            tempTerm.slug = term.slug;
                            tempTerm.link = getReplacedUrlLink(term.link);

                            termList.push(tempTerm);
                        }
                    })
                }
            });
        }
    }
    return termList;
}

function removeImageCroppedSize(imgSrc) {
    if (!imgSrc || imgSrc == "") {
        return "";
    }
    let replacedImgSrc = imgSrc.replace('-450x450', '')
        .replace('-75x75', '')
        .replace('-150x150', '')
        .replace('-300x300', '')
        .replace('-1024x1024', '')
        .replace('-450x450', '')
        .replace('-400x450', '')
        .replace('-657x404', '')
        .replace('-640x300', '')
        .replace('-1024x1024', '')
    return replacedImgSrc;
}

function formatDate(params) {
    let timeZone = ""; //default - Australia/Sydney
    if (params.timeZone && params.timeZone != "") {
        timeZone = params.timeZone;
        //date would be moment(params.date).tz('Asia/Tokyo').format('DD MMMM YYYYY - hh:mm a')
    }

    const now = moment();
    const inputDate = moment(params.date);
    const daysDiff = now.diff(inputDate, 'days');

    let dateReturn;
    if (daysDiff > 0 || (params.type && params.type == 'full')) {
        dateReturn = inputDate.format('DD MMMM YYYY - hh:mm a');
    } else {
        if (inputDate.fromNow() != "a day ago") {
            dateReturn = inputDate.fromNow();
        } else {
            dateReturn = inputDate.format('DD MMMM YYYY - hh:mm a');
        }
    }

    return dateReturn;
}

function getElemDistance(elem) {
    if (!elem) {
        return 0;
    }
    var location = 0;
    if (elem.offsetParent) {
        do {
            location += elem.offsetTop;
            elem = elem.offsetParent;
        } while (elem);
    }
    return location >= 0 ? location : 0;
}

function formatSlugToName(slug) {
    if (!slug || slug == "") {
        return slug;
    }
    return slug.replace(/\-/g, ' ');
}

function getSocialLink(params) {
    let url = "";

    if (params.region == "Oceana") { //ANZ
        if (params.type == 'facebook') {
            url = "https://www.facebook.com/theceomagazineGlobal/";
        } else if (params.type == 'instagram') {
            url = "https://www.instagram.com/theceomagazineau/";
        } else if (params.type == 'linkedin') {
            url = "https://www.linkedin.com/company/the-ceo-magazine/";
        } else if (params.type == 'twitter') {
            url = "https://twitter.com/CEOMagazineau";
        }
    } else if (params.region == "Europe") {//EMEA
        if (params.type == 'facebook') {
            url = "https://www.facebook.com/theceomagazineemea";
        } else if (params.type == 'instagram') {
            url = "https://www.instagram.com/theceomagazineemea/";
        } else if (params.type == 'linkedin') {
            url = "https://www.linkedin.com/company/the-ceo-magazine-europe/";
        } else if (params.type == 'twitter') {
            url = "https://twitter.com/CEOMagazineemea";
        }

    } else if (params.region == "Asia") {
        if (params.country == "IN") { //INDIA
            if (params.type == 'facebook') {
                url = "https://www.facebook.com/theceomagazineindia";
            } else if (params.type == 'instagram') {
                url = "https://www.instagram.com/theceomagazineindia/";
            } else if (params.type == 'linkedin') {
                url = "https://www.linkedin.com/company/the-ceo-magazine-india/";
            } else if (params.type == 'twitter') {
                url = "https://twitter.com/CEOMagazineIN";
            }
        } else { //ASIA
            if (params.type == 'facebook') {
                url = "https://www.facebook.com/theceomagazineasia";
            } else if (params.type == 'instagram') {
                url = "https://www.instagram.com/theceomagazineasia/";
            } else if (params.type == 'linkedin') {
                url = "https://www.linkedin.com/company/the-ceo-magazine-asia/";
            } else if (params.type == 'twitter') {
                url = "https://twitter.com/CEOMagazineasia";
            }
        }
    } else {//DEFAULT - ANZ
        if (params.type == 'facebook') {
            url = "https://www.facebook.com/theceomagazineGlobal/";
        } else if (params.type == 'instagram') {
            url = "https://www.instagram.com/theceomagazineau/";
        } else if (params.type == 'linkedin') {
            url = "https://www.linkedin.com/company/the-ceo-magazine/";
        } else if (params.type == 'twitter') {
            url = "https://twitter.com/CEOMagazineau";
        }
    }

    return url;
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

const displayAds = ({posttype}) => {
    switch (posttype) {
        case 'business':
            return {
                leaderboard: '/77678524/CEO_Business_LB_728X90',
                leaderboardId: 'div-gpt-ad-1523594098788-0',
                mrec: '/77678524/CEO_Business_RSB_ATF_300X250',
                mrecId: 'div-gpt-ad-1523593494978-0'
            }
            break;
        case 'home':
            return {
                leaderboard: '/77678524/CEO_Home_LB_728X90',
                leaderboardId: 'div-gpt-ad-1488342309549-0',
                mrec: '/77678524/CEO_Home_RSB_ATF_300X250',
                mrecId: 'div-gpt-ad-1523593727241-0'
            }
            break;
        case 'executive-interviews':
        case 'globalShop':
            return {
                leaderboard: '/77678524/CEO_Home_LB_728X90',
                leaderboardId: 'div-gpt-ad-1488342309549-0',
                mrec: '/77678524/CEO_Home_RSB_ATF_300X250',
                mrecId: 'div-gpt-ad-1523593727241-0'
            }
            break;
        case 'lifestyle':
            return {
                leaderboard: '/77678524/CEO_Lifestyle_LB_728X90',
                leaderboardId: 'div-gpt-ad-1488343402254-0',
                mrec: '/77678524/CEO_Lifestyle_RSB_ATF_300X250',
                mrecId: 'div-gpt-ad-1523594270073-0'
            }
            break;
        default:
            return {
                leaderboard: '/77678524/CEO_Business_LB_728X90',
                leaderboardId: 'div-gpt-ad-1523594098788-0',
                mrec: '/77678524/CEO_Business_RSB_ATF_300X250',
                mrecId: 'div-gpt-ad-1523593494978-0'
            }
            break;
    }
};
const eventTrans = ({posttype}) => {
        let transPosttype = '';
        switch (posttype) {
            case 'events-private':
            case 'events-experience':
            case 'events-executive':
                return {
                    transPosttype: 'events'
                }
                break;
            default:
                return {
                    transPosttype: posttype
                }
                break;
        }

    };

function getPathVariable(variable, path) {
    let parts = path.substr(0).split('/'), value1 ='',value='';
    while  (parts.length) {
            value1 = parts.shift();
             if (value1 === variable)
               {
                 value = parts.shift();
                 break;
               }
           }
    return value;
}
export default {
    getReplacedUrlLink,
    structureFeaturedMedia,
    getImageUrl,
    getImageSrcSet,
    extractShortcode,
    removeImageCroppedSize,
    structurePostTags,
    formatDate,
    getElemDistance,
    formatSlugToName,
    replaceRegex,
    getSocialLink,
    getCookie,
    displayAds,
    eventTrans,
    getPathVariable

}
