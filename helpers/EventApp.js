import React, {PureComponent} from 'react';
import PaginationEvent from './PaginationEvent';
import EventCard from './EventCard';
import ArchiveListBlock from '../components/Layout/Blocks/ArchiveListBlock'
import ArchiveSingleBlock from "../components/Layout/Blocks/ArchiveSingleBlock";

class EventApp extends PureComponent {
    state = {allEvents: [], currentEvent: [], currentPage: null, totalPages: null};

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        let totalEventTmp = [];
        if (this.props.store.eventOriginalPosttype == "events-executive") {
            totalEventTmp = this.props.eawordlist;
        }
        if (this.props.store.eventOriginalPosttype == "events-private") {
            totalEventTmp = this.props.eprivatelist;
        }
        if (this.props.store.eventOriginalPosttype == "events-experience") {
            totalEventTmp = this.props.eexperiencelist;
        }
        this.setState({allEvents: totalEventTmp});
    }

    onPageChanged = data => {
        const {allEvents} = this.state;
        const {currentPage, totalPages, pageLimit} = data;
        const offset = (currentPage - 1) * pageLimit;
        const currentEvent = allEvents.slice(offset, offset + pageLimit);
        this.setState({currentPage, currentEvent, totalPages});
    };

    render() {

        const {allEvents, currentEvent, currentPage, totalPages} = this.state;
        const totalEvents = allEvents.length;
        if (totalEvents === 0) return null;
        let theCount = 0;


        let archiveText = currentEvent.map(archive => {
            return <ArchiveSingleBlock {...this.props.store} showTags="true" classNamesUl="widget-pav" imgSize="medium"
                                       key={archive.id} archive={archive} posttype={this.props.store.posttype}/>
        });

        return <div>
             <ul className={"widget-pav"+ " list-unstyled"}>
                {archiveText}
             </ul>
             < PaginationEvent totalRecords={totalEvents}
                              pageLimit={9}
                              pageNeighbours={1}
                              onPageChanged={this.onPageChanged}/>
             </div>

    }
}
export default EventApp;
