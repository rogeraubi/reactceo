import fetch from 'isomorphic-unfetch'
import config from './config';
import axios from 'axios'
import buffer from 'buffer';
import { inject, observer } from 'mobx-react'
import jQuery from 'jquery';

async function getArchive(posttype, params) {
    let filter = "";

    if(params){
        for(var i in params){
            if (params[i] > 0){
                filter += '&' + i + '=' + params[i];
            }
        }
    }
    // console.log("params==%j",params);

    console.log("getArchive==="+config.endpoint + '/' + posttype +'?_embed=true' + filter);
    const res =  await fetch(config.endpoint + '/' + posttype +'?_embed=true' + filter);
    return res;
}

async function getThirdPartyArchive( type, params){
    let filter = "";
    if(params){
        for(var i in params){
            filter += '&' + i + '=' + params[i];
        }
    }
    const res =  await fetch(config.vendorEndpoint + '/' + type + '?1=1' + filter);
    console.log('getThirdPartyArchive()'+config.vendorEndpoint + '/' + type + '?1=1' + filter);
    return res;
}

async function getSinglePost(params){
	const res =  await fetch(config.endpoint + '/' + params.posttype + '?slug=' + params.slug + '&_embed=true');
    //console.log('getSinglePost()'+config.endpoint + '/' + params.posttype + '?slug=' + params.slug + '&_embed=true');
    return res;
}

async function getPrivatePost(posttype, params){
    let filter = "";
    if(params){
        for(var i in params){
            filter += '&' + i + '=' + params[i];
        }
    }
    console.log('getPrivatePost()');
    const res =  await fetch( config.privEndpoint + '/post/' + posttype + '?_embed=true' + filter );
    const data = await res.json();
    return data;
}

async function getShopMagazine(params){

    /* ANZ: 10
        EU: 12
        ASIA: 14
        INDIA: 15
        EMEA: 16 */
    let getShopProductPromise = null;
    let categoryId = 10;

    if(params.region == "Oceana"){ //ANZ
      categoryId= 10;

    }else if(params.region == "Europe"){//EMEA
      categoryId= 16;

    }else if(params.region == "Asia"){
      if(params.country == "IN"){ //INDIA
        categoryId= 15;
      }else{ //ASIA
        categoryId= 14;
      }
    }else{//DEFAULT - ANZ
      categoryId= 10;
    }

    try{
        const resPromise = await fetch(config.vendorEndpoint + "/shop-product?category_id="+categoryId+"&limit=1&order=entity_id", {
            headers: {
                'Content-Type': 'application/json'
            }
        });

        const res = await resPromise.json();
        const resObj = res[Object.keys(res)[0]];

        const productDetailsPromise = await fetch(config.vendorEndpoint + "/shop-product/" + resObj.entity_id, {
            headers: {
                'Content-Type': 'application/json'
            }
        });

        return productDetailsPromise;
    }catch(error){
        return null;
    }
}


async function postComment(params){
    return jQuery.ajax({
        url: config.cmsEndpoint + '/wp-admin/admin-ajax.php?action=post_comment',
        type: 'POST',
        data: {
            author_email: params.authorEmail,
            author: params.authorName,
            content: params.content,
            post_id: params.postId,
            parent_id: params.parentId,
            g_recaptcha_response: params.gRecaptchaResponse
        }
    });
}
export default{
    getArchive, getSinglePost, getPrivatePost,
    getShopMagazine, getThirdPartyArchive,
    postComment
}
