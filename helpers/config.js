import getConfig from 'next/config'
const {publicRuntimeConfig} = getConfig();

let endpoint = 'https://vyems1rzfc.execute-api.ap-southeast-2.amazonaws.com/cms';
let sitename = "https://www.theceomagazine.com";
if(publicRuntimeConfig.node_env === "development" || publicRuntimeConfig.node_env === "production_net"){
   endpoint = 'http://cms.theceomagazine.net/wp-json/wp/v2';    
}
if(publicRuntimeConfig.node_env === "production_com"){
   endpoint = 'https://vyems1rzfc.execute-api.ap-southeast-2.amazonaws.com/cms';
}

if(publicRuntimeConfig.node_env === "development"){
  sitename = "http://localhost:3000";
}
if(publicRuntimeConfig.node_env === "production_net"){
  sitename = "https://www.theceomagazine.net";
}
export default {
	endpoint: endpoint,
  sitename: sitename,
  privEndpoint: 'https://g4p0wuix4c.execute-api.ap-southeast-2.amazonaws.com/priv',
  vendorEndpoint: 'https://h90og3p05b.execute-api.ap-southeast-2.amazonaws.com/vendor',
  cmsEndpoint: 'http://cms.theceomagazine.net',
	postTypes: [ 'lifestyle',
      				  'business',
      				  'events',
      				  'executive-interviews',
      				  'opinion',
      				  'company-directory',
                'technology',
                'philanthropy',
                'search',
                'video',
                'careers',
                'press',
                'authors'
				    ]
}
