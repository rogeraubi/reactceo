import Document, { Head, Main, NextScript } from 'next/document'
import GoogleTagManager from '../components/Advertising/GoogleTagManager'
import { renderToString } from 'react-dom/server';

import getConfig from 'next/config'
const { publicRuntimeConfig } = getConfig();

export default class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);

    return { ...initialProps }
  }

  render () {

    let gtmId = "";
    if(publicRuntimeConfig && publicRuntimeConfig.node_env === "production_com"){
      gtmId = "GTM-PZTM2K";
    }else{
      gtmId = "GTM-P9HJ7CP";
    }


    return (
      <html>
        <Head>
          <link rel="stylesheet" href="/_next/static/style.css" />
        </Head>
        <body>
          <GoogleTagManager scriptId="google-tag-manager" gtmId={gtmId} type="noscript"/>
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}
