import { PureComponent } from 'react'
import MainLayout from '../components/Layout/MainLayout.js'
import Home from '../components/Layout/Home.js'
import fetch from 'isomorphic-unfetch'
import config from '../helpers/config';
import service from '../helpers/service';
import { Provider } from 'mobx-react';
import { initStore } from '../store';
import { Link } from '../routes';

export default class Index extends PureComponent{
  //second to be called
  constructor(props){
    super(props)
    this.store = initStore(props.isServer, { menu: props.menu, viewerRegion: props.viewerRegion,
                                              viewerCountry: props.viewerCountry,
                                              shopProduct: props.shopProduct,
                                              yoastPageSEO: props.yoastPageSEO,
                                              posttype: 'home'
                                          } );
  }

  //first to be called
  /*
    params pathname = url
    query - quer string section of url
    asPath - string of actual path
    req - http request (server only)
    res - http response (server only)
    jsonPageRes - fetch response ( client only)
    err - error object
  */
  static async getInitialProps ({ req, pathname, params, query }) {
    const isServer = !!req;
    let dataMenu = {};
    if(isServer){
      const dataMenuPromise = await service.getThirdPartyArchive('menu', {});
      dataMenu = await dataMenuPromise.json();
    }
    const viewerCountry = (req && req.headers ) ? (req.headers['cloudfront-viewer-country'] ? req.headers['cloudfront-viewer-country'] : "AU" ) : 'AU';

    const getRegionOfViewerPromise = await service.getThirdPartyArchive( 'region', {fields: 'region', codes: viewerCountry});
    const getRegionOfViewer = await getRegionOfViewerPromise.json();
    const viewerRegion = "Oceania";

    const businessListPromise = await service.getArchive( 'business', { per_page: 9 });
    const businessList = await businessListPromise.json();

    const lifestyleListPromise = await service.getArchive( 'lifestyle', { per_page: 9 });
    const lifestyleList = await lifestyleListPromise.json();

    const execInterviewListPromise = await service.getArchive( 'executive-interviews', { per_page: 9});
    const execInterviewList = await execInterviewListPromise.json();

    const opinionListPromise = await service.getArchive( 'opinion', { per_page: 9 });
    const opinionList = await opinionListPromise.json();


    const eventsListPromise = await service.getArchive( 'events', { per_page: 9 });
    const eventsList = await eventsListPromise.json();


    const featuredListPromise = await service.getArchive( 'featured', {});
    const featuredList = await featuredListPromise.json();

    let shopProduct = {};
    try{
      const getShopProductPromise = await service.getShopMagazine({region: viewerRegion, country: viewerCountry});
      shopProduct = await getShopProductPromise.json();
    }catch(error){
      console.log('error getting shop products');
      console.log(error);
    }


    const yoastPageSEOPromise = await service.getThirdPartyArchive('seo-details', {});
    const yoastPageSEO = await yoastPageSEOPromise.json();


    const parameters = {
             menu: dataMenu,
             posttype: 'home',
             yoastPageSEO: yoastPageSEO,
             businessList: businessList,
             lifestyleList: lifestyleList,
             execInterviewList: execInterviewList,
             opinionList: opinionList,
             eventsList:   eventsList,
             featuredList: featuredList,
             viewerRegion: viewerRegion,
             viewerCountry: viewerCountry,
             shopProduct: shopProduct
           };

    const store = initStore(isServer, parameters);

    return parameters;
  }
  //third to be called
  render(){
    return (
      <Provider store={this.store}>
        <div id="page-wrapper">
          <MainLayout>
            <Home businessList={this.props.businessList}
                  lifestyleList={this.props.lifestyleList}
                  execInterviewList={this.props.execInterviewList}
                  opinionList={this.props.opinionList}
                  featuredList={this.props.featuredList}
                  eventsList ={this.props.eventsList}/>
          </MainLayout>
        </div>
      </Provider>
    )
  }
}
