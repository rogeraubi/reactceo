import {PureComponent} from 'react'
import fetch from 'isomorphic-unfetch'
import config from '../helpers/config';
import service from '../helpers/service';
import {Provider} from 'mobx-react';
import {initStore} from '../store';
import MainLayout from '../components/Layout/MainLayout'
import Page from '../components/Layout/Page'
import SinglePost from '../components/Layout/SinglePost'
import CompanyDirectoryPost from '../components/Layout/CustomPages/CompanyDirectory/CompanyDirectoryPost'
import ExecutiveInterviewsPost from '../components/Layout/CustomPages/ExecutiveInterviews/ExecutiveInterviewsPost'
import EventsPost from '../components/Layout/CustomPages/Events/EventsPost'
import ShopPost from '../components/Layout/CustomPages/Shop/ShopPost'
import CareersPost from '../components/Layout/CustomPages/Careers/CareersPost'

import Archive from '../components/Layout/Archive'
import Search from '../components/Layout/Search'
import EmptyPage from '../components/Layout/EmptyPage';
import Feed from '../components/Layout/Feed'
import AuthorsPost from "../components/Layout/AuthorsPost";

import Helper from "../helpers/helper";


export default class Post extends PureComponent {
    //second to be called
    constructor(props) {
        super(props);
        this.store = initStore(props.isServer, {
            menu: props.menu,
            archiveList: props.archiveList,
            archiveTotalPosts: props.archiveTotalPosts,
            archivePageActive: props.archivePageActive,
            eventOriginalPosttype: props.eventOriginalPosttype,
            postList: props.postList,
            page: props.page,
            posttype: props.posttype,
            category: props.category,
            categoryId: props.categoryId,
            region: props.region,
            slug: props.slug,
            singlePost: props.singlePost,
            sidebarList: props.sidebarList,
            featuredList: props.featuredList,
            viewerRegion: props.viewerRegion,
            viewerCountry: props.viewerCountry,
            shopProduct: props.shopProduct,
            yoastPageSEO: props.yoastPageSEO,
            archiveFilterRegion: props.archiveFilterRegion,
            archiveFilterCategory: props.archiveFilterCategory
        });
    }

    //first to be called
    static async getInitialProps({req, asPath, pathname, params, query}) {

        if (asPath == "/favicon.ico") {
            return {};
        }
        const isServer = !!req;

        const viewerCountry = (req && req.headers) ? (req.headers['cloudfront-viewer-country'] ? req.headers['cloudfront-viewer-country'] : "AU") : 'AU';

        const getRegionOfViewerPromise = await service.getThirdPartyArchive('region', {
            fields: 'region',
            codes: viewerCountry
        });
        const getRegionOfViewer = await getRegionOfViewerPromise.json();
        const viewerRegion = "Oceania";
        let transPosttype = Helper.eventTrans({posttype: query.posttype});
        let origPosttype = query.posttype;

        if (query.posttype.search(/events/i) === 0) {
            query.posttype = transPosttype.transPosttype;
            console.log(query.posttype);
        }

        let dataMenu = {};
        if (isServer) {
            const dataMenuPromise = await service.getThirdPartyArchive('menu', {});
            dataMenu = await dataMenuPromise.json();
        }

        let isPostType = config.postTypes.indexOf(query.posttype);

        let isPage = false;
        let isCategoryArchive;
        let page = {};
        let archiveList = [];
        let archiveTotalPosts = 0;
        let archivePageActive = 1;
        let sidebarList = [];
        let postList = [];
        let slug = query.slug;
        let singlePost = {};
        let isSearchPage = false;
        let subCategoryId = 0;
        let subRegionId = 0;
        let isPagination = false;
        let paginationNumber = 0;
        let isCategoryPagination = false;
        let isFeedPage = false;

        let archiveFilterRegion = [];
        let archiveFilterCategory = [];

        /*** PAGINATION ARCHIVE ***/
        console.log(query);
        if ((query.category && query.category == "page") || (query.slug && query.slug == "page")) {
            isPagination = true;
            if (query.category && query.category == "page") {
                paginationNumber = 9 * (query.slug - 1);
                archivePageActive = query.slug;
            } else if (query.slug && query.slug == "page") {
                paginationNumber = 9 * (query.twoslug - 1);
                archivePageActive = query.twoslug;
                isCategoryPagination = true;
            }
        } else if ((query.twoslug == 'page') && (query.posttype == 'executive-interviews')) {
            paginationNumber = 9 * (query.threeslug - 1);
            archivePageActive = query.threeslug;
            isCategoryPagination = true;
        }

        if (isPostType <= -1 && query.posttype) {
            if (query.posttype == 'feed') { //static feed page
                isFeedPage = true;
            } else {
                let pageSlug = query.posttype;
                if (query.category && query.category != "") {
                    pageSlug = query.category;
                }

                const isPagePromise = await service.getSinglePost({posttype: 'pages', slug: pageSlug});
                const isPageData = await isPagePromise.json();
                if (isPageData && isPageData.length > 0) {
                    slug = query.posttype;
                    page = isPageData[0];
                    isPage = true;
                }
            }
        } else {
            isPostType = true;
            if (query.posttype == 'search') { //use search component and retrieve search results
                isSearchPage = true;
                /************** POST TYPE WITHOUT CATEGORY IN THE URL ******************/
            } else if (query.p && query.preview && query.p != "" && query.preview != "") {
                //View draft page
                isCategoryArchive = false;
                const dataSinglePost = await service.getPrivatePost(query.posttype, {
                    include: query.p,
                    status: 'draft'
                });
                singlePost = dataSinglePost[0];

                /************** PRIVATE ******************/
            } else if (query.preview_id && query.preview && query.preview_id != "" && query.preview != "") {
                //View scheduled post
                isCategoryArchive = false;
                const dataSinglePost = await service.getPrivatePost(query.posttype, {
                    include: query.preview_id,
                    status: 'future'
                });
                singlePost = dataSinglePost[0];
            } else {
                /*** SINGLE POST OPINION - WITHOUT CATEGORY ***/
                if ((query.posttype == 'opinion' || query.posttype == 'events' || query.posttype == 'press') && !slug && query.category) {
                    isCategoryArchive = false;
                    const dataPromise = await service.getSinglePost({posttype: query.posttype, slug: query.category});
                    const singlePostJson = await dataPromise.json();
                    singlePost = singlePostJson[0];
                    /*** SINGLE POST ***/
                } else if (query.slug && query.slug !== 'europe' && query.slug !== 'north-america' && query.slug !== 'india' && query.slug !== 'anz' && query.slug !== 'global' && query.slug !== 'asia' && query.slug !== 'emea' && !isPagination && query.posttype == "executive-interviews") {
                    isCategoryArchive = false;
                    const dataPromise = await service.getSinglePost({posttype: query.posttype, slug: slug});
                    const singlePostJson = await dataPromise.json();
                    singlePost = singlePostJson[0];
                    if (!singlePost && query.twoslug && query.twoslug != "") {
                        const dataPromiseTwoSlug = await service.getSinglePost({
                            posttype: query.posttype,
                            slug: query.twoslug
                        });
                        const singlePostJsonTwoSlug = await dataPromiseTwoSlug.json();
                        singlePost = singlePostJsonTwoSlug[0];
                    }
                } else if (query.slug && query.slug != "" && !isPagination && query.posttype != "executive-interviews") {
                    //with third level url - slug | check later if it's a sub-category or not
                    isCategoryArchive = false;
                    const dataPromise = await service.getSinglePost({posttype: query.posttype, slug: slug});
                    const singlePostJson = await dataPromise.json();

                    singlePost = singlePostJson[0];

                    if (!singlePost && query.twoslug && query.twoslug != "") {
                        const dataPromiseTwoSlug = await service.getSinglePost({
                            posttype: query.posttype,
                            slug: query.twoslug
                        });
                        const singlePostJsonTwoSlug = await dataPromiseTwoSlug.json();
                        singlePost = singlePostJsonTwoSlug[0];
                    }
                } else if (query.category && query.category != "" && query.category != "page" || isCategoryPagination) {
                    isCategoryArchive = true;
                    let subCategory = query.posttype + '-category';
                    const categoriesPromise = await service.getArchive(subCategory, {per_page: 100});
                    const categories = await categoriesPromise.json();

                    for (let i in categories) {
                        const category = categories[i];
                        if (category.slug === query.category) {
                            subCategoryId = category.id;
                            continue;
                        }
                    }
                    let subRegion = '';
                    if (query.posttype == 'company-directory' || query.posttype == 'executive-interviews') {
                        subRegion = query.posttype + '-region';
                        const regionPromise = await service.getArchive(subRegion, {per_page: 100});
                        const regions = await regionPromise.json();
                        for (let i in regions) {
                            const region = regions[i];

                            if (region.slug === query.slug) {
                                subRegionId = region.id;
                                continue;
                            }

                            if (region.slug === query.category) {
                                subRegionId = region.id;
                                continue;
                            }
                        }
                    }
                    let de_perpage = 9;
                    if (query.posttype == "executive-interviews") {
                        de_perpage = 11;
                    }

                    const archiveListPromise = await service.getArchive(query.posttype, {
                        [subCategory]: subCategoryId,
                        [subRegion]: subRegionId,
                        offset: paginationNumber, per_page: de_perpage
                    });
                    archiveTotalPosts = archiveListPromise.headers && archiveListPromise.headers._headers && archiveListPromise.headers._headers['x-wp-total']
                    && archiveListPromise.headers._headers['x-wp-total'][0]
                        ? archiveListPromise.headers._headers['x-wp-total'][0] : 0;
                    archiveList = await archiveListPromise.json();
                } else if ((!query.category && !query.slug) || isPagination) {
                    /*** CATEGORY ARCHIVE FIRST TIER ONLY ***/
                    isCategoryArchive = true;
                    let archiveListPromise = "";
                    if (query.posttype == 'search') {

                    } else {
                        let total_number = 9;
                        let queryPosttype = '';

                        if (query.posttype == 'events') {
                            total_number = 100;
                        }
                        if (query.posttype == 'executive-interviews') {
                            total_number = 11;
                        }

                        archiveListPromise = await service.getArchive(query.posttype, {
                            offset: paginationNumber,
                            per_page: total_number
                        });
                        archiveTotalPosts = archiveListPromise.headers && archiveListPromise.headers._headers && archiveListPromise.headers._headers['x-wp-total']
                        && archiveListPromise.headers._headers['x-wp-total'][0] ? archiveListPromise.headers._headers['x-wp-total'][0] : 0;
                        archiveList = await archiveListPromise.json();
                    }
                }
            }
        }

        /*** ADD CUSTOM CATEGORY FILTERS ***/
        if (isCategoryArchive == true && (query.posttype == 'company-directory' || query.posttype == 'executive-interviews')) {
            const archiveFilterRegionPromise = await service.getArchive(query.posttype + '-region', {per_page: 100});
            archiveFilterRegion = await archiveFilterRegionPromise.json();

            const archiveFilterCategoryPromise = await service.getArchive(query.posttype + '-category', {per_page: 100});
            archiveFilterCategory = await archiveFilterCategoryPromise.json();
        }

        if (isCategoryArchive) {
            const sidebarListPromise = await service.getArchive('featured', {per_page: 4});
            sidebarList = await sidebarListPromise.json();
        } else {
            let postListPromise = '';
            if (query.posttype == 'authors' && singlePost) {
                postListPromise = await service.getArchive('author_articles', {slug: singlePost.id, per_page: 16});
            } else {
                postListPromise = await service.getArchive(query.posttype, {per_page: 4});
            }
            postList = await postListPromise.json();
        }

        const featuredListPromise = await service.getArchive('featured', {post_type: query.posttype, per_page: 5});
        const featuredList = await featuredListPromise.json();

        let shopProduct = {};
        try {
            const getShopProductPromise = await service.getShopMagazine({region: viewerRegion, country: viewerCountry});
            shopProduct = await getShopProductPromise.json();
        } catch (error) {
            console.log('error getting shop products');
            console.log(error);
        }

        const yoastPageSEOPromise = await service.getThirdPartyArchive('seo-details', {});
        const yoastPageSEO = await yoastPageSEOPromise.json();

        const parameters = {
            menu: dataMenu,
            page: page,
            posttype: query.posttype,
            category: query.category,
            categoryId: subCategoryId,
            region: query.region,
            subRegionId: subRegionId,
            slug: slug,
            isPostType: isPostType,
            isPage: isPage,
            isFeedPage: isFeedPage,
            isCategoryArchive: isCategoryArchive,
            archiveList: archiveList,
            postList: postList,
            archiveTotalPosts: archiveTotalPosts,
            archivePageActive: archivePageActive,
            singlePost: singlePost,
            sidebarList: sidebarList,
            featuredList: featuredList,
            isSearchPage: isSearchPage,
            searchText: query.q,

            viewerRegion: viewerRegion,
            viewerCountry: viewerCountry,
            shopProduct: shopProduct,
            yoastPageSEO: yoastPageSEO,

            archiveFilterRegion: archiveFilterRegion,
            archiveFilterCategory: archiveFilterCategory,
            eventOriginalPosttype: origPosttype
        };

        const store = initStore(isServer, parameters);

        return parameters;
    }


    render() {
        return (
            <Provider store={this.store} id="page-wrapper">
                <div id="page-wrapper">
                    <MainLayout>
                        {this.props.isPostType == true
                            ? (this.props.isCategoryArchive == true ? <Archive/> :
                                    (this.props.isSearchPage == true ? <Search searchText={this.props.searchText}/> :
                                            (!this.props.singlePost ? <EmptyPage/> :
                                                    (this.props.posttype == 'company-directory' ?
                                                            <CompanyDirectoryPost/> :
                                                            this.props.posttype == 'executive-interviews' ?
                                                                <ExecutiveInterviewsPost/> :
                                                                this.props.posttype == 'careers' ? <CareersPost/> :
                                                                    this.props.posttype == 'events' ? <EventsPost/> :
                                                                        this.props.posttype == 'authors' ?
                                                                            <AuthorsPost/> : <SinglePost/>
                                                    )
                                            )
                                    )
                            )
                            : this.props.isPage == true ? <Page/>
                                : this.props.isFeedPage == true ? <Feed/>
                                    : this.props.eventOriginalPosttype == 'globalShop' ? <ShopPost/> : <EmptyPage/>
                        }
                    </MainLayout>
                </div>
            </Provider>
        )
    }
}
