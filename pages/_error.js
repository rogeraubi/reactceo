import { PureComponent } from 'react'

export default class ErrorPage extends PureComponent {
  render () {
    return (
        <div>
          <h1>Sorry we can't find that page! </h1>
          <h1> go back to <a href="https://www.theceomagazine.com">The CEO Magazine</a> </h1>
        </div>
    )
  }
}