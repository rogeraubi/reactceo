  import { Component } from 'react'
import MainLayout from '../components/Layout/MainLayout.js'
import Link from 'next/link'
import fetch from 'isomorphic-unfetch'
import config from '../helpers/config';
import { Provider } from 'mobx-react';
import { initStore } from '../store';

//import jquery  from 'jquery';

//import { inject, observer } from 'mobx-react'


export default class Error extends Component{

  //second to be called
  constructor(props){

    super(props)
    this.store = initStore(props.isServer, props.menu, props.obj)
  }

  //first to be called
  static async getInitialProps ({ req, pathname, res, err }) {
    console.log(err);
    const isServer = !!req
    const store = initStore(isServer)
    //, 'Access-Control-Allow-Origin': '*'}
    let dataMenu = {};
    /*if(isServer){
      const resMenu =  await fetch(config.endpoint + '/menu');
      dataMenu = await resMenu.json();
    }*/

    const statusCode = res ? res.statusCode : err ? err.statusCode : null;

    const isPostType = config.postTypes.includes(pathname);
    let isPage = false;
    /*if(isPostType === false){
      const resIsPage=  await fetch(config.endpoint + '/pages?slug=thank-you'); //, {body: {slug: ''}
      const dataIsPage = await resIsPage.json();
      if(dataIsPage && dataIsPage.length > 0){
        isPage = true;
      }
    }*/
    //isPostType: isPostType, isPage: isPage
    return { statusCode, isServer, menu: dataMenu, obj: {}  }
  }

  //third to be called
  render(){

    return (
      <Provider store={this.store}>
        <MainLayout>
           <p>

          {this.props.statusCode
            ? `An error ${this.props.statusCode} occurred on server`
            : 'An error occurred on client'}
        </p>
        </MainLayout>
      </Provider>
    )
  }
}
//{ this.isPostType ? 'POST TYPE' :  this.isPage ? 'PAGE TRUE' : 'ERROR NOTHING'}
