import { Component } from 'react';
import helper from '../../helpers/helper'

export default class CarouselScript extends Component {
  componentDidMount() {
      $(document).ready(function () {
            $(".slick-carousel").slick({
                dots: true,
                infinite: true,
                speed: 300,
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                responsive: [
                  /*{
                    breakpoint: 520,
                    settings: {
                      overflow: 'unset'
                    }
                  },*/
                ]
            }); 
      });
  }

  render(){
  	return <div>
        	    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                <script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.8.1/slick/slick.min.js"></script>
                <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css" />
                <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css" />
                <style global jsx>{`
                  .slick-prev{
                    left: 0px !important;
                  }
                  .slick-next{
                    right: 0px !important;
                  }
                  .slick-prev, .slick-next{
                    z-index: 1;
                    height: 35px !important;
                    width: 35px !important;
                  }
                  .slick-next:before, .slick-prev:before{
                    font-size: 35px !important;
                  }
                `}</style>
  		   </div>
  }
}



