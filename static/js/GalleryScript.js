import { Component } from 'react';
import helper from '../../helpers/helper'

export default class GalleryScript extends Component {
  componentDidMount() {
      $(document).ready(function ($) {
		    $('.gallery').each(function() {
		    	$(this).find('.gallery-item').each(function(){
		    		var imgSrc = $(this).find('.gallery-icon').find('img').attr('src');
		    		imgSrc = helper.removeImageCroppedSize(imgSrc);

		    		$(this).find('.gallery-icon').attr('href', imgSrc);
		    	});
		    	$(this).magnificPopup({
		            delegate: '.gallery-icon',
		            type: 'image',
		            gallery: {enabled: true},
		            image: {
		            	titleSrc: function(item) {
						    return item.el.parents('.gallery-item').find('.gallery-caption').text();
						}
					}
		        });
		    });
      });
  }

  render(){
  	return <div>
			  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
			  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
			  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css" media="all" />
			  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
  		   </div>
  }
}



	